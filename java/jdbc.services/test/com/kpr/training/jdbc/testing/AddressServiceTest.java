package com.kpr.training.jdbc.testing;

/*To Test All possible conditions for Address Service 

Requirement
To Test All possible conditions for Address Service

Entity
* ServicesTest
* PersonService
* AppException
* Connection
* Address

Method Signature
* public void insertAddress();
* public void readAddress();
* public void readAllAddress();
* public void updateAddress()
* public void deleteAddress()

Jobs to be Done:
 * Create a Class name AddressServiceTest
 * Declare and Initialize  insertPersonStatus and it is equal to zero
 * uptateStatus is in type boolean and it is false.
 * deleteStatus is in type boolean and it is false.
 * In method insertAddress() method
      => Create a object for Address and AddressServices name address and addressServices
      => set city,street and postal code
      => Call the method insertAddress by address as parameter the return value is stored in insertPersonStatus.
      => Assert true when insertPersonStatus is greater than zero.
      -> check if createAddressStatus > 0
		  => call the method commitRollback() and pass the parameter is true
	  -> else 
		  => call the method commitRollback() and pass the parameter is false
		  
 * In method readAddress() method
      => Create a object for AddressServices name addressServices
      => Call the method readAddress by 1 as parameter the return value is stored in list.
      => Assert true when objectlist is not equal to null.         
 * In method readAllAddress() method
      => Create a object for AddressServices name addressServices
      => Call the method readAllAddress and the return value is stored in list.
      => Assert true when objectlist is not equal to null.              
 * In method updateAddress() method
      => Create a object for Address and AddressServices name address and addressServices
      => set city,street and postal code
      => Call the method updateAddress by address as parameter the return value is stored in updatePersonStatus.
      => Assert true when updatePersonStatus is greater than zero.      
      		-> check if createAddressStatus > 0
			=> call the method commitRollback() and pass the parameter is true
		-> else 
			=> call the method commitRollback() and pass the parameter is false
 * In method deleteAddress() method
      => Create a object for AddressServices name addressServices
      => Call the method readAddress by 1 as parameter the return value is stored in deletePersonStatus.
      => Assert true when deletePersonStatus is equal to false.            

PsudoCode:
public class AddressServiceTest {
	@BeforeTest
	public static void setUp() throws Exception {
		addressService = new AddressServices();

	}

	@Test(priority = 1)
	public void insertAddressCSVFile() throws Exception {
		IncludeAddress address = new IncludeAddress();	
		address.includeAddress();
	}

	@Test(priority = 2)
	public void insertAddress() throws Exception {
		Address address = new Address("kolkatta","gandhi road",600123);
		createPersonStatus = addressService.create(address);
		Assert.assertTrue(createPersonStatus > 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}
	@Test(priority = 3)
	public void readAddress() throws Exception {
		Address address = addressService.read(5);
		Address expectedAddress = new Address("new york", "manhatten", 123456);
		Assert.assertTrue(address.getStreet().equals(expectedAddress.getStreet())
						&& address.getCity().equals(expectedAddress.getCity()));
	}

	@Test(priority = 4)
	public void updateAddress() throws Exception {
		Address address = new Address("madhurai","valuvar road",636006);
		address.setId(3);
		addressService.update(address);
		Address updatedAddress = addressService.read(3);
		Assert.assertEquals(address.getStreet(), updatedAddress.getStreet());
		if(address.getStreet() == updatedAddress.getStreet()) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 5)
	public void deleteAddress() throws Exception {	
		AddressServices.delete(1);
		Address deletedAddress = addressService.read(1);
		Assert.assertNull(deletedAddress);
		if(deletedAddress == null) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 6)
	public void readAllAddress() throws Exception {		
		ArrayList<Address> addressList = addressService.readAll();
		Stream.of(addressList.toString()).forEach(System.out::println);
				Assert.assertTrue(addressList.size() > 0);
	}

	@Test(priority = 7)
	public void searchAddress() throws Exception {
		Address address = new Address();
		address.setPostal_code(636006);
		ArrayList<Address> objectList = addressService.addressSearch(address);
		Stream.of(objectList.toString()).forEach(System.out::println);
		Assert.assertTrue(objectList.toString() != null);
	}
}
*/
import java.util.ArrayList;
import java.util.stream.Stream;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;
import com.kpr.training.jdbc.model.Address;
import com.kpr.training.jdbc.services.AddressServices;
import com.kpr.training.jdbc.services.ConnectionService;
public class AddressServiceTest {
	long createPersonStatus = 0;
	static AddressServices addressService;
	boolean updateStatus = false;
	boolean deleteStatus = false;

	@BeforeTest
	public static void setUp() throws Exception {
		addressService = new AddressServices();

	}

	@Test(priority = 1)
	public void insertAddressCSVFile() throws Exception {
		IncludeAddress address = new IncludeAddress();	
		address.includeAddress();
	}

	@Test(priority = 2)
	public void insertAddress() throws Exception {
		Address address = new Address("kolkatta","gandhi road",600123);
		createPersonStatus = addressService.create(address);
		Assert.assertTrue(createPersonStatus > 0);
		if(createPersonStatus > 0) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}
	@Test(priority = 3)
	public void readAddress() throws Exception {
		Address address = addressService.read(5);
		Address expectedAddress = new Address("new york", "manhatten", 123456);
		Assert.assertTrue(address.getStreet().equals(expectedAddress.getStreet())
						&& address.getCity().equals(expectedAddress.getCity()));
	}

	@Test(priority = 4)
	public void updateAddress() throws Exception {
		Address address = new Address("madhurai","valuvar road",636006);
		address.setId(3);
		addressService.update(address);
		Address updatedAddress = addressService.read(3);
		Assert.assertEquals(address.getStreet(), updatedAddress.getStreet());
		if(address.getStreet() == updatedAddress.getStreet()) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 5)
	public void deleteAddress() throws Exception {	
		AddressServices.delete(1);
		Address deletedAddress = addressService.read(1);
		Assert.assertNull(deletedAddress);
		if(deletedAddress == null) {
			ConnectionService.commitRollback(true);
		} else {
			ConnectionService.commitRollback(false);
		}
	}

	@Test(priority = 6)
	public void readAllAddress() throws Exception {		
		ArrayList<Address> addressList = addressService.readAll();
		Stream.of(addressList.toString()).forEach(System.out::println);
				Assert.assertTrue(addressList.size() > 0);
	}

	@Test(priority = 7)
	public void searchAddress() throws Exception {
		Address address = new Address();
		address.setPostal_code(636006);
		ArrayList<Address> objectList = addressService.addressSearch(address);
		Stream.of(objectList.toString()).forEach(System.out::println);
		Assert.assertTrue(objectList.toString() != null);
	}
}