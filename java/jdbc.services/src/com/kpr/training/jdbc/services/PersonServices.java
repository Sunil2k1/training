package com.kpr.training.jdbc.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.sql.Date;
import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCodesvalues;
import com.kpr.training.jdbc.model.Person;
import com.kpr.training.jdbc.validator.PersonValidator;

public class PersonServices {
	private long personId;
	PersonValidator validator = new PersonValidator();
	AddressServices as = new AddressServices();
	public long create(Person person) {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.INSERT_PERSON_QUERY)) {
			
			checkNameUnique(person.getFirstName(), person.getLastName());
			checkEmailUnique(person.getEmail());
			setPersonWithPreparedStatement(ps, person);
			ps.setLong(5, as.create(person.address));
			personId = ps.executeUpdate();
			
			if (personId == 0) {
				throw new AppException(ErrorCodesvalues.ERROR07);
			}
			return personId;
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR07, e);
		}
	}

	public void update(Person person) {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.UPDATE_PERSON_QUERY)) {
			checkNameUnique(person.getFirstName(), person.getLastName());
			checkEmailUnique(person.getEmail());
			setPersonWithPreparedStatement(ps, person);
			ps.setLong(5, as.update(person.address));
			ps.setLong(6, person.getId());
			
			if (ps.executeUpdate() == 0) {
				throw new AppException(ErrorCodesvalues.ERROR21);
			}
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR21, e);
		}
	}

	public ArrayList<Object> readAll() {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.READ_ALL_PERSON_QUERY)) {
			
			ArrayList<Object> personList = new ArrayList<Object>();
			ResultSet resultSet = ps.executeQuery();
			
			while (resultSet.next()) {
				personList.add(getPersonWithResultSet(resultSet));
				personList.add(AddressServices.getAddressWithResultSet(resultSet));
			}
			return personList;
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR22, e);
		}

	}

	public ArrayList<Object> read(long id, boolean includeAddress) {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.READ_PERSON_QUERY)) {
			
			ArrayList<Object> personList = new ArrayList<Object>();
			ps.setLong(1, id);
			ResultSet resultSet = ps.executeQuery();
			
			if (resultSet.next() && !includeAddress) {
				personList.add(getPersonWithResultSet(resultSet));
				return personList;
			}
			personList.add(getPersonWithResultSet(resultSet));
			personList.add(AddressServices.getAddressWithResultSet(resultSet));
			return personList;
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR23, e);
		}

	}

	public void delete(long personid) {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.DELETE_PERSON_QUERY)) {
			ps.setLong(1, personid);
			
			if (ps.executeUpdate() != 0) {
				throw new AppException(ErrorCodesvalues.ERROR24);
			}
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR24);
		}

	}

	public void checkEmailUnique(String email) throws Exception {
		
		int count = 1;
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.UNIQUE_EMAIL_QUERY)) {
			ps.setString(1, email);
			ResultSet resultSet = ps.executeQuery();

			if (resultSet.next()) {
				count = resultSet.getInt(Constant.COUNT);
			}
			if (count != 0) {
				throw new AppException(ErrorCodesvalues.ERROR09);
			}
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR09, e);
		}
	}

	public void checkNameUnique(String firstname, String lastname) {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.UNIQUE_NAME_QUERY)) {
			
			if (firstname == null || lastname == null) {
				throw new AppException(ErrorCodesvalues.ERROR15);
			}
			int count = 1;
			ps.setString(1, firstname);
			ps.setString(2, lastname);
			ResultSet resultSet = ps.executeQuery();
			
			if (resultSet.next()) {
				count = resultSet.getInt(Constant.COUNT);
			}
			
			if (count != 0) {
				throw new AppException(ErrorCodesvalues.ERROR11);
			}
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR11, e);
		}

	}

	public static void setPersonWithPreparedStatement(PreparedStatement ps, Person person) throws Exception {
		
		ps.setString(1, person.getFirstName());
		ps.setString(2, person.getLastName());
		ps.setString(3, person.getEmail());
		Date birthDate = new Date(person.getBirthdate().getTime());
		ps.setDate(4, birthDate);
	}

	public static Person getPersonWithResultSet(ResultSet resultSet) throws Exception {
		
		Person person = new Person();
		person.setId(resultSet.getLong(Constant.ID));
		person.setFirstName(resultSet.getString(Constant.FIRSTNAME));
		person.setLastName(resultSet.getString(Constant.LASTNAME));
		person.setEmail(resultSet.getString(Constant.EMAIL));
		person.setBirthdate(resultSet.getDate(Constant.BIRTH_DATE));
		return person;
	}
}
