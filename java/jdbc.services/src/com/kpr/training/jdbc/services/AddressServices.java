package com.kpr.training.jdbc.services;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import com.kpr.training.jdbc.constant.Constant;
import com.kpr.training.jdbc.constant.QueryStatement;
import com.kpr.training.jdbc.exception.AppException;
import com.kpr.training.jdbc.exception.ErrorCodesvalues;
import com.kpr.training.jdbc.model.Address;

public class AddressServices {
	public long addressId;
	public long uniqueAddressValue;

	public long create(Address address) {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.INSERT_ADDRESS_QUERY, Statement.RETURN_GENERATED_KEYS)) {

			postalCodeIsNotEmpty(address.getPostalCode());
			setAddressWithPreparedStatement(ps, address);
			uniqueAddressValue = checkUniqueAddress(address);
			ResultSet resultSet;
			
			if (uniqueAddressValue != 0) {
				return uniqueAddressValue;
			}
			
			if (ps.executeUpdate() == 1 && (resultSet = ps.getGeneratedKeys()).next()) {
				return resultSet.getLong(Constant.GENERATED_ID);
			}
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR03, e);
		}
		return 0;
	}

	public long update(Address address) {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.UPDATE_ADDRESS_QUERY)) {
			postalCodeIsNotEmpty(address.getPostalCode());
			setAddressWithPreparedStatement(ps, address);
			ps.setLong(4, address.getId());
			
			if (ps.executeUpdate() == 0) {
				throw new AppException(ErrorCodesvalues.ERROR05);
			}
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR05, e);
		}	
		return address.getId();
	}

	public Address read(long id) {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.READ_ADDRESS_QUERY)) {
			ps.setLong(1, id);
			ResultSet resultSet = ps.executeQuery();
			
			if (!resultSet.next()) {
				return null;
			}
			return getAddressWithResultSet(resultSet);
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR16, e);
		}
	}

	public ArrayList<Address> readAll() {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.READ_ALL_ADDRESS_QUERY)) {
			ArrayList<Address> addressList = new ArrayList<Address>();
			ResultSet resultSet = ps.executeQuery();
			
			while (resultSet.next()) {
				addressList.add(getAddressWithResultSet(resultSet));
			}
			return addressList;
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR16, e);
		}
	}

	public static void delete(long id) {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.DELETE_ADDRESS_QUERY)) {
			ps.setLong(1, id);
			
			if (ps.executeUpdate() == 0) {
				throw new AppException(ErrorCodesvalues.ERROR19);
			}
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR19, e);
		}
	}

	public ArrayList<Address> addressSearch(Address address) {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.SEARCH_ADDRESS_QUERY)) {
			ArrayList<Address> addressList = new ArrayList<Address>();
			
			if (address.getStreet() == null && address.getCity() == null && address.getPostalCode() == 0) {
				throw new AppException(ErrorCodesvalues.ERROR10);
			}
			setAddressWithPreparedStatement(ps, address);
			
			if (address.getStreet() != null || address.getCity() != null || address.getPostalCode() != 0) {
				ResultSet resultSet = ps.executeQuery();
				
				while (resultSet.next()) {
					addressList.add(getAddressWithResultSet(resultSet));
				}
			}
			return addressList;

		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR20, e);
		}

	}

	public long checkUniqueAddress(Address address) {
		
		ResultSet resultSet = null;
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.UNIQUE_ADDRESS_QUERY)) {
			
			setAddressWithPreparedStatement(ps, address);
			resultSet = ps.executeQuery();
			
			if (!resultSet.next()) {
				return 0;
			}
			return resultSet.getLong(Constant.ID);
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR18, e);
		}
	}

	public void postalCodeIsNotEmpty(int postalcode) throws Exception {
		
		if (postalcode == 0) {
			throw new AppException(ErrorCodesvalues.ERROR02);
		}
	}

	public static void unUsedAddress() {
		
		try (PreparedStatement ps = ConnectionService.getConnection()
				.prepareStatement(QueryStatement.UNUSED_ADDRESS_QUERY)) {
			
			ResultSet resultSet = ps.executeQuery();
			
			while (resultSet.next()) {
				delete(resultSet.getLong(Constant.ID));
			}
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR18, e);
		}

	}

	public static void setAddressWithPreparedStatement(PreparedStatement ps, Address address) throws Exception {
		
		ps.setString(1, address.getStreet());
		ps.setString(2, address.getCity());
		ps.setInt(3, address.getPostalCode());
	}

	public static Address getAddressWithResultSet(ResultSet resultSet) throws Exception {
		
		Address address = new Address();
		address.setId(resultSet.getLong(Constant.ID));
		address.setCity(resultSet.getString(Constant.CITY));
		address.setStreet(resultSet.getString(Constant.STREET));
		address.setPostal_code(resultSet.getInt(Constant.POSTAL_CODE));
		return address;
	}
}