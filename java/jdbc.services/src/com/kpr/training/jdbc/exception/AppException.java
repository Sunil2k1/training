package com.kpr.training.jdbc.exception;
/* Create a AppException
 
Entity
 AppException
  
Method Signature
 * public AppException();
 * public AppException(ErrorCodes error);
 * public String getErrorMessage();

Jobs to be Done
 * Create a variable called code of int type
 * Create a variable called message of type String
 * Create a empty paramater constructor 
 * Create a constructor with ErrorCodesvalues type code as parameter
 * Create a method called getErrorMessage that return String and return message
  
Pseudo Code
public class AppException extends Exception{
	public String message;
	public AppException() {
		super();
	}
    public AppException(ErrorCodesvalues error) {
    	super(error.getMessage());
	}
}
 */	

@SuppressWarnings("serial")
public class AppException extends RuntimeException{
	public AppException() {
		super();
	}

    public AppException(ErrorCodesvalues error) {
    	super(error.getMessage());
	}
    public AppException(ErrorCodesvalues error,Exception e) {
    	super(error.getMessage(), e);
	}
}
	