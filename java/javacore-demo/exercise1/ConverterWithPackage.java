/* Importing Calculation Package ,It also Include Distance Package Automaticaly.
 * In this We are using While Condition,Switch case and using Break statement.
 * By using While Condition We Can Run the Code n number of times without closing.
 * By using This Code We can Convert the Distance
*/
import java.util.Scanner;                 //importing Scanner
import ent.Calculation;                            //importing Calculation package
public class ConverterWithPackage {
    public static void main (String args[]) {
    Scanner sc=new Scanner(System.in);
    System.out.println("Distance Converter");
    Calculation cal=new Calculation(0.0f,0.0f,0.0f);
    int condition = 1;        
    while(condition == 1) {                       //using while condition
        System.out.println("1:KM to Metre,2:Mile to KM,3:KM to Mile");
        int ch = sc.nextInt();
        switch(ch) {                             //using switch case
                case 1:
                  System.out.println("Enter the KiloMetre value ");
                  cal.KM = sc.nextFloat();
                  cal.calm();
                  System.out.println("The Value of Metre is "+cal.Metre);
                  break;
                case 2:
                  System.out.println("Enter the Mile value ");
                  cal.Mile = sc.nextFloat();
                  cal.calkm();
                  System.out.println("The Value of KM is "+cal.KM);
                  break;
                case 3:
                  System.out.println("Enter the KM value ");
                  cal.KM = sc.nextFloat();
                  cal.calmile();
  	              System.out.println("The Value of Mile is "+cal.Mile); 
                  break;  
                default:
                  System.out.println("enter a valid number ");
                }
            System.out.println("IF you Want to Continue Press 1 or to Exit Press 0 : ");
            condition = sc.nextInt();
               
            }

        } 
 }

