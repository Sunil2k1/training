/*This code is to converet from km to metre or mile to km or km to mile without using any packages 
 * In this We are using While Condition,Switch case and using Break statement.
 * By using While Condition We Can Run the Code n number of times without closing.
 * By using This Code We can Convert the Distance
*/
import java.util.Scanner;                          //importing Scanner
public class Converter {
    public static void main (String args[]) {
        float Metre,KM,Mile;    
        Scanner sc=new Scanner(System.in);
        System.out.println("Distance Converter");
        int condition = 1;        
        while(condition == 1) {                       //using while condition
           System.out.println("1:KM to Metre,2:Mile to KM,3:KM to Mile");
           int ch = sc.nextInt();
           switch(ch) {                             //using switch case
                case 1:
                  System.out.println("Enter the KiloMetre value ");
                  KM = sc.nextFloat();
                  Metre = KM * 1000f;
                  System.out.println("The Value of Metre is "+Metre);
                  break;
                case 2:
                  System.out.println("Enter the Mile value ");
                  Mile = sc.nextFloat();
                  KM = Mile * 1.6f;
                  System.out.println("The Value of KM is "+KM);
                  break;
                case 3:
                  System.out.println("Enter the KM value ");
                  KM = sc.nextFloat();
                  Mile = KM / 1.6f;
  	              System.out.println("The Value of Mile is "+Mile); 
                  break;  
                default:
                  System.out.println("enter a valid number ");
                }
            System.out.println("IF you Want to Continue Press 1 or to Exit Press 0 : ");
            condition = sc.nextInt();
               
            }

        } 
 }

