/*In package ent this code get stored
 *It is inhereted from Distance class
 *In this we using public Class.
 *In this we are using inner class or Sub class.
 *In this Code we are Mainly Calculate the given Distance
*/
package ent;
public class Calculation extends Distance{
    public float Metre,KM,Mile;
    public Calculation(float Metre,float KM,float Mile)
    {  super(Metre,KM,Mile);
     
    }
    public void calm() {
        this.Metre = KM * 1000f;
    }
    public void calkm() {
        this.KM = Mile * 1.6f;
    }
    public void calmile() {
        this.Mile = KM / 1.6f;
    }
     public float getMetre() {
      return Metre ;
    }
    public float getKM() {
      return KM;
    }
    public float getMile() {
      return Mile;
    }
}