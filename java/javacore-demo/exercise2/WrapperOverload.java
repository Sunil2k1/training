/*
   Overloading with Wrapper types
    
Entity
    * Wrapper Class
    * WrapperOverLoad Class
    
Jobs to be done
    * Create class Wrapper
    * Inside wrapIntSetter(int a)
       => print a
    * Inside wrapcharSetter(char b)
       => print b
    * Inside wrapDoubleSetter(double c)
       => print c
    * Create public class  WrapperOverload
       => Declare and Initialize number is equal to 65
       => Declare and Initialize letter is equal to 'S'    
       => Declare and Initialize decimal is equal to 3.14
       => Call the method wrapIntSetter(number)
       => Call the method wrapcharSetter(letter)
       => Call the method wrapDoubleSetter(decimal)         
*/

class Wrapper {
    void wrapIntSetter(int a) {
        System.out.println("int Value changed to Integer "+a);
    }
    
    void wrapcharSetter(char b) {
        System.out.println("char Value changed to Character "+b);
    }
    
    void wrapDoubleSetter(double c) {
        System.out.println("double Value changed to Double "+c);
    }
}

public class WrapperOverload {
    public static void main(String[] args) {
        //Overloading Wrapper types//
        
        Integer number = 65;
        Character letter = 'S';
        Double decimal = 3.14;
        Wrapper wrap = new Wrapper();
        wrap.wrapIntSetter(number);
        wrap.wrapcharSetter(letter);
        wrap.wrapDoubleSetter(decimal);
    }
}