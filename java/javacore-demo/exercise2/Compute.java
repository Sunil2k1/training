/*+ Write a program that computes your initials from your full name and displays them.

Requirements:
  * String name
  * String Buffer
Entities:
  * Compute
Function Declaration:
  -none-
Jobs to be done:
  * Create a class name Compute
  * In public static void main
      => Declare and Initialize name
      => Create a new object for StringBuffer name initial
      => using length() method find length of string and store in length
      => For each letter
         -> check the letter is in upper case using isUpperCase method
         -> If condition is satisfied append the letter to initial
      => print initial
  * In isUpperCase Method
      => return false .       

code:*/
public class Compute{
    public static void main(String [] args) {
         String name = "sunil.G";
         StringBuffer initial = new StringBuffer();
         int length = name.length();
         for(int i = 0;i < length;i++){
             if(Character.isUpperCase(name.charAt(i))){
                 initial.append(name.charAt(i));
             }
         } 
         System.out.println("My Initial is "+initial);         
    }

	private static boolean isUpperCase(char charAt) {
		// TODO Auto-generated method stub
		return false;
	}
}