/* compare the enum values using equal method and == operator
Requirements :
  * Season,
  * Month
Entities:
  * Enumvalues
Function Declaration:
  * public enum Season()
  * public enum Month()
Job to be done :
 * Create a class name Enumvalues
 * create a method name Season and enter values.
 * create a method name Month and enter values.
 * Inside public static void main
   => comparing two enum operator using equals() method and print it
   => comparing two enum operator using == operator and print it.
*/
//Code:
class Enumvalues{
    public enum Season {
        SUMMER,
        WINTER,
        AUTUMN,
        SPRING
    }
    public enum Month{
        JAN,
        FEB,
        MARCH,
        APRIL,
        MAY,
        JUNE,
        JULLY,
        AUGUST,
        SEPT,
        OCT,
        NOV,
        DEC
    }
    public static void main(String [] args) {
        //comparing two enum operator using equals() method.
        System.out.println(Month.MAY.equals(Season.SUMMER));
        //comparing two enum operator using == operator.
        boolean result = Season.SUMMER == Season.WINTER;
        System.out.println(result);
        
    }
}