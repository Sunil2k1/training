/*+ demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects

code using inheritance, overloading, overriding using Animal, Dog class object:


Requirements :
  *  dogname
  *  catname
  *  snakename
  *  dogcount
  *  catcount
  *  snakecount
Entities:
  *  Animal
  *  Dog
  *  Cat
  *  Snake
  *  Inheritance
Function Declaration:
  * public void habitate()
  * public void sigAmtDog()
  * public void sigAmtCat()
  * public void sigAmtSnake()
Job to be done :
  * Create a class name Animal.
     => Declare dogname in String as static variable 
     => Declare catname in String as static variable
     => Declare snakename in String as static variable
     => In method habitate() 
         -> print Domestic Animal
  * Create class name Dog which is extends from Animal
     => Declare dogcount in integer as static variable    
     => In method sigAmtDog() 
         -> return 3000
  * Create class name Cat which is extends from Animal
     => Declare catcount in integer as static variable    
     => In method sigAmtCat() 
         -> return 2000
  * Create class name Snake which is extends from Animal
     => Declare snakecount in integer as static variable    
     => In method sigAmtSnake() 
         -> return 3500       
  * Create public class name Inheritance
     => Initialize dogcount =20
     => Create a object for Dog class name d
     => print the value which return from sigAmtDog() method
     => Create a object for Animal class name a
     => call the method habitate() method              
        
*/
class Animal{
    static String dogname;
    static String catname;
    static String snakename;
    public void habitate(){
        System.out.println("Domestic Animal");
    }

}
class Dog extends Animal{
    static int dogcount;
    public int sigAmtDog() {
        return 3000; 
    }    
}
class Cat extends Animal{
    static int catcount;
    public int sigAmtcat() {
        return 2000; 
    }    
}
class Snake extends Animal{
    static int snakecount;
    public int sigAmtSnake() {
        return 3500; 
    }    
}
public class Inheritance {
    public static void main(String [] args) {
        int dogcount = 20;    
        // method overloading    
        Dog d = new Dog();
        System.out.println(d.sigAmtDog()); 
        //method overriding
        Animal cat = new Animal();
        cat.habitate();
        
    }
}

/*Output:
java Demo
Process started (PID=14712) >>>
3000
Domestic Animal
<<< Process finished (PID=14712). (Exit code 0)
================ READY ================*/