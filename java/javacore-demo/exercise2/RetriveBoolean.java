/*Program to store and retrieve four boolean flags using a single int.

Requirement:
    * Program to store and retrieve four boolean flags using a single int.
Entities:
    * RetriveBoolean
Function Declaration:
    -none-
Jobs to be done:
     * Create a class called RetriveBoolean with main method and using boolean initialize a value.
     * initialize res equal to "Fail" as String
     * check if res equal to "Pass"
         => print flag
     * else
         => flag =! flag
         => print flag      
*/  
            
public class RetriveBoolean {
    public static void main(String[] args) {
        //To Invert the Value of the Boolean//
        boolean flag = false;
        String res = "Fail";
        if(res == "Pass") {
            System.out.println(flag);
        } else {
            flag = !flag;
            System.out.println(flag);
        }
    }
}