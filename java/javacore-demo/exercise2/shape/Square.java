/*Requirement:
    * length
Entities:
    * Square
Function Declaration:
    * squareArea() 
Jobs to be done:
   * Create class Square which is extends from Shape
   * Declare area as float
   * In squareArea() method
        => area is equal length * length
        =>print area
 */
package shape; //package name shape
public class Square extends Shape {   
    public float area;    
    public void squareArea() {
          area = length * length;
          System.out.println(area);          
    }
}  