/*
Requirement:
    * width
    * height
Entities:
    * Shape
Function Declaration:
    * public void squareArea(float length)
    * public void circleArea()
    * public void circlePerimeter()
Jobs to be done:
   * Create abstract class Shape
   * Declare length,radius
   * In squareArea(float length) method
        => print "Area of the Square"
   * In circleArea() method
        => print "Area of the Circle"        
   * In circlePerimeter() method
        => print "Perimeter of the Circle"                
*/
package shape; //package name shape
public abstract class Shape{   //abstract class
    public float length;
    public float radius;
    public void squareArea(float length) {
          System.out.println("Area of the Square");          
    }
    public void circleArea() {
          System.out.println("Area of the Circle");          
    }
    public void circlePerimeter() {
          System.out.println("Perimeter of the Circle");          
    }  
}    
    
 
