/*
Requirement:
    * radius
Entities:
    * Circle
Function Declaration:
    * circleArea() 
    * circlePerimeter()
Jobs to be done:
   * Create class Circle which is extends from Shape
   * Declare area and perimeter as float
   * In circleArea() method
        => area is equal to 2*3.14*radius*radius
        =>print area
   * In circlePerimeter() method
        => area is equal to 2*3.14*radius
        =>print perimeter        
        
*/
package shape;  //package name shape
public class Circle extends Shape {   
    public float area;
    public float perimeter; 
    public void circleArea() {
       area = 2 * 3.14f * radius * radius;
       System.out.println("Area of the circle is "+area);   
    }  
    public void circlePerimeter() {
       perimeter = 2 * 3.14f * radius;
       System.out.println("Perimeter of the circle is "+perimeter);        
    }    
}   