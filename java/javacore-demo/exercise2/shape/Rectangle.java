/*
Requirement:
    * width
    * height
Entities:
    * Rectangle
Function Declaration:
    * area()
Jobs to be done:
   * Create class Rectangle which is extends from Shape
   * Declare width,height and area 
   * In area() method
        => area is equal width * height
        => return area
        
*/
package shape;    
    public class Rectangle {
        public int width ;
        public int height;
        public int area;
        public int area() {
              area = width * height;
              return area;
        }
    }