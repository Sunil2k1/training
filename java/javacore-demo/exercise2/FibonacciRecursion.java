/*+ print fibinocci using for loop, while loop and recursion

Requirements :
  * three numbers
Entities:
 * FibonacciRecursion
Function Declaration:
 * printFib(int count);
Job to be done :
 * create a class name FibonacciRecursion.
 * In printFib(int count) method
    =>  * Declare and initialize values for n1=0,n2=1,n3=0
    => check count > 0
      -> n3 is equal to n1 + n2
      -> n1 is equal to n2
      -> n2 is equal to n3
      -> print n3
      -> call printFib(count-1) method
 * In public static void main
    => Declare and initialize values for count = 10
    => print n1 and n2
    => call printFib(count-2) method

Fibinocci using recursion:*/
class FibonacciRecursion{
    static int n1 = 0,n2 =1 ,n3=0;
    static void printFib(int count) {
        if(count > 0) {
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            System.out.println(n3);
            printFib(count-1);
        }
    }
    public static void main(String [] args) {
        int count = 10;
        System.out.println(n1+" "+n2);
        printFib(count-2);
    }
}

/*output:
Process started (PID=9420) >>>
0 1
1
2
3
5
8
13
21
34
<<< Process finished (PID=9420). (Exit code 0)
================ READY ================*/