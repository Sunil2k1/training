/*sort and print following String[] alphabetically ignoring case. Also convert and print even indexed Strings into uppercase
       { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }

Requirement:
    Sort and print following array alphabetically ignoring case.
    Also convert and print even indexed Strings into uppercase.

Entity:
    SortArrays
Function Declaration:
    No Function
Jobs to be done:
   * Create a class name SortArray
   * Declare and Initialize a String[] name districts =  { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" }
   * for each districts
      => print districts
   * print "Even indexed districts"
   * for each districts
      => check if i modulo of  2 is equal to 0
          -> convert all letters to uppercase by using  toUpperCase() method
          -> print district
      
*/
import java.util.Arrays;
class SortArrays {
    public static void main(String[] args) {
        String[] districts = { "Madurai", "Thanjavur", "TRICHY", "Karur", "Erode", "trichy", "Salem" };
        Arrays.sort( districts, 0, districts.length );
        for (String i : districts) {
            System.out.print(i + " ");
        }
        System.out.println("\nEven indexed districts:");
        for(int i = 0; i < districts.length; i++) {
            if(i % 2 == 0) {
                String district = districts[i].toUpperCase();
                System.out.print(district + " ");
            }
        }
    }
}

