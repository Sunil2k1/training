/*+ Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively
    
Requirements :
  * radius of the circle
  * length of the square    
Entities:
  * Square
  * Circle 
  * AbstractDemo
 Function Declaration:
  * public void  squareArea()
  * public void  circleArea()
  *  public void  circlePerimeter()    
 Work to be done:
  * Create a class name AbstractDemo
  * Declare and initialize a = 0 ;
  * Using While Loop 
     => check if a =0
     => Get Input from user to choose Circle or Square
     => Using Switch case
        >> Case 1: Circle
          -> Get the radius value.
          -> Create a Object for Circle class name circle
          -> Call the method circleArea and circlePerimeter
          -> use break Statement.
        >> Case 2: Square
          -> Get the length value.
          -> Create a Object for Square class name square
          -> Call the method squareArea .
          -> use break Statement    
        >> Default:
          -> Print Statement like "Enter a valid number".
     => Get the value if the program runs again.         
    */



import java.util.Scanner;
import shape.Square;
import shape.Circle;
public class AbstractDemo{
    public static void main(String [] args){
    int a = 0;
    while(a == 0) {
            Scanner sc=new Scanner(System.in);
            System.out.println("1.Circle, 2.Square");
            int ch=sc.nextInt();
            switch(ch){
                  case 1:
                     Circle circle =new Circle();
                     System.out.println("enter a radius: ");
                     circle.radius=sc.nextFloat();
                     circle.circleArea();
                     circle.circlePerimeter();
                     break;     
                  case 2:
                     Square square =new Square();
                     System.out.println("enter a length: ");
                     square.length=sc.nextFloat();
                     square.squareArea();
                     break;  
                 default:
                     System.out.println("Enter a valid number");                 
            }
            System.out.println("If you want to continue enter 0 or exit enter 1");
            a = sc.nextInt();
      }        
    }    
}
