/*+ print fibinocci using for loop, while loop and recursion

Requirements :
  * three numbers
Entities:
  * FibonacciRecursion
Function Declaration:
 -none-
Job to be done :
  * create a class name FibonacciRecursion.
 * Declare and initialize values for n1=0,n2=1,n3=0,count = 10
 * print n1 and n2
 * while check count > 0
    => n3 is equal to n1 + n2
    => n1 is equal to n2
    => n2 is equal to n3
    => print n3
Fibinocci using while loop:
*/
class FibonacciWhile{
    public static void main(String [] args) {
        int n1 = 0,n2 = 1 ,n3 = 0,i = 1;
        int count = 10;
        System.out.println(n1+" "+n2);
        while(i < count) {
              n3 = n1 + n2;
              n1 = n2;
              n2 = n3;
              System.out.println(n3);
              i++;
        }   
    }
}

/*output:
Process started (PID=9420) >>>
0 1
1
2
3
5
8
13
21
34
<<< Process finished (PID=9420). (Exit code 0)
================ READY ================*/