/*Print the absolute path of the .class file of the current class 


Requirements :
    * Print the absolute path of the .class file of the current class.
Entities:
    * AbsolutePath
Function Declaration:
    * public AbsolutePath()
Jobs to be done:
  * Create a Class name AbsolutePath
  * In AbsolutePath Method 
       => Invoke getClass method and get ClassLoader method and store it in path.
       => Invoke getResource method and store it in the current.
       => Print current.
  * In Public static void main
       => Invoke the AbsolutePath method and store it in path.
  *  */

import java.net.*;

class AbsolutePath {
   public AbsolutePath() {
      ClassLoader path = this.getClass().getClassLoader();
      URL current = path.getResource("AbsolutePath.class");
      System.err.println("Url=" + current);
   }
   public static void main( String[] args ) {
      AbsolutePath path = new AbsolutePath();
   }
}