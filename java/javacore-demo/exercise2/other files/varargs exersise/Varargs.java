/*+ demonstrate overloading with varArgs

code:

public class Varargs{
    public static void main(String [] args) {
        play();
    }
    //varargs method using datatype float
    static void play(float..a) {
        System.out.println("float");
    }
    //varargs method using datatype int
    static void play(int..a) {
        System.out.println("int");
    }
    //varargs method using double datatype 
    static void play(double..a) {
        System.out.println("double");
    }    
}*/