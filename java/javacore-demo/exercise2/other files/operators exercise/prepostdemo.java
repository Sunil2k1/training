/*+ In the following program, explain why the value "6" is printed twice in a row:
       class PrePostDemo {
           public static void main(String[] args){
               int i = 3;
               i++;
               System.out.println(i);    // "4"
               ++i;
               System.out.println(i);    // "5"
               System.out.println(++i);  // "6"
               System.out.println(i++);  // "6"
               System.out.println(i);    // "7"
           }
       }
Requirements :
  i =3
Entitties:
 PrePostDemo
Function Declaration:
 -none-
Job to be done :
  * Create a class with name PrePostDemo.
  * Put public static void main.
  * Inside that declare that i with int datatype and intialize i = 3,increment i++ and print result.
  * And then increment the ++i.
  * And then print as i,++i,i++,i.

explain why the value "6" is printed twice in a row:

* i = 5 when ++i increments i and returns that value so ++i gives the value 6.
* i = 6 when i++ returns i value and then Incrementents i, so  ++i gives the same value 6. */