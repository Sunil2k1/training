/*+ Show two ways to concatenate the following two strings together to get the string "Hi, mom.":
    String hi = "Hi, ";
    String mom = "mom.";

Requirements :
  hi =Hi,
  mom = "mom."
Entitties:
 -none-
Function Declaration:
 -none-
Job to be done :
  * declare hi with string datatype and initialize the value.
  * declare mom with string datatype and initialize the value.


ans:
  hi.concat(mom); ( or) hi+mom;*/