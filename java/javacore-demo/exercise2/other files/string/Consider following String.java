/*+ Consider the following string:
    String hannah = "Did Hannah see bees? Hannah did.";
    - What is the value displayed by the expression hannah.length()?
    - What is the value returned by the method call hannah.charAt(12)?
    - Write an expression that refers to the letter b in the string referred to by hannah.


Requirements :
  tring hannah = "Did Hannah see bees? Hannah did."
Entitties:
 -none-
Function Declaration:
 -none-
Job to be done :
 * Declare that hannah with String datatype and intialize hannah is equal to Did Hannah see bees? Hannah did.




What is the value displayed by the expression hannah.length()?
ans: 32

What is the value returned by the method call hannah.charAt(12)?
ans: e

Write an expression that refers to the letter b in the string referred to by hannah.
ans: hannah.charAt(15)*/