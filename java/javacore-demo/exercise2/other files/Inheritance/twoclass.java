/*+ Consider the following two classes:
    public class ClassA {
        public void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public static void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }

    public class ClassB extends ClassA {
        public static void methodOne(int i) {
        }
        public void methodTwo(int i) {
        }
        public void methodThree(int i) {
        }
        public static void methodFour(int i) {
        }
    }
    a. Which method overrides a method in the superclass?
    b. Which method hides a method in the superclass?
    c. What do the other methods do?


Requirements :
  result = 1 + 2
Entitties:
 ClassA
 ClassB 
Function Declaration:
 -none-
Job to be done :
* Creating a public class name ClassA
* Inside that class method with public void  with name methodOne and inside the paranthesis variable i with datatype int.
* Inside that class method with public void  with name methodTwo and inside the paranthesis variable i with datatype int.
* Inside that class method with public void static with name methodThree and inside the paranthesis variable i with datatype int.
* Inside that class method with public void static with name methodFour and inside the paranthesis variable i with datatype int.
* Creating a public class name ClassB which extends ClassA.
* Inside that class method with public void static with name methodOne and inside the paranthesis variable i with datatype int.
* Inside that class method with public void  with name methodTwo and inside the paranthesis variable i with datatype int.
* Inside that class method with public void  with name methodThree and inside the paranthesis variable i with datatype int.
* Inside that class method with public void static with name methodFour and inside the paranthesis variable i with datatype int.


a. Which method overrides a method in the superclass?
ans: methodTwo
b. Which method hides a method in the superclass?
ans: methodFour
c. What do the other methods do?
ans: it cause compile time error*/