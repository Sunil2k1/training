/* Is the following interface valid?
    public interface Marker {}

Requirement:
   -To check interface valid or not
Entity:
    -No Class
Function Declaration:
    -public interface Marker
Jobs to be done:
    Create an interface without any methods.
    
Is the following interface valid?
    public interface Marker {}
Answer : Yes. The above following interface is valid.Methods are not required because empty 
interfaces can be used as types and to mark classes without requiring any particular method 
implementations.*/