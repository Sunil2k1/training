/*Program to store and retrieve four boolean flags using a single int.

Requirements :
    * Program to store and retrieve four boolean flags using a single int.
Entities:
    * BooleanFlag
Function Declaration:
    -none-
Jobs to be done:
   * Create a class called BooleanFlag.
   * Declare main method inside the class and declare five integer variables apple,mango,banana,
watermelon,orange  and initialize a values.
   * check for the fruits 
     => Check if Apple is present and print Found apple if it is present or else print NO apple .
     => Check if Mango is present and print Found Mango if it is present or else print NO Mango .
     => Check if Banana is present and print Found Banana if it is present or else print NO Banana . 
     => Check if watermelon is present and print Found watermelon if it is present or else print NO watermelon .
     => Check if orange is present and print Found orange if it is present or else print NO orange.
*/

public class BooleanFlag {
    public static void main(String[] args) {
        // define fruits
        int apple = 1;
        int mango = 2;
        int banana = 4;
        int watermelon = 8;
        int orange = 16;
        System.out.println("Weapons defined: apple,mango,banana,watermelon,orange");
        
        // create fruit bag
        int myFruitBag;
        System.out.println("Initiating fruit bag...");
        
        // store fruit in bag
        System.out.println("Storing a few fruits in bag (apple, mango, watermeloon)...");
        myFruitBag = apple | mango | watermelon;
        
        // check for fruits
        System.out.println("Looking for fruits...");
        System.out.println((myFruitBag & apple) > 0 ? "--> Found apple" : "NO apple");
        System.out.println((myFruitBag & mango) > 0 ? "--> Found mango" : "NO mango");
        System.out.println((myFruitBag & banana) > 0 ? "--> Found banana" : "NO banana");
        System.out.println((myFruitBag & watermelon) > 0 ? "--> Found watermelon" : "NO watermelon");
        System.out.println((myFruitBag & orange) > 0 ? "--> Found orange" : "NO orange");
    }
}