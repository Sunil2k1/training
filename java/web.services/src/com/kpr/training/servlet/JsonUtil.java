package com.kpr.training.servlet;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class JsonUtil {
	public Object objectConversion(String jsonString,Object classObject) {
		 Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd").create();
		return gson.fromJson(jsonString, classObject.getClass());
	}
	public String stringConversion(Object json ) {
		Gson gson = new Gson();		 
		String jsonString = gson.toJson(json);
		return jsonString;
	}

	/*public static void main (String [] args) {
	     Address address = new Address("kolkatta","gandhi road",600123);
		Person person = new Person("Sunil", "Kumar", "Sunil@hotmail.com", PersonValidator.dateFormate("21-05-2001"),
				address);
        
		Gson gson = new Gson();
		 
		String jsonString = gson.toJson(person);
		 
		System.out.println(jsonString);*/
		
		/*String jsonString = "{\"id\":0,\"firstname\":\"Sunil\",\"lastname\":\"Kumar\",\"email\":\"Sunil@hotmail.com\",\"address\":{\"id\":0,\"street\":\"kolkatta\",\"city\":\"gandhi road\",\"postalCode\":600123},\"birthDate\":\"May 21, 2001 12:00:00 AM\"}\r\n" + 
				"";
        
		Gson gson = new Gson();
		 
		Person empObject = gson.fromJson(jsonString, Person.class);
		 
		System.out.println(empObject);
	}*/
}
