package com.kpr.training.servlet;



import java.io.BufferedReader;
import java.io.PrintWriter;

import com.kpr.training.services.ConnectionService;
import com.kpr.training.model.Person;
import com.kpr.training.services.*;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class PersonServlet extends HttpServlet
{   Person person = new Person();
	PersonServices personService = new PersonServices();
	protected void doPost(HttpServletRequest req, HttpServletResponse res) {
    	
    	try {
    		try (BufferedReader reader = req.getReader()) {
    		      
    		      StringBuffer personJson = new StringBuffer();
    		      String line = null;

    		      while ((line = reader.readLine()) != null) {
    		        personJson.append(line);
    		      }

    		      JsonUtil jsonUtil = new JsonUtil();
    		      long personId = personService.create((Person)jsonUtil.objectConversion(personJson.toString(),person));
				  PrintWriter writer = res.getWriter();
    				if(personId > 0) {
    					ConnectionService.commitRollback(true);
    					writer.append("Person Creation Success : " + personId);
    				} else {
    					ConnectionService.commitRollback(false);
    					writer.append("Person Creation Failed");
    				}
    		      
    		      
    		    }
			
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    }
	
	protected void doPut(HttpServletRequest req, HttpServletResponse res) {
    	
    	try {
    		try (BufferedReader reader = req.getReader()) {   		      
    			  StringBuffer personJson = new StringBuffer();
    		      String line = null;
    		      while ((line = reader.readLine()) != null) {
    		        personJson.append(line);
    		      }
    		      JsonUtil jsonUtil = new JsonUtil();
    		      personService.update((Person)jsonUtil.objectConversion(personJson.toString(),person));
    		      ConnectionService.commitRollback(true);
    		      PrintWriter writer = res.getWriter();
    		      writer.append("Person Update Success ");
    		    }
			
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    }
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
    	try {
    		      JsonUtil jsonUtil = new JsonUtil();
    		      String personJson = jsonUtil.stringConversion(personService.read(Long.parseLong(req.getParameter("id")), 
    		    		  Boolean.parseBoolean(req.getParameter("includeAddress")))); 
    		      PrintWriter writer = res.getWriter();
    		      writer.append("Person read Success  "+personJson);		
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
	}
	
	protected void doDelete(HttpServletRequest req, HttpServletResponse res) {

		try {
			personService.delete(Long.parseLong(req.getParameter("id")));
			ConnectionService.commitRollback(true);
			PrintWriter writer = res.getWriter();
			writer.append("Person Delete Successfully");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
