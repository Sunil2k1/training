package com.kpr.training.servlet;

import java.io.BufferedReader;
import java.io.PrintWriter;

import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.kpr.training.model.Address;
import com.kpr.training.services.AddressServices;
import com.kpr.training.services.ConnectionService;

public class AddressServlet extends HttpServlet{
	Address address = new Address();
	AddressServices as = new AddressServices();
	protected void doPost(HttpServletRequest req, HttpServletResponse res) {
    	
    	try {
    		try (BufferedReader reader = req.getReader()) {
    		      
    		      StringBuffer addressJson = new StringBuffer();
    		      String line = null;

    		      while ((line = reader.readLine()) != null) {
    		    	  addressJson.append(line);
    		      }

    		      JsonUtil jsonUtil = new JsonUtil();
    		      
    		      long addressId = as.create((Address)jsonUtil.objectConversion(addressJson.toString(),address));
    		      PrintWriter writer = res.getWriter();
  				if(addressId > 0) {
					ConnectionService.commitRollback(true);
					writer.append("Address Creation Success : " + addressId);
				} else {
					ConnectionService.commitRollback(false);
					writer.append("Address Creation Failed");
				}
    		    }
			
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    }
	
	protected void doPut(HttpServletRequest req, HttpServletResponse res) {
    	
    	try {
    		try (BufferedReader reader = req.getReader()) {   		      
    			  StringBuffer addressJson = new StringBuffer();
    		      String line = null;
    		      while ((line = reader.readLine()) != null) {
    		    	  addressJson.append(line);
    		      }
    		      JsonUtil jsonUtil = new JsonUtil();
    		      as.update((Address)jsonUtil.objectConversion(addressJson.toString(),address));
    		      ConnectionService.commitRollback(true);
    		      PrintWriter writer = res.getWriter();
    		      writer.append("Person Update Success ");
    		    }
			
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
    	
    }
	
	protected void doGet(HttpServletRequest req, HttpServletResponse res) {
    	try {
    		      JsonUtil jsonUtil = new JsonUtil();
    		      String personJson = jsonUtil.stringConversion(as.read(Long.parseLong(req.getParameter("id")))); 
    		      PrintWriter writer = res.getWriter();
    		      writer.append("Person read Success  "+personJson);		
    	} catch (Exception e) {
    		e.printStackTrace();
    	}
	}
	
	protected void doDelete(HttpServletRequest req, HttpServletResponse res) {

		try {
			AddressServices.delete(Long.parseLong(req.getParameter("id")));
			ConnectionService.commitRollback(true);
			PrintWriter writer = res.getWriter();
			writer.append("Person Delete Successfully");			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
