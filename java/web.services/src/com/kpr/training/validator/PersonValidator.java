package com.kpr.training.validator;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.kpr.training.exception.AppException;
import com.kpr.training.exception.ErrorCodesvalues;

public class PersonValidator {
	public static Date birthDate;

	public static Date dateFormate(String birthdate) {
		
		try {
			
			SimpleDateFormat sdformat = new SimpleDateFormat("dd-MM-yyyy");
			sdformat.setLenient(false);
			birthDate = sdformat.parse(birthdate);
			return birthDate;
			
		} catch (Exception e) {
			throw new AppException(ErrorCodesvalues.ERROR13, e);
		}
	}
}
