package com.kpr.training.model;

/*  POJO Class for Person
Entity
  * Person
 
Method Signature
* public public Person(String name, String email, Date birth_date);
* public long getId();
* public String getName();
* public String getEmail();
* public long getAddressId();
* public Date getBirthDate();
* public Date getCreatedDate()

Jobs to be Done
* Create a id field of long type
* Create a name field of String type
* Create a email field of String type
* Create a address_id of int type
* Create a birth_date of Date type
* Create a created_date of Date type.
* Create a toString() method and return id,firstname,lastname,email,address_id,birth_date.
* Generate Constructors using fields.
* Create a public constructor take all the Address fields
* Create a name getter of String return type
* Create a email getter of String return type
* Create a id getter of long type
* Create a address_id of long type
* Create a birth_date of Date type
* Create a created_date of Date type.
Pseudo Code
public class Person {
	public Person(String firstname, String lastname, String email, Date birth_date) {
		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.birth_date = birth_date;
	}
	public Person() {
		super();
		this.id = id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.address_id = address_id;
		this.birth_date = birth_date;
	}
	@Override
	public String toString() {
		return "Person [firstname=" + firstname + "lastname=" + lastname +", email=" + email 
				+ ", birth_date=" + birth_date + "]";
	}
	public long id;
	public String firstname;
	public String lastname;
	public String email;
	public long address_id;
	private Date birth_date;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public String getFirstName() {
		return firstname;
	}
	public String getLastName() {
		return lastname;
	}
	public void setFirstName(String firstname) {
		this.firstname = firstname;
	}
	public void setLastName(String lastname) {
		this.lastname = lastname;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public long getAddress_id() {
		return address_id;
	}
	public void setAddress_id(long address_id) {
		this.address_id = address_id;
	}
	public Date getBirth_date() {
		return birth_date;
	}
	public void setBirth_date(Date birth_date) {
		this.birth_date = birth_date;
	}
}

	*/
import java.util.Date;

public class Person {

	public long id;
	public String firstname;
	public String lastname;
	public String email;
	public Address address;
	private Date birthDate;

	public Person() {
		super();
	}

	public Person(String firstname, String lastname, String email, Date birthDate, Address address) {

		super();
		this.firstname = firstname;
		this.lastname = lastname;
		this.email = email;
		this.birthDate = birthDate;
		this.address = address;
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstname;
	}

	public String getLastName() {
		return lastname;
	}

	public void setFirstName(String firstname) {
		this.firstname = firstname;
	}

	public void setLastName(String lastname) {
		this.lastname = lastname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public Date getBirthdate() {
		return birthDate;
	}

	public void setBirthdate(Date birthdate) {
		this.birthDate = birthdate;
	}

	@Override
	public String toString() {
		return new StringBuilder("Person [id=")
						.append(id)
                		.append(", firstname=")
                		.append(firstname)
                		.append(", lastname=")
                		.append(lastname )			                    
                		.append(", email=")
                		.append(email)
                		.append(", birthDate=") 
                		.append( birthDate ).toString(); 
	}


}
