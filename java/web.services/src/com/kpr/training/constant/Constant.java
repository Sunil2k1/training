package com.kpr.training.constant;
public class Constant {
	
	public static final String ID = "id";
	public static final String COUNT = "count";
	public static final String GENERATED_ID = "generated_key";
	public static final String STREET = "street";
	public static final String CITY = "city";
	public static final String POSTAL_CODE = "postal_code";
	public static final String FIRSTNAME = "firstname";
	public static final String LASTNAME = "lastname";
	public static final String EMAIL = "email";
	public static final String BIRTH_DATE = "birth_date";
	public static final String CREATED_DATE = "created_date";
	public static final String URL = "URL";
	public static final String USER = "USER";
	public static final String PASSWORD = "PASSWORD";
	public static final int MAX_THREADS = 3;
}
