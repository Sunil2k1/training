/* Consider a following code snippet 
        Queue bike = new PriorityQueue();    
        bike.poll();
        System.out.println(bike.peek());    

  what will be output and complete the code.
  
  
Output:
java QueueSnippet
Process started (PID=904) >>>
null
<<< Process finished (PID=904). (Exit code 0)
================ READY ================

Requirements :
  what will be output and complete the code.
Entities:
  QueueSnippet
Function Declaration:
 -none-
Job to be done :  
 * Create a class name QueueSnippet
 * Create a Queue name car in String by invoking PriorityQueue() method as String.
 * add  2 elements to car.
 * remove peek element using poll() method
 * print peek element.
 
Pseudocode:
public class QueueSnippet{
   public static void main(String [] args) {
        Queue<String> car = new PriorityQueue<String>(); //creating a queue
        ADD 2 ELEMENTS TO car;
        PRINT containsMazda;
        REMOVE PEEK ELEMENT USING poll() METHOD 
        PRINT PEEK VALUE FOR bike;        
   } 
} 
*/
import java.util.PriorityQueue;
import java.util.Queue;
public class QueueSnippet{
    public static void main(String [] args) {
        Queue<String> bike= new PriorityQueue<String>();
        bike.add("KTM");
        bike.add("Honda");
        bike.poll();
        System.out.println(bike.peek());  
    }
}