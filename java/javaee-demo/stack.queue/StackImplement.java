/*  Create a stack using generic type and implement
  -> Push atleast 5 elements
  -> Pop the peek element
  -> search a element in stack and print index value
  -> print the size of stack
  -> print the elements using Stream
 
Requirements :
  Five elements to insert in the stack
Entities:
  StackImplement 
Function Declaration:
 -none-
Job to be done :  
 * Create a class name StackImplement
 * Create a Stack name fruits  as String.
 * push 5 elements in fruits
 * pop the peek element from fruits.
 * print the index value for a element by using search() method
 * print the size of fruits
 * print fruits using Stream
 
 Psudocode:
 
 public class StackImplement{
   public static void main(String [] args) {
        Stack<String> fruits = new Stack<String>();  //creating a stack
        Stream<String> stream = fruits.stream();  //creating a stream
        PUSH 5 ELEMENTS TO car;
        POP PEEK ELEMENT;
        PRINT INDEX VALUE FOR A ELEMENT;
        PRINT fruits SIZE5
        stream.forEach((element) -> {
             PRINT element;
        });           
   } 
} 
 Code:
*/
import java.util.Stack;
import java.util.stream.*;
public class StackImplement {
    public static void main(String [] args) {
        Stack<String> fruits = new Stack<String>();  //stack using generic type
        Stream<String> stream = fruits.stream();
        fruits.push("Apple");
        fruits.push("Banana");
        fruits.push("Mango");
        fruits.push("Orange"); 
        fruits.add("Watermelon");        
        fruits.pop();   //pop top element in stack
        int index = fruits.search("Mango");  //search
        int size = fruits.size();        //size
        System.out.println(size);       //printing the stack size 
        System.out.println(index);      //printing the index of the element
        stream.forEach((element) -> {
           System.out.println(element);  // print element
        });    //Process Stack Using Stream
        
    }
}
/*
Output:
java StackImplement
Process started (PID=6836) >>>
4
2
Apple
Banana
Mango
Orange
<<< Process finished (PID=6836). (Exit code 0)
================ READY ================
*/