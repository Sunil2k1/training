/* Create a queue using generic type and in both implementation Priority Queue,Linked list and complete following  
  -> add atleast 5 elements
  -> remove the front element
  -> search a element in stack using contains key word and print boolean value value
  -> print the size of queue
  -> print the elements using Stream 
Requirements :
  Five elements to insert in the Queue
Entities:
  QueueLinkedList
Function Declaration:
 -none-
Job to be done :  
 * Create a class name QueueLinkedList
 * Create a Queue name car in String by invoking LinkedList() method as String.
 * Create a Stream name stream in String.
 * add  5 elements to car.
 * remove the peek element from car.
 * search a element in car and print its result in boolean value
 * print the size of car
 * print car using Stream
 
Pseudocode:
public class QueueLinkedList{
   public static void main(String [] args) {
        Queue<String> car = new LinkedList<String>(); //creating a queue
        Stream<String> stream = car.stream();  //creating  stream
        ADD 5 ELEMENTS TO car;
        REMOVE PEEK ELEMENT;
        boolean containsMazda = car.contains("Mazda");   
        PRINT containsMazda;
        PRINT SIZE OF car;
        stream.forEach((element) -> {
             PRINT element;
        });           
   } 
} 
 
*/
import java.util.LinkedList;
import java.util.Queue;
import java.util.stream.*;
public class QueueLinkedList{
    public static void main(String [] args) {
        Queue<String> car = new LinkedList<String>();
        Stream<String> stream = car.stream();
        car.add("BMW");        
        car.add("Mazda");
        car.add("RR");
        car.add("Lexus");
        car.add("Meclaren");
        car.remove();
        boolean containsMazda = car.contains("Mazda");   //Check if Queue Contains Element
        System.out.println(containsMazda);
        int size = car.size();
        System.out.println(size);        //Size
        stream.forEach((element) -> {
           System.out.println(element);  // print element
        });    //Process Stack Using Stream
    }
}
