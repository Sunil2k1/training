/* Reverse List Using Stack with minimum 7 elements in list.

Requirements :
  Seven elements to insert in the List
Entities:
  ReverseList 
Function Declaration:
 -none-
Job to be done :  
 * Create a class name ReverseList
 * Create a List name fruits invoking ArrayList() method as String.
 * add 7 elements in fruits
 * print fruits
 * Create a Stack name stack in String
 * Check while
     => size of fruits > 0
     => remove the element from list and push to stack 
 * Check while
     => size of stack > 0
     => pop the element from stack and add to fruits
 * print fruits
 
Psudocode:
public class ReverseList{
    public static void main(String [] args) {
       List<String> fruits = new ArrayList<String>(); //creating a list
       ADD 7 ELEMNETS TO fruits;
       PRINT fruits;
       Stack<String> stack = new Stack<String>(); //creating a stack 
       While (FRUITS SIZE > 0) {
             REMOVE THE ELEMENT FROM fruits AND PUSH TO STACK;
       While (STACK SIZE > 0) {
             POP THE ELEMENT FROM STACK AND ADD TO fruits;
       PRINT fruits;                   
       }
    }
}    
 Code:
*/
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
public class ReverseList{
    public static void main(String [] args) {
          List<String> fruits = new ArrayList<String>();
          fruits.add("Apple");
          fruits.add("Banana");
          fruits.add("Mango");
          fruits.add("Orange");
          fruits.add("Watermelon");
          fruits.add("Pineapple");
          fruits.add("Jackfruit");
          System.out.println(fruits);

         Stack<String> stack = new Stack<String>();
         while(fruits.size() > 0) {
                   stack.push(fruits.remove(0)); //element is removed from list and push to stack
         }

         while(stack.size() > 0){
                 fruits.add(stack.pop()); //element is pop from stack and add to list
         } 

         System.out.println(fruits);
    }
}
/*
Output:
java ReverseList
Process started (PID=14476) >>>
[Apple, Banana, Mango, Orange, Watermelon, Pineapple, Jackfruit]
[Jackfruit, Pineapple, Watermelon, Orange, Mango, Banana, Apple]
<<< Process finished (PID=14476). (Exit code 0)
================ READY ================
*/