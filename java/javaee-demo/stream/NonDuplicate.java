/* Consider a following code snippet:
     List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
   - Get the non-duplicate values from the above list using java.util.Stream API


Requirement:
    Consider a following code snippet:
      - List<Integer> randomNumbers = Array.asList({1, 6, 10, 1, 25, 78, 10, 25})
      - Get the non-duplicate values from the above list using java.util.Stream API

Entity:
    - NonDuplicate

Function declaration
    - public static void main(String[] args)

Jobs to be done:
     * Create a Class name NonDuplicate and Declare main method.
     * Convert Array to list using asList method and store it in randomNumbers list.
     * Filters randomNumbers list using stream method with filter and frequency method.
           => Check randomNumbers list with number equals to one.
           => Collections collect as list using asList method.
           => Print using for each.      

Psudeocode:
public class NonDuplicate {
    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        randomNumbers.stream()
        .filter(number -> Collections.frequency(randomNumbers, number) == 1)
        .collect(Collectors.toList())
        .forEach(System.out::println); 
    }
}

Code:
*/


import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class NonDuplicate {

    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 1, 25, 78, 10, 25);
        randomNumbers.stream()
        .filter(number -> Collections.frequency(randomNumbers, number) == 1)
        .collect(Collectors.toList())
        .forEach(System.out::println); 
    }

}
