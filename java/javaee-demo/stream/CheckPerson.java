/* Consider the following Person:
            new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
       - Check if the above person is in the roster list obtained 
    from Person class.

Requirement:
    * Check if the above person is in the roster list obtained from Person class.

Entity:
    * CheckPersonInList

Function declaration
	* public static boolean contains(List<Person> list, Person o)


Jobs to be done:
    * Create a Class name CheckPerson
    * In contains(List<Person> list, Person o) Method 
          => For each person
              -> check if each person is equal to name ,gender,emailAddress and birthday in the person is equal values to be searched.
                  -> Return True
          => return false
    * Declare main method
    * Invoke Person class createRoster method and store it in roster List.
    * Create new Person and give values like name,date,gender,birthday inside it.
    * Call the method contains() and pass the parameter in it and print the boolean value which is return from this method. 

Psudeocode:

public class CheckPerson {
    public static boolean contains(List<Person> list, Person o) {
        FOR Person person : list {
            IF person.name.equals(o.name) AND
                    person.gender.equals(o.gender) AND 
                    person.emailAddress.equals(o.emailAddress) AND
                    person.birthday.equals(o.birthday)) {
                return true;
            }
        }
        RETURN false;
    }
    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Person aPerson = new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");

        PRINT contains(roster, aPerson);
    }
}

Code:
*/


import java.time.chrono.IsoChronology;
import java.util.List;

public class CheckPerson {

    public static boolean contains(List<Person> list, Person o) {
        for (Person person : list) {
            if (person.name.equals(o.name) &&
                    person.gender.equals(o.gender) &&
                    person.emailAddress.equals(o.emailAddress) &&
                    person.birthday.equals(o.birthday)) {
                return true;
            }
        }
        return false;
    }

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        Person aPerson = new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com");

        System.out.println("Check person is in the roster: " +
                contains(roster, aPerson));
    }
}
