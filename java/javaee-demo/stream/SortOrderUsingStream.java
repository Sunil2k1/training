/* Sort the roster list based on the person's age in descending order using
java.util.Stream

Requirement:
    * Sort the roster list based on the person's age in descending order using java.util.Stream
Entity:
	* SortOrderUsingStream
Function declaration
    -none-
Jobs to be done:
    * Create a Class SortOrderUsingStream and Declare main method.
    * Invoke Person class createRoster method and store it in personList List.
    * Sort person's age using stream sorted method by compare using Person class compareByAge method.
    * For each personList print person name and age using Person class printPerson method.
    
Psudeocode:
public class SortOrderUsingStream {
    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.stream().sorted(Person::compareByAge)
            .forEach(Person::printPerson);
    }
}

Code:
*/
import java.util.List;

public class SortOrderUsingStream {

    public static void main(String[] args) {
    	// stream() method can pipeline many different methods to get desired
        List<Person> personList = Person.createRoster();
        personList.stream().sorted(Person::compareByAge).forEach(Person::printPerson);

    }
}
