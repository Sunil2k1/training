/*Consider a following code snippet:
    - List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
    - Find the sum of all the numbers in the list using java.util.Stream API
    - Find the maximum of all the numbers in the list using java.util.Stream API
    - Find the minimum of all the numbers in the list using java.util.Stream API

-----------------------------------------------WBS---------------------------------------------

Requirement:
   - Consider a following code snippet:
    - List<Integer> randomNumbers = Array.asList({1, 6, 10, 25, 78})
    - Find the sum of all the numbers in the list using java.util.Stream API
    - Find the maximum of all the numbers in the list using java.util.Stream API
    - Find the minimum of all the numbers in the list using java.util.Stream API
Entity:
	* MaxMinSum
Function declaration
	-none-
Jobs to be done:
     * Create a Class name MaxMinSum and Declare main method.
     * Create a randomNumbers list and store it in Array elements as list using asList method.
     * Find integer values using stream class mapToInt method ,intValue method and sum all 
integer using sum method and store it in sum integer.
     * Find integer values using stream class mapToInt method ,getAsInt method and find the maximum
value in the list using max method and store it in maxList integer.
     * Find integer values using stream class mapToInt method ,getAsInt method and find the minimum
value in the list using min method and store it in minList integer.
     * Print the sum , minInList and maxInList.
     
PsudeoCode:

public class MaxMinSum {

    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 5, 10, 20, 78);
        int sum = randomNumbers.stream().mapToInt(Integer::intValue).sum();
        int maxInList = randomNumbers.stream()
                .mapToInt(Integer::intValue).max().getAsInt();
        int minInList = randomNumbers.stream()
                .mapToInt(Integer::intValue).min().getAsInt();
        PRINT sum;
        PRINTminInList;
        PRINT  maxInList;
    }
}

Code: */


import java.util.Arrays;
import java.util.List;

public class MaxMinSum {

    public static void main(String[] args) {
        List<Integer> randomNumbers = Arrays.asList(1, 6, 10, 25, 78);  //creating randomNumbers and inserting the values
        int sum = randomNumbers.stream().mapToInt(Integer::intValue).sum();
        int maxInList = randomNumbers.stream().mapToInt(Integer::intValue).max().getAsInt();
        int minInList = randomNumbers.stream().mapToInt(Integer::intValue).min().getAsInt();
        System.out.println("Sum: " + sum);
        System.out.println("Minimum in list: " + minInList);
        System.out.println("Maximum in list: " + maxInList);
    }

}
