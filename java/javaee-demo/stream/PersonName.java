/* Write a program to print minimal person with name and email address from the
Person class using java.util.Stream<T>#map API

Requirement:
  * Write a program to print minimal person with name and email address from the Person class using java.util.Stream<T>#map API

Entity:
  * PersonNameAndMail

Function declaration
  -none-

Jobs to be done:
   * Create a Class PersonName  and Declare main method.
   * Invoke Person class createRoster method and store it in rosterList List.
   * For each rosterList 
         => Add each person using put method.
   * Collect person name to map using stream , collect, toMap method
         => Get person name and email using Person class getEmailAddress and getName method.
   * For each print using entrySet method.
   
Psudeocode:

public class PersonName {
    public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();
        Map<String, String> mapDetails = new HashMap<>();
        FOR Person roster : rosterList {
            mapDetails.put(roster.emailAddress, roster.name);
        }
        Map<String, String> rosterMap = rosterList.stream()
                .collect(Collectors.toMap(Person::getEmailAddress, Person::getName));
        rosterMap.entrySet().forEach(System.out::println);
    }
}

Code:

*/

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class PersonName {
    public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();
        Map<String, String> mapDetails = new HashMap<>();
        for (Person roster : rosterList) {
            mapDetails.put(roster.emailAddress, roster.name);
        }
        Map<String, String> rosterMap = rosterList.stream().collect(Collectors.toMap(Person::getEmailAddress, Person::getName));       
        rosterMap.entrySet().forEach(System.out::println);
    }

}
