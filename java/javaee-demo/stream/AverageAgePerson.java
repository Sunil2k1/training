/* Write a program to find the average age of all the Person in the person List


Requirement:
   * Write a program to find the average age of all the Person in the person List

Entity:
   * AverageAgePerson

Function declaration
   -none-

Jobs to be done:
   * Create a Class name AverageAgePerson and Declare main method.
   * Invoke Person class createRoster method and store it in persons List.
   * For each age
         => Get all person's Age using Person class getAge method.
         => Average all person's Age using average method.
         => Get all person's Age in double using getAsDouble method.
   * print age.      
   
Psudeocode:

public class AverageAgePerson {
    public static void main(String[] args) {
        INVOKE PERSON CLASS createRoster METHOD AND STORE IT IN persons LIST.
        double age = persons.stream()
        		               .mapToDouble(Person::getAge)
        		               .average() 
        		               .getAsDouble();
        PRINT age;
    }
}
*/

import java.util.List;

public class AverageAgePerson {
    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();  //invoke person class createRoster Method and store it in persons list.
        double age = persons.stream()
        		               .mapToDouble(Person::getAge)
        		               .average() //calculates the average for list of elements
        		               .getAsDouble(); //converts and return Optional double to double       
        System.out.println("Average age of all persons: " + age);
    }

}
