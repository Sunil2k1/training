import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class RemoveRoster {
	public static void main(String[] args) {
	        List<Person> roster = Person.createRoster();
	        List<Person> newRoster = new ArrayList<>();
	        newRoster.add(
	            new Person(
	            "John",
	            IsoChronology.INSTANCE.date(1980, 6, 20),
	            Person.Sex.MALE,
	            "john@example.com"));
	        newRoster.add(
	            new Person(
	            "Jade",
	            IsoChronology.INSTANCE.date(1990, 7, 15),
	            Person.Sex.FEMALE, "jade@example.com"));
	        newRoster.add(
	            new Person(
	            "Donald",
	            IsoChronology.INSTANCE.date(1991, 8, 13),
	            Person.Sex.MALE, "donald@example.com"));
	        newRoster.add(
	            new Person(
	            "Bob",
	            IsoChronology.INSTANCE.date(2000, 9, 12),
	            Person.Sex.MALE, "bob@example.com"));

	        newRoster.forEach(roster::add);        
	        for (int i = 0; i< roster.size(); i++ ) {
	        	System.out.println(roster.get(i).getName() );
	        	
	        }
 	        Iterator<Person> iterator = roster.iterator();
	        while (iterator.hasNext()) {
	            iterator.next().printPerson();
	        }
	}
}
