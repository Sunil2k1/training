/* Sort the roster list based on the person's age in descending order
using comparator


Requirement:
    * Sort the roster list based on the person's age in descending order using comparator

Entity:
    *  SortOrderUsingComparator

Function declaration
    -none-

Jobs to be done:
    * Create a SortOrderUsingComparator and Declare main method.
    * Invoke Person class createRoster method and store it in personList List.
    * Sort with person age get using Person class getAge method and reverse using comparator and reversed method.
    * For each personList print using Person class printPerson method.

Psudeocode:
public class SortOrderUsingComparator {
    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.sort(Comparator.comparing(Person::getAge).reversed());
        FOR Person person : personList {
            person.printPerson();
        }
    }
}

Code:
*/


import java.util.Comparator;
import java.util.List;

public class SortOrderUsingComparator {

    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.sort(Comparator.comparing(Person::getAge).reversed()); // sorting the list using Comparator methods
        for (Person person : personList) {
            person.printPerson();
        }
    }

}
