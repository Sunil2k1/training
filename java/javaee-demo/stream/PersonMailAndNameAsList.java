/* Write a program to collect the minimal person with name and email address
from the Person class using java.util.Stream<T> API as List


Requirement:
    * Write a program to collect the minimal person with name and email address
from the Person class using java.util.Stream<T> API as List

Entity:
    * PersonMailAndNameAsList

Function Declaration
   -none-

Jobs to be done:
     * Create a Class PersonMailAndNameAsList  and Declare main method.
     * Invoke the createRoster method using the Person class and store it in roster list.
     * Get the person's email using Person class getEmailAddress and collect as list using asList method store it in emailRoster list.
     * For each email adderss present in roster
         => For each email id present given email address
             ->Check if given email address is equal to email in roster
                  -> print name and email adddress.
           
     
Pseudo Code:

public class PersonMailAndNameAsList {
    public static void main(String[] args) {
        INVOKE Person CLASS createRoster METHOD AND STORE IT IN roster LIST.
        List<String> emailRoster = roster.stream()
            .map(Person::getEmailAddress)
            .collect(Collectors.toList());
        FOR (int i = 0; i < emailRoster.size(); i++) {
            FOR (int j = 0; j < roster.size(); j++) {
                IF roster.get(j).emailAddress == emailRoster.get(i) {
                    PRINT NAME,MAIL ID
                    );
                }
            }
        }
        }
    }
}
*/

import java.util.List;
import java.util.stream.Collectors;

public class PersonMailAndNameAsList {
    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        // creating a list and storing the returned list
        List<String> emailRoster = roster.stream()
            .map(Person::getEmailAddress)
            .collect(Collectors.toList());
        // created a list of emails from objects of person class using pipelined
        for (int i = 0; i < emailRoster.size(); i++) {
            for (int j = 0; j < roster.size(); j++) {
                if (roster.get(j).emailAddress == emailRoster.get(i)) {
                    System.out.println(
                        "Person name: " +
                        roster.get(j).name + ", mail Id: " +
                        emailRoster.get(i)
                    );
                }
            }
        }
    }
}
