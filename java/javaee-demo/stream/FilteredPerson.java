/*  Write a program to filter the Person, who are male and age greater than 21

Requirement:
   * Write a program to filter the Person, who are male and age greater than 21
Entity:
   * FilterByGenderAndAge
Function declaration
   -none-
Jobs to be done:
   * Create a Class name AverageAgePerson and Declare main method.
   * Invoke Person class createRoster method and store it in persons List.
   * For each age
         => Check if Person's gender is MALE and age is greater than 21.
         => Convert it as list using asList method.
   * print filteredPersons.         
        
Psudocode:

public class FilteredPerson {
    public static void main(String[] args) {
    	 INVOKE Person CLASS createRoster METHOD AND STORE IT IN persons LIST.
	     List<Person> filteredPersons = persons.stream()
	        		                              .filter(person -> person.gender == Person.Sex.MALE )
	        		                              .filter(person -> person.getAge() > 21)
	        		                              .collect(Collectors.toList());
	    PRINT filteredPersons.toString();
    }
}

*/

import java.util.List;
import java.util.stream.Collectors;

public class FilteredPerson {
    public static void main(String[] args) {
    	 List<Person> persons = Person.createRoster();
	        List<Person> filteredPersons = persons.stream()
	        		                              .filter(person -> person.gender == Person.Sex.MALE )
	        		                              .filter(person -> person.getAge() > 21)
	        		                              .collect(Collectors.toList());
			System.out.println(filteredPersons.toString());
    }
}
