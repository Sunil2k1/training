/* Print all the persons in the roster using java.util.Stream<T>#forEach 


Requirement:
	- Print all the persons in the roster using java.util.Stream<T>#forEach

Entity:
    * PrintPersonForEach

Function declaration
	-none-

Jobs to be done:
    * Create a Class PrintPerson and Declare main method.
    * Invoke Person class createRoster method and store it in personList List.
    * For each personList print person name and age using Person class printPerson method

Psudeocode:

public class PrintPerson {
    public static void main(String[] args) { 
        List<Person> personList = Person.createRoster();
        personList.forEach(Person::printPerson);
    }
}

Code:
*/

import java.util.List;

public class PrintPerson {

    public static void main(String[] args) {
        List<Person> personList = Person.createRoster();
        personList.forEach(Person::printPerson);
    }
}
