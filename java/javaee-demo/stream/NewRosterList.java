/* Consider the following code snippet:
            List<Person> newRoster = new ArrayList<>();
            newRoster.add(
                new Person(
                "John",
                IsoChronology.INSTANCE.date(1980, 6, 20),
                Person.Sex.MALE,
                "john@example.com"));
            newRoster.add(
                new Person(
                "Jade",
                IsoChronology.INSTANCE.date(1990, 7, 15),
                Person.Sex.FEMALE, "jade@example.com"));
            newRoster.add(
                new Person(
                "Donald",
                IsoChronology.INSTANCE.date(1991, 8, 13),
                Person.Sex.MALE, "donald@example.com"));
            newRoster.add(
                new Person(
                "Bob",
                IsoChronology.INSTANCE.date(2000, 9, 12),
                Person.Sex.MALE, "bob@example.com"));
    - Create the roster from the Person class and add each person in the
        newRoster to the existing list and print the new roster List.
    - Print the number of persons in roster List after the above addition.
    - Remove the all the person in the roster list


Requirement:
	- Create the roster from the Person class and add each person in the newRoster to the existing list and print the new roster List.
	- Print the number of persons in roster List after the above addition.
	- Remove the all the person in the roster list

Entity:
	* NewRosterList

Function declaration
	-none-

Jobs to be done:
     * Create a Class name NewRosterList and Declare main method.  
     * Invoke Person class createRoster method and store it in persons List.
     * For each newRoster 
          => Invoke add method and add new rosters.
          => Print all person name and age using person class printPerson method.
     * Print the size of roster using size method.
     * Remove all roasters using clear method.     
     * Print the size of roster using size method.
              
Psudeo Code:

public class NewRosterList {
    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
        newRoster.forEach(roster::add);
        newRoster.forEach(Person::printPerson);
        PRINT  roster.size();
        CLEAR roster
        PRINT THE SIZE OF roster;
    }
}


Code:
*/

import java.time.chrono.IsoChronology;
import java.util.ArrayList;
import java.util.List;

public class NewRosterList {

    public static void main(String[] args) {
        List<Person> roster = Person.createRoster();
        List<Person> newRoster = new ArrayList<>();
        newRoster.add(
            new Person(
            "John",
            IsoChronology.INSTANCE.date(1980, 6, 20),
            Person.Sex.MALE,
            "john@example.com"));
        newRoster.add(
            new Person(
            "Jade",
            IsoChronology.INSTANCE.date(1990, 7, 15),
            Person.Sex.FEMALE, "jade@example.com"));
        newRoster.add(
            new Person(
            "Donald",
            IsoChronology.INSTANCE.date(1991, 8, 13),
            Person.Sex.MALE, "donald@example.com"));
        newRoster.add(
            new Person(
            "Bob",
            IsoChronology.INSTANCE.date(2000, 9, 12),
            Person.Sex.MALE, "bob@example.com"));
        newRoster.forEach(roster::add);
        newRoster.forEach(Person::printPerson);
        System.out.println("Number of persons in roster list: " + roster.size());
        roster.clear();        // removing all the elements of roster list
        System.out.println("Number of persons in roster list: " + roster.size());
    }

}
