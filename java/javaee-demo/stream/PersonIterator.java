/* Iterate the roster list in Persons class and and print the person
without using forLoop/Stream 

Requirement:
  * Iterate the roster list in Persons class and and print the person without using forLoop/Stream 
Entity:
  * PrintPersonIterator
Function declaration
  -none-

Jobs to be done:

   * Invoke Person class createRoster method and store it in persons List.
   * Print persons list using iterator method
        => Check list contain next person using iterator method with hasNext method.
        => Print Person class printPerson method using next method.

Psudeocode:
public class PersonIterator {
    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        Iterator<Person> iterator = persons.iterator();
        WHILE iterator.hasNext() {
            iterator.next().printPerson();
        }
    }
}

Code:
*/


import java.util.Iterator;
import java.util.List;

public class PersonIterator {
    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();
        Iterator<Person> iterator = persons.iterator();   // creating a object of iterator
        while (iterator.hasNext()) {
            iterator.next().printPerson();
        }
    }
}
