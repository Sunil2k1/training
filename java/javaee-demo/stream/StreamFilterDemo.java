/*   Write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons


1.Requirement:

   - Write a program to filter the Person, who are male and
        - find the first person from the filtered persons
        - find the last person from the filtered persons
        - find random person from the filtered persons

Entity:
   * StreamFilterDemo

Function declaration
   -none-

Jobs to be done:
    * Create a Class StreamFilterDemo and Declare main method.
    * Invoke Person class createRoster method and store it in rosterList List.
    * Filter person's gender is Male and collect as list using asList method.
    * Find the first person using get method.
    * Find the last person subracting size of list by one using Person class printPerson method.
    * Find random person get method with random nextInt using Person class printPerson method.
    
Psudeocode:
public class StreamFilterDemo {

    public static void main(String[] args) {
        List<Person> rosterList = Person.createRoster();
        List<Person> maleRosterList = rosterList.stream()
                                                .filter(aPerson -> aPerson.gender == Person.Sex.MALE)
                                                .collect(Collectors.toList());
        maleRosterList.get(0).printPerson();
        maleRosterList.get(maleRosterList.size() - 1).printPerson();
        Random random = new Random();
        maleRosterList.get(random.nextInt(maleRosterList.size())).printPerson();
    }
}

Code:

*/



import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class StreamFilterDemo {

    public static void main(String[] args) {
        List<Person> persons = Person.createRoster();       //creating a roster  of persons
        List<Person> maleList = persons.stream()
                                                .filter(aPerson -> aPerson.gender == Person.Sex.MALE)
                                                .collect(Collectors.toList());
        maleList.get(0).printPerson();
        maleList.get(maleList.size() - 1).printPerson();
        Random random = new Random();
        maleList.get(random.nextInt(maleList.size())).printPerson();
    }

}
