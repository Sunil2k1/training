/* TO complete the program using override annotation and show the output.


Requirement:
  * TO complete the program using override annotation and show the output.
Entity:
  * Base
  * Derived
Function Declaration:
  * public void display() 
  * public void display(int x) 
Jobs to be done:
  *  create a parent class Base.
  *  Create a Derived class extends Base.
  *  Using override annotation override,override the Derived class Display method
  *  Hence the method overriding fails and throws an error message "method does not override or implement a method from a supertype"
	
Pseudo Code:
class Base{
    public void display()
	{
    }
}
class Derived Extends Base{
    @Override
    public void display(int x)
	{
	public static void main(String args[]) 
    { 
        Derived obj = new Derived(); 
        obj.display(); 
    } 

}

*/
//import java.lang.Annotation;
class Base 
{ 
     public void display() 
     { 
         
     }
     	 
} 
class Derived extends Base 
{ 
     //@Override
     public void display(int x) 
     { 
         
     } 
  
     public static void main(String args[]) 
     { 
         Derived obj = new Derived(); 
         obj.display(); 
     } 
}
