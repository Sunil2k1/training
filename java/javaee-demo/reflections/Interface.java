/*  To create a Program to demonstrate Target annotation (@Target) using interface annotation (@interface)
Requirement:
  * To create a Program to demonstrate Target annotation (@Target) using interface annotation (@interface)

Entity:
  * Interface

Function Declaration:
  -none-
Jobs to be done:
  * Create class name Interface. 
  * Define the target type as METHOD.
  * MyCustomAnnotation is created by using @interface.
  * Apply MyCustomAnnotation to class MyClass.
  * Thus Annotation are applied to methods using target annotation.

Psuedo Code:
@Target(ElementType.METHOD)
@interface MyCustomAnnotation{
  int studentAge() default 18;
  String studentName();
  String stuAddress();
  String stuStream() default "ECE";
}

public class Interface{
  
  @MyCustomAnnotation(
  studentName="Chaithra",
  stuAddress="Agra"

)
  public static void main(String[] args){
  PRINT "This function is annotated";
  }
}
*/


import java.lang.annotation.ElementType;
import java.lang.annotation.Target;
@Target(ElementType.METHOD)
@interface MyCustomAnnotation{
  int studentAge() default 18;
  String studentName();
  String stuAddress();
  String stuStream() default "ECE";
}

public class Interface{
  
  @MyCustomAnnotation(
  studentName="Chaithra",
  stuAddress="Agra"

)
  public static void main(String[] args){
  System.out.println("This function is annotated");
  }
}


//Output:
//This function is annotated
