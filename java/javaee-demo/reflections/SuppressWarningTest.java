/*	 To Deprecate the display method and to suppress the deprecate message. 
Requirement:
  * To Deprecate the display method and to suppress the deprecate message. 

Entity:
  * DeprecatedTest
  * Test

Function Declaration:
  * public void display() 

Jobs to be done:
  * create a class DeprecatedTest with method Display().
  * Declare the main class as public
  * using Deprecated annotation make the display method as deprecated method
  * hence the method fails to execute and throws an error message as " SuppressWarningTest.java uses or overrides a deprecated API."
  * To suppress the deprecated error, use SuppressWarning annotation to suppress "checked" and "deprecation" error in main class.
  * Thus the deprecation is suppressed and output is displayed.

Pseudo code:
class DeprecatedTest 
{   
    @Deprecated
    public void Display() 
    { 
        PRINT "Deprecatedtest display()"; 
    } 
} 
  
public class SuppressWarningTest
{ 
    
    @SuppressWarnings({"checked", "deprecation"}) 
    public static void main(String args[]) 
    { 
        DeprecatedTest d1 = new DeprecatedTest(); 
        d1.Display(); 
    } 
} 
*/



import java.lang.annotation.Annotation;

class DeprecatedTest { 
    @Deprecated
    public void Display() 
    { 
        System.out.println("Deprecatedtest display()"); 
    } 
} 
  
public class SuppressWarningTest 
{ 
    
    @SuppressWarnings({"checked", "deprecation"}) 
    public static void main(String args[]) 
    { 
        DeprecatedTest d1 = new DeprecatedTest(); 
        d1.Display(); 
    } 
} 
