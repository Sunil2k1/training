/*
To write a code to retrieve the added elements using the appropriate method.

Requirment:
    - Program to retrieve the added elements using the appropriate method.

Entity:
     - ArrayRetrieveElement

Method Decalaration
    - public static void main(String[] args) 

Jobs to be done:
   1.Create an array using newIntance method and store its size.
   2.Add elements to array using setInt method
   3.Get elements of array using getInt method.
   
Pseudo Code:
public class ArrayRetrieveElement { 
	public static void main(String[] args) 
	{ 
		int sizeOfArray = 3; 
		int[] intArray = (int[])Array.newInstance(int.class, sizeOfArray); 
		System.out.println(Arrays.toString(intArray)); 
		System.out.println("Element at index 2: "+ Array.getInt(intArray, 2))
     }
}

*/

import java.lang.reflect.Array;
import java.util.Arrays;

public class ArrayRetrieveElement {
	public static void main(String[] args) {

		int sizeOfArray = 4;

		// Create an integer array using reflect.Array class and this is done using the
		// newInstance() method
		int[] intArray = (int[]) Array.newInstance(int.class, sizeOfArray);

		// Add elements from the array and this is done using the getInt() method
		Array.setInt(intArray, 1, 10);
		Array.setInt(intArray, 2, 20);
		Array.setInt(intArray, 3, 30);

		System.out.println(Arrays.toString(intArray));

		// Retrieve elements from the array and this is done using the getInt() method
		System.out.println("Element at index 0: " + Array.getInt(intArray, 1));
		System.out.println("Element at index 1: " + Array.getInt(intArray, 2));
		System.out.println("Element at index 2: " + Array.getInt(intArray, 3));
	}
}
