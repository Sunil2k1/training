/*write a program to display the following using escape sequence in java
A.	 My favorite book is "Twilight" by Stephanie Meyer
B.	She walks in beauty, like the night, 
Of cloudless climes and starry skies 
And all that's best of dark and bright 
Meet in her aspect and her eyes�
C.	"Escaping characters", � 2019 Java

Requirement:
   A.	 My favorite book is "Twilight" by Stephanie Meyer
   B.	She walks in beauty, like the night, 
        Of cloudless climes and starry skies 
        And all that's best of dark and bright 
        Meet in her aspect and her eyes�
   C.	"Escaping characters", � 2019 Java

Entity:
   * EscapeSequenceDemo

Function declaration
  -none-

Jobs to be done:
  * print "My favorite book is \"Twilight\" by Stephanie Meyer"
  * print "She walks in beauty, like the night, \r\n" + 
				"Of cloudless climes and starry skies \r\n" + 
				"And all that's best of dark and bright \r\n" + 
				"Meet in her aspect and her eyes�"
  * print "\"Escaping characters\", � 2019 Java" 				

Psudocode:

public class EscapeSequenceDemo {
	public static void main(String [] args) {
		PRINT "My favorite book is \"Twilight\" by Stephanie Meyer");
		PRINT "She walks in beauty, like the night, \r\n" + 
				"Of cloudless climes and starry skies \r\n" + 
				"And all that's best of dark and bright \r\n" + 
				"Meet in her aspect and her eyes�");
		PRINT "\"Escaping characters\", � 2019 Java";
	}
}
  */
public class EscapeSequenceDemo {
	public static void main(String [] args) {
		System.out.println("My favorite book is \"Twilight\" by Stephanie Meyer");
		System.out.println("She walks in beauty, like the night, \r\n" + 
				"Of cloudless climes and starry skies \r\n" + 
				"And all that's best of dark and bright \r\n" + 
				"Meet in her aspect and her eyes�");
		System.out.println("\"Escaping characters\", � 2019 Java");
	}
}
