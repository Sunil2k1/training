/*Write a Java program to calculate the revenue from a sale based on the unit price and quantity of a product input by the user.
The discount rate is 10% for the quantity purchased between 100 and 120 units, and 15% for the quantity purchased greater than 120 units. If the quantity purchased is less than 100 units, the discount rate is 0%. See the example output as shown below:
Enter unit price: 25
Enter quantity: 110
The revenue from sale: 2475.0$
After discount: 275.0$(10.0%)

Requirement:
   * unit price and Quantity

Entity:
   * CalculateRevenue

Function declaration
   -none-

Jobs to be done:
 * Create a Class name EmailValidation and declare main method.
 * Create a object for Scanner name sc.  
 * Get unit price from user and stored in unitprice type Integer.
 * Get quantity from user and stored in quantity type Integer.
 * revenue is equal to the product of   unitprice and quantity
 * Check if quantity is greater than 100 and lesser than 120.
      => dis type float is equal to unitprice by 10.
      => print dis.
 * Check if quantity is greater than 120.
      => dis type float is equal to unitprice by 15 .
      => print dis.
 * Else
      => Print revenue
Psudocode:

public class CalculateRevenue {
	public static void main(String [] args) {
		Scanner sc = new Scanner (System.in);
		PRINT "Enter unit price: ";
		int unitprice = sc.nextInt();
		PRINT "Enter quantity: ";
		int quantity = sc.nextInt();
		int revenue = unitprice * quantity;
		PRINT "The revenue from sale: "+revenue;
		IF quantity > 100 && quantity < 120 {
			float dis = revenue / 10 ;
			PRINT "After discount: "+dis+ "$(10.0%)";
		}
		ELSE IF quantity > 120  {
			float dis = revenue / 15 ;
			PRINT "After discount: "+dis+ "$(15.0%)";
		}
		ELSE {
			PRINT "After discount: $(0.0%)";
		}
	}
}
                     
 */
import java.util.Scanner;

public class CalculateRevenue {
	public static void main(String [] args) {
		Scanner sc = new Scanner (System.in);
		System.out.println("Enter unit price: ");
		int unitprice = sc.nextInt();
		System.out.println("Enter quantity: ");
		int quantity = sc.nextInt();
		int revenue = unitprice * quantity;
		System.out.println("The revenue from sale: "+revenue);
		if(quantity > 100 && quantity < 120) {
			float dis = revenue / 10 ;
			System.out.println("After discount: "+dis+ "$(10.0%)");
		}
		else if(quantity > 120 ) {
			float dis = revenue / 15 ;
			System.out.println("After discount: "+dis+ "$(15.0%)");
		}
		else {
			System.out.println("After discount: $(0.0%)");
		}
	}
}
