/*Addition,Substraction,Multiplication and Division concepts are 
achieved using Lambda expression and functional interface.
Requirements:
   * 2 Integers
Entities
   * LambdaExpDemo
   * Arthmetic - Interface 
Function Declaration
   * int operation(int a, int b)
Jobs to be done
    * In Interface Arithmetic
         =>operation(int a, inta )
    * Create a class name LambdaExpDemo and declaring the main.
    * Using Lambda expression calculate addition operation 
    * print addition operation
    * Using Lambda expression calculate subtraction operation  
    * print subtraction operation 
    * Using Lambda expression calculate multiplication operation 
    * print multiplication operation
    * Using Lambda expression calculate division operation 
    * print division operation

Psudocode:

interface Arithmetic {
	int operation(int a, int b);
}
public class LambdaExpDemo {
	public static void main(String[] args) {

		Arithmetic addition = (int a, int b) -> (a + b);
		PRINT addition.operation(5, 6);
		Arithmetic subtraction = (int a, int b) -> (a - b);
		PRINT subtraction.operation(5, 3);
		Arithmetic multiplication = (int a, int b) -> (a * b);
		PRINT multiplication.operation(4, 6);
		Arithmetic division = (int a, int b) -> (a / b);
		PRINT division.operation(12, 6);
	}
}
Code:
  */

interface Arithmetic {
	int operation(int a, int b);
}

public class LambdaExpDemo {
	public static void main(String[] args) {

		Arithmetic addition = (int a, int b) -> (a + b);
		System.out.println("Addition = " + addition.operation(5, 6));
		Arithmetic subtraction = (int a, int b) -> (a - b);
		System.out.println("Subtraction = " + subtraction.operation(5, 3));
		Arithmetic multiplication = (int a, int b) -> (a * b);
		System.out.println("Multiplication = " + multiplication.operation(4, 6));
		Arithmetic division = (int a, int b) -> (a / b);
		System.out.println("Division = " + division.operation(12, 6));
	}
}
