/* create an array list with 7 elements, and create an empty linked list add all elements of 
 the array list to linked list ,traverse the elements and display the result
 

Requirements:
   * array list with values
Entities
   * ArrayToLinkedList
Function Declaration
   -none-
Jobs to be done
    * Create a class name ArrayToLinkedList and declaring the main.
    * Create a List name car implementing ArrayList in String instance
    * add 7 vales in car
    * Create a List name cars implementing ArrayList in String instance
    * Check while
         => car size is greater than zero
         => Remove 0 Index value from car and add the value  to cars
    * Print cars
    * Create a Iterator name iter in String Instance
    * Check while 
         => iter.hasNext()
         => print iter.hasNext() 

Psudocode:

public class ArrayToLinkedList {
	public static void main(String [] args) {
		List<String> car = new ArrayList<String>();  //create a list
		ADD 7 VALUES IN car;
		PRINT car;
		List<String> cars = new LinkedList<String>();  /create a list
        WHILE car.size() > 0 {
            REMOVE 0 INDEX VALUE FROM car AND ADD THE VALUE TO cars;
            }
		PRINT cars;
        Iterator<String> iter = cars.listIterator(4);  // create a iterator
        WHILE iter.hasNext(){ 
           PRINT iter.next(); 
        } 
	}

}
Code:
*/

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Iterator;

public class ArrayToLinkedList {
	public static void main(String [] args) {
		List<String> car = new ArrayList<String>();
		car.add("BMW"); 	//Add 7 values in the list
		car.add("Mazda");
		car.add("Lexus");
		car.add("Audi");
		car.add("Mecleran");
		car.add("RR");
		car.add("Dodge");
		System.out.println("Elements in the array List : "+car);
		List<String> cars = new LinkedList<String>();
        while(car.size() > 0) {
            cars.add(car.remove(0));
            }
		System.out.println("Elements in the Linked List : "+cars);
        Iterator<String> iter = cars.listIterator(4); 
        System.out.println("After Traverse the elements using Iterator:"); 
        while(iter.hasNext()){ 
           System.out.println(iter.next()); 
        } 
	}

}
