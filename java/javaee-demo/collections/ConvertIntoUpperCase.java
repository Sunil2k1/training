/*8 districts are shown below
    Madurai, Coimbatore, Theni, Chennai, Karur, Salem, Erode, Trichy  
     to be converted to UPPERCASE.
Requirements:
   * List with values
Entities
   * ConvertIntoUpperCase
Function Declaration
   -none-
Jobs to be done
    * Create a class name ConvertIntoUpperCase and declaring the main.
    * Create a List name city in String instance.
    * Add 8 values in the city
    * For each city
         => converted to UPPERCASE
         => Print the element in city

Psudocode:

public class ConvertIntoUpperCase {
	public static void main(String[] args) {
	List<String> city = new ArrayList<String>();
	ADD 8 VALUES IN city;
	for(String i : city) {
	     PRINT i.toUpperCase();
	    }
    }
}
Code:      
     */
import java.util.ArrayList;
import java.util.List;

public class ConvertIntoUpperCase {
	public static void main(String[] args) {
	List<String> city = new ArrayList<String>();
	city.add("Madurai"); 	//Add 8 values in the list
	city.add("Coimbatore");
	city.add("Theni");
	city.add("Chennai");
	city.add("Karur");
	city.add("Salem");
	city.add("Erode");
	city.add("Trichy ");
	for(String i : city) {
	     System.out.println(i.toUpperCase() );
	    }
    }
}
