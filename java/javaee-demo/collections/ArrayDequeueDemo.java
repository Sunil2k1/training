/* use addFirst(),addLast(),removeFirst(),removeLast(),peekFirst(),peekLast(),pollFirst(),poolLast()
 methods to store and retrieve elements in ArrayDequeue.
Requirements:
   * Dequeue with values
Entities
   * ArrayDequeueDemo
Function Declaration
   -none-
Jobs to be done
    * Create a class name ArrayDequeueDemo and declaring the main.
    * Create a Deque implementing array list name car in String Instance.
    * add 1 element at first using addFirst() Method in car
    * add 4 elements in car
    * add 1 element at last using addLast() Method in car
    * print peek first value
    * print peek last value
    * poll first value in car
    * poll last value in car
    * remove first value in car
    * remove last value in car'
    * print car
   
Psudocode:

public class ArrayDequeueDemo {
	public static void main(String [] args) {
		Deque<String> car = new LinkedList<String>(); //Create a Deque
		ADD 1 ELEMENT AT FIRST USING addFirst() METHOD in car;
		ADD 4 ELEMENT IN car;
		ADD 1 ELEMENT AT Last USING addLast() METHOD in car;
		PRINT PEEK FIRST VALUE IN car;
		PRINT PEEK LAST VALUE IN car;
		POLL FIRST VALUE IN car;
		POLL LAST VALUE IN car;
		REMOVE FIRST VALUE IN car;
		REMOVE LAST VALUE IN car;
		PRINT car;
	}

} 
Code:
 */

import java.util.LinkedList;
import java.util.Deque;
public class ArrayDequeueDemo {
	public static void main(String [] args) {
		Deque<String> car = new LinkedList<String>();
		car.addFirst("Meclaren");  //add at first
		car.add("BMW");
		car.add("Mazda");
		car.add("Lexus");
		car.add("Audi");
		car.addLast("Bugati");   //add at last
		System.out.println("Peek First Value: "+car.peekFirst()); //peek first value
		System.out.println("Peek Last Value: "+car.peekLast()); //peek last value
		car.pollFirst();  //poll first value
		car.pollLast();   //poll last value
		car.removeFirst();  //remove first value
		car.removeLast();   //remove last value
		System.out.println(car);
	}

}
