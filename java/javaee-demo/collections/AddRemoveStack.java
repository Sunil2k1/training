/*add and remove the elements in stack
 Requirements:
   * stack with values
Entities
   * AddRemoveStack
Function Declaration
   -none-
Jobs to be done
    * Create a class name AddRemoveStack and declaring the main.
    * Create a stack name fruits in String instance.
    * Add 4 values in the list
    * pop top element
    * print the stack

Psudocode:

public class AddRemoveStack {
    public static void main(String [] args) {
        Stack<String> fruits = new Stack<String>();  //stack using generic type
        PUSH 4 VALUES IN fruits;
        POP ELEMENT FROM fruits;
        PRINT fruits;
    }
}
Code: 
 */
import java.util.Stack;
public class AddRemoveStack {
    public static void main(String [] args) {
        Stack<String> fruits = new Stack<String>();  //stack using generic type
        fruits.push("Apple"); //adding a element to stack
        fruits.push("Banana");
        fruits.push("Mango");
        fruits.push("Orange"); 
        fruits.pop();  //removing a element to stack
        System.out.println(fruits);
    }
}
