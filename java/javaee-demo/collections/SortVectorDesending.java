/* code for sorting vetor list in descending order.
Requirements:
   *vector with values
Entities
   * SortVectorDesending
Function Declaration
   -none-
Jobs to be done
    * Create a class name SortVectorDesending and declaring the main.
    * Create a vector name car 
    * Add 6 values in the vector
    * sorting car values in descending order and print

Psudocode:

public class SortVectorDesending {
	public static void main (String [] args) {
		Vector<String> car = new Vector<String>(); //create a vector
		ADD 6 VALUES IN car
		Collections.sort(car, Collections.reverseOrder());		 
		PRINT car;
	}
}
Code:
 */
import java.util.Collections;
import java.util.Vector;

public class SortVectorDesending {
	public static void main (String [] args) {
		Vector<String> car = new Vector<String>();
		car.add("Mazda");
		car.add("Lexus");
		car.add("Audi");
		car.add("Mecleran");
		car.add("RR");
		car.add("Dodge");
		System.out.println( car);
		Collections.sort(car, Collections.reverseOrder());		 
		System.out.println( car);
	}
}
