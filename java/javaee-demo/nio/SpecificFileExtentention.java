/* Get the file names of all file with specific extension in a directory

Requirements:
    * Program to get the file names of all file with specific extension in a directory.
    
Entities:
    * SpecificFileExtentention
    
Method signature:
   -none-
  
Jobs to be done:
    * Create a Class name SpecificFileExtentention and Declare main method.
    * Pass file path in File class contructor.
    * Filter file using list method and FilenameFilter class store it in string array.
    * Check the file name using toLowerCase method endsWith method return true or false.
    * Print the specific file extention in the path. 
    
Pseudo Code:
public class SpecificFileExtentention {
	public static void main(String a[]) {
		File file = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
		String[] list = file.list(new FilenameFilter() {
			public boolean accept(File dir, String name) {
				IF (name.toLowerCase().endsWith(".txt")) {
					RETURN true;
				} ELSE {
					RETURN false;
				}
			}
		});
		FOR (String f : list) {
			PRINT f;
		}
	}
}
code:
*/

import java.io.File;
import java.io.FilenameFilter;

public class SpecificFileExtentention {
	public static void main(String a[]) {
		File file = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
		String[] ext = file.list(new FilenameFilter() {
			@Override
			public boolean accept(File dir, String name) {
				if (name.toLowerCase().endsWith(".txt")) {
					return true;
				} else {
					return false;
				}
			}
		});
		for (String exts : ext) {
			System.out.println(exts);
		}
	}
}
