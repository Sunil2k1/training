/* Create two paths and test whether they represent same path

Requirements:
    * Program to create two paths and test whether they represent same path.
    
Entities:
    * CheckSamePath
    
Method signature:
   * private static void testSameFile(Path path1, Path path2) 
Jobs to be done:
    * Create a class name CheckSamePath and Declare main method.
    * Create three Path with specify the path as argument in Path class constructor.
    * Try block Invoke the testSameFile method.  
       => Check if the two file path are same using isSameFile method.
           -> Print Same Path
       => Print Not Same Path    
    * Catch Error using printStackTrace method. 
    
Pseudo Code:
''''''''''''

public class CheckSamePath {
  public static void main(String[] args) {
    Path path1 = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
    Path path2 = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
    Path path3 = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo");

    testSameFile(path1, path2);
    testSameFile(path1, path3);
  }
  public static void testSameFile(Path path1, Path path2) {
    TRY {
      if (Files.isSameFile(path1, path2)) {
        PRINT "Same Path";
      } ELSE {
        PRINT "Not the Same Path";
      }
    } CATCH IOException e {
      e.printStackTrace();
    }
  }
}

*/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class CheckSamePath {
  public static void main(String[] args) {
    Path path1 = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
    Path path2 = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
    Path path3 = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/");

    testSameFile(path1, path2);
    testSameFile(path1, path3);
  }
  private static void testSameFile(Path path1, Path path2) {
    try {
      if (Files.isSameFile(path1, path2)) {
        System.out.println("Same Path");
      } else {
        System.out.println("Not the Same Path");
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}