/* Number of files in a directory and number of directories in a directory

Requirements:
    * Program to number of files in a directory and number of directories in a directory.
    
Entities:
    * FileDirectoryCount
    
Method signature:
   -none-
  
Jobs to be done:
    * Create File class and pass the file path in File class constructor argument.
    * Invoke list method and length to find the number files in directory.
    * Print the number files in directory.
    
Pseudo Code:
public class FileDirectoryCount {
	public static void main(String args[]) {
		File directory = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
		int fileCount = directory.list().length;
		PRINT "File Count:" + fileCount;
	}
}
Code:
*/

import java.io.File;

public class FileDirectoryCount {
	public static void main(String args[]) {
		File directory = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
		int fileCount = directory.list().length;
		System.out.println("File Count:" + fileCount);
	}
}
