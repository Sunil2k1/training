/*  Delete a file using visitFile() method.

Requirements:
    * Program to delete a file using visitFile() method.
    
Entities:
    * DeleteVisitFileMethod
    
Method signature:
   -none-
  
Jobs to be done:
    * Create file SimpleFileVisitor class with generic type of Path 
    * Invoke the walkFileTree method it invoke
        => Check file get file Name using getFileName,convert to String using toString , contains method to file exists or not.
        => Invoke Files class delete method to delete file
        => Return the FileVisitResult class CONTINUE to stop process.
Pseudo Code:
public class DeleteVisitFileMethod {
	public static void main(String[] args) throws IOException {
		FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				
				if(file.getFileName().toString().contains("deletefile.txt")) {
					Files.delete(file);
				}
				return FileVisitResult.CONTINUE ;
				
			}
		};
		Files.walkFileTree(Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/"), fv);
	}
}


Code:*/



import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class DeleteVisitFileMethod {
	public static void main(String[] args) throws IOException {
		FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {				
				if(file.getFileName().toString().contains("text.txt")) {
					Files.delete(file);
				}
				return FileVisitResult.CONTINUE ;				
			}
		};
		Files.walkFileTree(Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/"), fv);
	}
}
