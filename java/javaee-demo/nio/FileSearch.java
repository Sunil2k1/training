/*Search any file using walkFileTree() method with enum instance(SKIP_SIBLINGS,SKIP_SUBTREE).

Requirements:
    * Program to  Search any file using walkFileTree() method with enum instance(SKIP_SIBLINGS,SKIP_SUBTREE).
    
Entities:
    * FileSearch
    
Method signature:
   -none-
  
Jobs to be done:
    * Create file SimpleFileVisitor class with generic type of Path 
    * Invoke the walkFileTree method it invoke
        => postVisitDirectory method to return enum SKIP_SUBTREE and SKIP_SIBLINGS.
        => visitFile method to return the directory containing all file paths
        => Invoke visitFile method to check exists file or not.
       
Pseudo Code:
public class FileSearch {
	public static void main(String[] args) throws IOException {
		FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
			@SuppressWarnings("static-access")
			public FileVisitResult postVisitDirectory(Path dir, IOException e ) throws IOException {
				return FileVisitResult.SKIP_SUBTREE.SKIP_SIBLINGS;
		
			}
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				System.out.println(file);
				return super.visitFile(file, attrs);
			}
		};
		Path path = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
		Files.walkFileTree(path, fv);
	}
}

Code:*/

import java.io.IOException;
import java.nio.file.FileVisitResult;
import java.nio.file.FileVisitor;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;

public class FileSearch {

	public static void main(String[] args) throws IOException {
		FileVisitor<Path> fv = new SimpleFileVisitor<Path>() {
			@SuppressWarnings("static-access")
			public FileVisitResult postVisitDirectory(Path dir, IOException e ) throws IOException {
				return FileVisitResult.SKIP_SUBTREE.SKIP_SIBLINGS;
		
			}
			public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) throws IOException {
				System.out.println(file);
				return super.visitFile(file, attrs);
			}
		};
		Path path = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
		Files.walkFileTree(path, fv);
	}
}
