/*Perform file operations using exists(), createDirectory(), copy(), move() and delete() methods.

Requirements:
    * Program to demonstrate toAbsolutePath(), normalize(), getName(), getFileName() and getFileCount().
Entities:
    * PathMethods
    
Method signature:
   -none-
  
Jobs to be done:
    * Create the txt file using Files class createTempFile method.
          => Using exist method print file exist or not. //true or false
    * Create the txt file using Files class createTempFile method.
          => Using copy method copy the content to another txt file.
          => Print the file copy or not.
    * Create the txt file using Files class createTempFile method.
          => Resolve method to get the file 
          => Using createDirectory method to create directory.
    * Delete the file using Files class delete method.
          => Print the file delete or not.
    * Create two file path to move one to one another file.
          => Try block using move method to move source to destination
          => StandardCopyOption with REPLACE_EXISTING print move or not 
          =>Source file deleted moved after.

Pseudo Code:
public class FileOpertaions {
	public static void main(String... args) throws IOException {
        Path path = Files.createTempFile("test-sample", ".txt");
        PRINT "File to check: " + path;
        boolean exists = Files.exists(path);
        PRINT "File to check exits: " + exists;
        Path copy = Files.createTempFile("copy-sample", ".txt");  //COPY METHOD
        PRINT "file to copy: " + path;
        Path copiedFile = Files.copy(
        		copy,
                Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/" + copy.getFileName()),
                StandardCopyOption.REPLACE_EXISTING
        );
        PRINT "file copied: " + copiedFile;        
        //createDirectory() method
        Path tempPath = Files.createTempDirectory("test");
        Path dirToCreate = tempPath.resolve("test1");
        Path directory = Files.createDirectory(dirToCreate);
        System.out.println("directory created: " + directory);
        
        //delete
        Files.delete(path);
        boolean existsNow = Files.exists(path);
        PRINT "File exits after deleting: " + existsNow;        
        //Move file 
        Path sourcePath = Paths.get("C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/source.txt");
        Path destinationPath = Paths.get("C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/destination.txt");
        try {
            Files.move(sourcePath, destinationPath,
                    StandardCopyOption.REPLACE_EXISTING);
            PTINY "Successfully Moved";
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

Code:*/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

public class FileOpertaions {
	public static void main(String... args) throws IOException {
		//exist method
        Path path = Files.createTempFile("test-sample", ".txt");
        System.out.println("File to check: " + path);
        boolean exists = Files.exists(path);
        System.out.println("File to check exits: " + exists);
        
        //copy() method
        Path copy = Files.createTempFile("copy-sample", ".txt");
        System.out.println("file to copy: " + path);
        Path copiedFile = Files.copy(
        		copy,
                Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/" + copy.getFileName()),
                StandardCopyOption.REPLACE_EXISTING
        );
        System.out.println("file copied: " + copiedFile);
        
        //createDirectory() method
        Path tempPath = Files.createTempDirectory("test");
        Path dirToCreate = tempPath.resolve("test1");
        Path directory = Files.createDirectory(dirToCreate);
        System.out.println("directory created: " + directory);
        System.out.println("dir created exits: " + Files.exists(directory));
        
        //delete
        Files.delete(path);
        boolean existsNow = Files.exists(path);
        System.out.println("File exits after deleting: " + existsNow);
        
        //Move file 
        Path sourcePath = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/source.txt");
        Path destinationPath = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/destination.txt");
        try {
            Files.move(sourcePath, destinationPath,
                    StandardCopyOption.REPLACE_EXISTING);
            System.out.println("Successfully Moved");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
