/* Write a program to write and read the string to a channel using buffer(to demonstrate Pipe)

Requirements:
    * Program to write and read the string to a channel using buffer.
    
Entities:
    * ChannelBuffer
    
Methodsignature:
    * public static void main(String[] args)
  
Jobs to be done:
    * Create Class name ChannelBuffer and Declare Main method.
    * Store the store to write in file.
    * Store file path and file name in strings.
    * File class check file exist or not.
    * Wirte string in file uisng for loop. 
         => Invoke toCharArray method store the char.
    
Pseudo Code:

public class ChannelBuffer {
  public static void main(String[] args) throws IOException {
    String phrase = new String("This content writtern by Channel Buffer");
    String dirname = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio"; 
    String filename = "destination.txt"; 
    File dir = new File(dirname); 
    File aFile = new File(dir, filename); 
    FileOutputStream outputFile = null; 
    try {
      outputFile = new FileOutputStream(aFile, true);
    } catch (FileNotFoundException e) {
      e.printStackTrace(System.err);
    }
    FileChannel outChannel = outputFile.getChannel();
    ByteBuffer buf = ByteBuffer.allocate(1024);
    for (char ch : phrase.toCharArray()) {
      buf.putChar(ch);
    }
}

Code:
*/

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;

public class ChannelBuffer {
  public static void main(String[] args) throws IOException {
    String phrase = new String("\nThis content writtern by Channel Buffer");
    String dirname = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio"; 
    String filename = "destination.txt"; 
    File dir = new File(dirname); 
    File aFile = new File(dir, filename); 
    FileOutputStream outputFile = null; 
    try {
      outputFile = new FileOutputStream(aFile, true);
      System.out.println("File stream created successfully.");
    } catch (FileNotFoundException e) {
      e.printStackTrace(System.err);
    }
    FileChannel outChannel = outputFile.getChannel();
    ByteBuffer buf = ByteBuffer.allocate(1024);
    for (char ch : phrase.toCharArray()) {
      buf.putChar(ch);
    }
    buf.flip(); 
    System.out.println("Buffer contents written to file.");
    outChannel.write(buf);
    outputFile.close(); 
  }
}