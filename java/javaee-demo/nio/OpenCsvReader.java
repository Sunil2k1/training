/* Reading a CSV file using java.nio.Files API as List string with each row in CSV as a String

Requirements:
    * Program to reading a CSV file using java.nio.Files API as List string with each row in CSV as a String.
    
Entities:
    * OpenCsvReader
    
Methodsignature:
   -none-
  
Jobs to be done:
    * Create a class name OpernCsvReader and Declare main.
    * Invoke Files class readAllLines to read all lines in file.
         => Get file path using Path class get method in parameter.
   * Using For each get the string print the CSV file content.
    
    
Pseudo Code:
public class OpenCsvReader {
	public static void main(String... args) throws IOException {
		List<String> lines = Files.readAllLines(Paths
				.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/democsv.csv"));
		for (String line : lines) {
			line = line.replace("\"", "");
			System.out.println(line);
		}
	}
}
Code:
*/
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class OpenCsvReader {
	public static void main(String... args) throws IOException {
		List<String> lines = Files.readAllLines(Paths
				.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/democsv.csv"));
		for (String line : lines) {
			line = line.replace("\"", "");
			System.out.println(line);
		}
	}
}


	

