/* Write a program to obtain current directory and parent directory 

Requirements:
    * Program to obtain current directory and parent directory
    
Entities:
    * RelativeAbsolutePath
    
Method signature:
   -none-
  
Jobs to be done:
    * Create a class name RelativeAbsolute and Declare main method.
    * Get the absolut path using
        => get method with toAbsolutePath and to get in string toString method.
    * Create File class and pass parameter as path and file 
        => Print parent using getParent method.
    
Pseudo Code:
public class CurrentParentDirectory {
	public static void main(String[] args) {
		String path = Paths.get("").toAbsolutePath().toString();
		PRINT "Working Directory = " + path;
		File file = new File(path, "CurrentParentDirectory.java");
		PRINT "File: " + file;
		PRINT "Parent: " + file.getParent();
	}
}

Code:
*/

import java.io.File;
import java.nio.file.Paths;

public class CurrentParentDirectory {
	public static void main(String[] args) {
		String path = Paths.get("").toAbsolutePath().toString();
		System.out.println("Working Directory = " + path);
		File file = new File(path, "CurrentParentDirectory.java");
		System.out.println("File: " + file);
		System.out.println("Parent: " + file.getParent());
	}
}
