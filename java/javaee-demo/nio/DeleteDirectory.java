 /*  Delete the directory along with the files recursively.

Requirements:
    * Program to delete the directory along with the files recursively.
    
Entities:
    * DeleteVisitFileMethod
    
Method signature:
   * public void deleteDir(File file)
Jobs to be done:
    * Create a Class name DeleteDirectory.
    * Store files path directory to delete all files recursively.
    * Create File class pass files path directory as argument.
    * Check the file not exixts 
        => Not exists print file not exist.
        => Create an object for DeleteDirectory class deleteDir method.
    * Check directory contains files and check length of files delete files using for each
        => Files not in directory delete directory using delete method
    
Pseudo Code:
public class DeleteDirectory {
	private static final String src_delete = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/src_delete";
	public static void main(String[] args) throws IOException {
		File file = new File(src_delete);
		IF !file.exists() {
			PRINT "Directory does not exist.";
		} ELSE {
			DeleteDirectory deleteDirectory = new DeleteDirectory();
			deleteDirectory.deleteDir(file);
		}
	}
	public void deleteDir(File file) THROWS IOException {
		IF (file.isDirectory()) {
			IF (file.list().length == 0) {
				deleteEmptyDir(file);
			} ELSE {
				File files[] = file.listFiles();
				FOR (File fileDelete : files) {
					deleteDir(fileDelete);
				}
				IF (file.list().length == 0) {
					deleteEmptyDir(file);
				}
			}
		} ELSE {
			file.delete();
			PRINT "File is deleted : " + file.getAbsolutePath();
		}
	}

	private void deleteEmptyDir(File file) {
		file.delete();
		PRINT "Directory is deleted : " + file.getAbsolutePath();
	}
}


Code:
*/





import java.io.File;
import java.io.IOException;
public class DeleteDirectory {
	private static final String delete = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/delete.txt";
	public static void main(String[] args) throws IOException {
		File file = new File(delete);
		if (!file.exists()) {
			System.out.println("Directory does not exist.");
		} else {
			DeleteDirectory deleteDirectory = new DeleteDirectory();
			deleteDirectory.deleteDir(file);
		}
	}
	public void deleteDir(File file) throws IOException {
		if (file.isDirectory()) {
			if (file.list().length == 0) {
				deleteEmptyDir(file);
			} else {
				File files[] = file.listFiles();
				for (File fileDelete : files) {
					deleteDir(fileDelete);
				}
				if (file.list().length == 0) {
					deleteEmptyDir(file);
				}
			}
		} else {
			file.delete();
			System.out.println("File is deleted : " + file.getAbsolutePath());
		}
	}
	private void deleteEmptyDir(File file) {
		file.delete();
		System.out.println("Directory is deleted : " + file.getAbsolutePath());
	}
}
