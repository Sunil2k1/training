/*Write a program to demonstrate absolute and relative path.


Requirements:
    * Program to demonstrate absolute and relative path.
    
Entities:
    * RelativeAbsolutePath
    
Method signature:
   -none-
  
Jobs to be done:
    * Store the path to create a file specified path.
    * Create FileOutputStream class with path and true value to create file.
    * Create PrintWriter class to write a file 
          => Write in file using println 
          => Close the abosluteprinter.
    * Create FileOutputStream class with file name and true value to create file.
    *Create PrintWriter class to write a file 
          => Write in file using println 
          => Close the abosluteprinter.
    
Pseudo Code:
public class RelativeAbsolutePath {
	public static void main(String[] args) throws IOException {

		String aboslute = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/absolute.txt";
		FileOutputStream aboslutefos = new FileOutputStream(aboslute, true);
		PrintWriter abosluteprinter = new PrintWriter(aboslutefos);
		abosluteprinter.println("Sample File For Absolute");
		abosluteprinter.close();		
		FileOutputStream fos = new FileOutputStream("relative.txt", true);
		PrintWriter printer = new PrintWriter(fos);		
		PRINT "Sample File For Relative";
		printer.close();
		PRINT "File Successfully Created and Written";
	}
}

Code:*/

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

public class RelativeAbsolutePath {
	public static void main(String[] args) throws IOException {

		String aboslute = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/absolute.txt";
		FileOutputStream aboslutefos = new FileOutputStream(aboslute, true);
		PrintWriter abosluteprinter = new PrintWriter(aboslutefos);
		abosluteprinter.println("Sample File For Absolute");
		abosluteprinter.close();
		
		FileOutputStream fos = new FileOutputStream("relative.txt", true);
		PrintWriter printer = new PrintWriter(fos);
		
		printer.println("Sample File For Relative");
		printer.close();
		System.out.println("File Successfully Created and Written");
	}
}
