/* Given a path, check if path is file or directory
Requirements:
    * Program to check if path is file or directory

Entities:
    * FileOrDiretory
    
Method signature:
   -none-
  
Jobs to be done:
    * Create a class name FileOrDiretory and Declare main.
    * Get the file using File class passing parameter in File class.
    * Invoke isFile method to check path is file or directory 
         => Check and Print file or directory.
    
    
Pseudo Code:
public class FileOrDiretory {
	public static void main(String[] args) {
		File file = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/nio.txt");
		IF (file.isFile()) {
			PRITN "The Given path is file";
		}  ELSE {
			PRINT "The Given path is directory";
		}
		
	}
}
Code:
*/

import java.io.File;

public class FileOrDiretory {
	public static void main(String[] args) {
		File file = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/nio.txt");
		if (file.isFile()) {
			System.out.println("The Given path is file");
		} else {
			System.out.println("The Given path is directory");
		}
	}
}
