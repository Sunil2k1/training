/* InputStream to String and vice versa

Requirements:
    * Program to InputStream to String and vice versa.
    
Entities:
    * InputStreamToString
    
Method signature:
   -none-
  
Jobs to be done:
    * Create FileInputStream class to read the file 
        => Specify file path in argument of FileInputStream class.
    * Create InputStreamReader class and read the file using BufferedReader class.
    * Read file string using StringBuffer class.
    * Invoke readLine method and read line in file.
    * Print the string in file.
    
Pseudo Code:
public class InputStreamToString{
   public static void main(String args[]) throws IOException {
      InputStream inputStream = new FileInputStream("C:/Users/santh/eclipse-workspace/JavaEE-Demo/advance.nio/sample nio.txt");
      InputStreamReader isReader = new InputStreamReader(inputStream);
      BufferedReader reader = new BufferedReader(isReader);
      StringBuffer sb = new StringBuffer();
      String string;
      WHILE (string = reader.readLine())!= null{
         sb.append(string);
      }
      PRINT sb.toString();
   }
}

Code:
*/
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
public class InputStreamToString{
   public static void main(String args[]) throws IOException {
      InputStream inputStream = new FileInputStream("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/nio.txt");
      InputStreamReader isReader = new InputStreamReader(inputStream);
      BufferedReader reader = new BufferedReader(isReader);
      StringBuffer sb = new StringBuffer();
      String string;
      while((string = reader.readLine())!= null){
         sb.append(string);
      }
      System.out.println(sb.toString());
   }
}