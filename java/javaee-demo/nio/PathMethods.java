 /* Write a program to demonstrate toAbsolutePath(), normalize(), getName(), getFileName() and getFileCount()

Requirements:
    * Program to demonstrate toAbsolutePath(), normalize(), getName(), getFileName() and getFileCount().
    
Entities:
    * PathMethods
    
Method signature:
   -none-
  
Jobs to be done:
    * Get the file path using Paths class get method pass file name.
          => Get absolut path using toAbsolutePath method.
          => Print the absolute path.
    * Get the file path using Paths class get method pass file path.
          => Get normalize path using normalize method.
          => Print the normalize path.
    * Get the file path using Paths class get method pass file path.
          => Get get index file Name using getName method.
          => Print the specified index file path.
    * Get the file path using Paths class get method pass file path.
          => Get get file Name using getFileName method.
          => Print the file path.
    * Get the file path using Paths class get method pass file path.
          => Get get the no of in directory using size method with length.
          => Print the no of files in directory  
Pseudo Code:
public class PathMethods {
	public static void main(String[] args) {
		Path path = Paths.get("PathMethods.java");
		Path absPath = path.toAbsolutePath();
		System.out.println("Absolute Path: " + absPath);		
		Path normalPath = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/PathMethods.java");
		Path normalizedPath = normalPath.normalize();
		System.out.println("Normalized Path : " + normalizedPath);
		Path getNamePath = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
		Path indexpath = getNamePath.getName(4);
		System.out.println("Index 4: " + indexpath);
		Path pathGet = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/PathMethods.java");
		Path fileName = pathGet.getFileName();
		System.out.println("FileName: " + fileName.toString());
		File directory = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
		int fileCount = directory.list().length;
		System.out.println("File Count:" + fileCount);
	}
}


Code:*/

import java.io.File;
import java.nio.file.Path;
import java.nio.file.Paths;

public class PathMethods {
	public static void main(String[] args) {
		Path path = Paths.get("PathMethods.java");
		Path absPath = path.toAbsolutePath();
		System.out.println("Absolute Path: " + absPath);
		Path normalPath = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/PathMethods.java");
		// normalize the path
		Path normalizedPath = normalPath.normalize();
		// print normalized path
		System.out.println("Normalized Path : " + normalizedPath);
		Path getNamePath = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");		
		Path indexpath = getNamePath.getName(4);
		// print ParentPath
		System.out.println("Index 4: " + indexpath);
		Path pathGet = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/PathMethods.java");
		Path fileName = pathGet.getFileName();
		System.out.println("FileName: " + fileName.toString());		
		File directory = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/");
		int fileCount = directory.list().length;
		System.out.println("File Count:" + fileCount);
	}
}
