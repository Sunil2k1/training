
/*  Get the permission allowed for a file
Requirements:
    * Program to get the permission allowed for a file.
    
Entities:
    * GetFilePermission
    
Method signature:
   -none-
  
Jobs to be done:
    * Create a Class name GetFilePermission and Declare the main method.
    * Pass the file path to File class constructor.
    * Check file exists or not using exists method.
    * File exists 
           => Invoke canExecute method file allowed to execute or not.
           => Invoke canRead method file allowed to read or not.
           => Invoke canWrite method file allowed to write or not.
    
Pseudo Code:

public class GetFilePermission {
	public static void main(String[] args) {
		File file = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/nio.txt");
		boolean exists = file.exists();
		IF exists == true {
			PRINT "Executable: " + file.canExecute();
			PRINT "Readable: " + file.canRead();
			PRINT "Writable: " + file.canWrite();
		} ELSE {
			PRINT "File not found.";
		}
	}
}
Code:
*/
import java.io.File;

public class GetFilePermission {
	public static void main(String[] args) {
		// creating a file instance
		File file = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/nio.txt");
		boolean exists = file.exists();
		if (exists == true) {
			System.out.println("Executable: " + file.canExecute());
			System.out.println("Readable: " + file.canRead());
			System.out.println("Writable: " + file.canWrite());
		} else {
			System.out.println("File not found.");
		}
	}
}
