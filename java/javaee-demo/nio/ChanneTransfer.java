/*Create two files named source and destination .Transfer the data from source file to destination file 
using nio filechannel.


Requirements:
    * Program to transfer the data from source file to destination file using nio filechannel.
    
Entities:
    * ChanneTransfer
    
Method signature:
   -none-
  
Jobs to be done:
    * Create a Class name ChanneTransfer and Declare main method.
    * Store the array of file path to transfer files. 
    * Store the file path to distination files.
    * Invoke FileOutputStream class pass argument as new File with destination file string.
    * Invoke WritableByteChannel class getChannel method to get channel path.
    * For loop Get the source file
        => Transfer the file content source to destination using transferTo method
        => Close inputChannel and fis using close method.
    
Pseudo Code:
public class ChanneTransfer {
	public static void main(String[] argv) throws Exception {
		String[] inputFiles = new String[] { "C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/source.txt"};
		String outputFile = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/nio/destination.txt";
		FileOutputStream fos = new FileOutputStream(new File(outputFile));
		WritableByteChannel targetChannel = fos.getChannel();
		FOR int i = 0; i < inputFiles.length; i++ {
			FileInputStream fis = new FileInputStream(inputFiles[i]);
			FileChannel inputChannel = fis.getChannel();

			inputChannel.transferTo(0, inputChannel.size(), targetChannel);
			inputChannel.close();
			fis.close();
		}
		PRINT "The data successfully transfer from source file to destination file";
		targetChannel.close();
		fos.close();
	}
}

Code:
*/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;

public class ChanneTransfer {
	public static void main(String[] argv) throws Exception {		
		String[] inputFiles = new String[] { "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/text.txt"}; // Input files
		String outputFile = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/destination.txt";
		FileOutputStream fos = new FileOutputStream(new File(outputFile));
		WritableByteChannel targetChannel = fos.getChannel();
		for (int i = 0; i < inputFiles.length; i++) {
			FileInputStream fis = new FileInputStream(inputFiles[i]);
			FileChannel inputChannel = fis.getChannel();
			inputChannel.transferTo(0, inputChannel.size(), targetChannel);
			inputChannel.close();
			fis.close();
		}
		System.out.println("The data successfully transfer from source file to destination file");
		targetChannel.close();
		fos.close();
	}
}
