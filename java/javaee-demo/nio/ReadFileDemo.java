/*  Read a file using java.nio.Files using Paths

Requirements:
    * Program to read a file using java.nio.Files using Paths.
    
Entities:
    * ReadFileDemo
    
Method signature:
   -none-
  
Jobs to be done:
    * Create a Class name ReadFileDemo and Declare main method.
    * Try block
         => Pass file path in get method parameter using Paths class.
         => Read the file by bytes using Files class readAllBytes method.
         => Read the file by lines using Files class readAllLines method.
         => Print the file read bytes and lines.
    * Catch Exception invoke printStackTrace method.
    
    
Pseudo Code:

public class ReadFileDemo {
	public static void main(String[] args) {		
		Path path = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/nio.txt");
		TRY {
			byte[] bs = Files.readAllBytes(path);
			List<String> strings = Files.readAllLines(path);
			
			PRINT "Read bytes: \n"+new String(bs);
			PRINT "Read lines: \n"+strings;
		} CATCH Exception e {
			e.printStackTrace();
		}
	}
}

Code:
*/
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class ReadFileDemo {

	public static void main(String[] args) {
		
		Path path = Paths.get("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/nio/nio.txt");
		try {
			byte[] bs = Files.readAllBytes(path);
			List<String> strings = Files.readAllLines(path);			
			System.out.println("Read bytes: \n"+new String(bs));
			System.out.println("Read lines: \n"+strings);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
