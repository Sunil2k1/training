/* Demonstrate the catching multiple exception with example.
 
 Requirements:
   * Demonstrate the catching multiple exception with example. 
 Entity:
   * MultipleException
 Function Declaration:
   -none
 Jobs to be done:
  * Create a public class MultipleException declare the main.
  * try 
     => Create  a array name array in Integer data type.
     => Put array is equal to 1 / 0 
  *catch using ArithmeticException Or ArrayIndexOutOfBoundsException
     => Print "ArithmeticException or ArrayIndexOutOfBoundsException"   
  *catch using Exception 
     => Print "Other Exception".

Psudocode:

public class MultipleException {    
    public static void main(String[] args) {    
        TRY {    
            int array[] = new int[2]; // create a array    
            array[2] = 1/0;    
        }CATCH ArithmeticException OR ArrayIndexOutOfBoundsException  {   
            System.out.println("ArithmeticException or ArrayIndexOutOfBoundsException");  
        } CATCH Exception  { 
            PRINT "Other Exception";  //multi catch
        }
    }    
}
code:
*/


public class MultipleException {    
    public static void main(String[] args) {    
        try {    
            int array[] = new int[2];    
            array[2] = 1/0;    
        }catch(ArithmeticException | ArrayIndexOutOfBoundsException e) {   
            System.out.println("ArithmeticException or ArrayIndexOutOfBoundsException");  
        } catch(Exception e) { 
            System.out.println("Other Exception");  //multi catch
        }
    }    
}