/* Explain the below program.
         try{
          dao.readPerson();
        } catch (SQLException sqlException) {
        throw new MyException("wrong", sqlException);
      }
Ans:
 In this code Exception wrapping is present.dao.readPerson() method can throw an SQLException.
 Suppose it does the SQLException is caught and wrapped in a MyException.*/