/* THROWS

Requirements:
   -Throws with example. 
Entity:
   - ThrowsDemo
Function Declaration:
   division(int a, int b)
Jobs to be done:
  * Create a public class ListOfNumbers
  * In division(int a, int b) method throws ArithmeticException
      => Integer d is equal to a/b
      => return d
  * In Main Method
      => Create a object for ThrowsDemo name throwdemo.
      => try
           ->Call the division method by passing parameter inside that method and print it
      => catch using ArithmeticException Exception        
           -> Print  "Number Should not be Divide By Zero".
  
Psudocode:

public class ThrowsDemo{  
   int division(int a, int b) THROWS ArithmeticException{  
	int d = a/b;
	RETURN d;
   }  
   public static void main(String[] args){  
	   ThrowsDemo throwdemo = new ThrowsDemo();
	TRY{
	   PRINT throwdemo.division(2,0);  
	}
	CATCH ArithmeticException{
	   PRINT "Number Should not be Divide By Zero";
	}
   }  
}
*/
public class ThrowsDemo{  
   int division(int a, int b) throws ArithmeticException{  
	int d = a/b;
	return d;
   }  
   public static void main(String[] args){  
	   ThrowsDemo throwdemo = new ThrowsDemo();
	try{
	   System.out.println(throwdemo.division(2,0));  
	}
	catch(ArithmeticException e){
	   System.out.println("Number Should not be Divide By Zero");
	}
   }  
}
