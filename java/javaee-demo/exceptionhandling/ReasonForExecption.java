/* Handle and give the reason for the exception in the following code: 
        PROGRAM:
             public class Exception {  
             public static void main(String[] args)
             {
  	       int arr[] ={1,2,3,4,5};
	      System.out.println(arr[7]);
            }
          }
//Display the output.

Requirement
   * This code required a integer array with integer in the array.
Entity
  * ReasonForExecption
Function declaration
  -none-
Jobs to be done
  * Create a public class ListOfNumbers and declarE the main.
  * try 
     => create a array name arr in integer datatype and insert values 1,2,3,4,5
     => Print 7 index value of arr.
  *catch using ArrayIndexOutOfBoundsException
     => Print "The Given index is not present in the array"      

Psudocode:

public class ReasonForExecption {  
   public static void main(String[] args) {
	TRY{
	   int arr[] ={1,2,3,4,5};
	   PRINT arr[7];
	}CATCH ArrayIndexOutOfBoundsException {
	   PRINT "The Given index is not present in the array";
	}
   }
}

correct code:*/


public class ReasonForExecption {  
   public static void main(String[] args) {
	try{
	   int arr[] ={1,2,3,4,5};
	   System.out.println(arr[7]);
	}catch(ArrayIndexOutOfBoundsException e){
	   System.out.println("The Given index is not present in the array");
	}
   }
}

/*---------------Output---------------
   The Given index is not present in the array
   
   
---------------------Reason---------------
    In the Given Array , It has only five elements.But we are trying to display the 7 th index value.
so,It throw ArrayIndexOutOfBoundsException which is a unchecked exception.*/

