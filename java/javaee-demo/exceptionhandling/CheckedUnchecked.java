/* Compare the checked and unchecked exception.

Ans:
* Checked exceptions :
    * java.lang.Exception -  checked exception.
    * Checked exceptions which are known to compiler and that are checked only at compile time . 
	* It is also called as Compile time exceptions	
    * Example:
	    IOException, ClassNotFoundException,SQLException.

* Unchecked exceptions :
    * All the sub classes of java.lang.Error and java.lang.RunTimeException are unchecked exceptions.
    * Unchecked exceptions occur only at run time.
    * Unchecked exceptions are those exceptions which are not at all known to compiler.
	* It is also called as run time exceptions.	
    * Example
       NullPointerException, ArrayIndexOutOfBoundsException, ArithmeticException, IllegalArgumentException, NumberFormatException*/