/*Difference between catching multiple exceptions and Mutiple catch blocks.

Catching multiple exceptions:
   * This is also known as multi catch,We could separate different exceptions using pipe ( | ).
Example
try{
 //code . . . . . 
}catch(IOException | SQLException ex)//syntax{
 //code . . . . . 
}

Mutiple catch blocks:
       * For catching different exceptions need to write different catch blocks.
Example
try{
 //code . . . . . 
}catch(IOException ex1){
 //code . . . . .
} catch(SQLException ex2){
 //code . . . . .
}
 */