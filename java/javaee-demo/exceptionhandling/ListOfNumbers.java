/* Write a program ListOfNumbers (using try and catch block).

Requirement:
   * This code requeried list of numbers.
Entity:
   * ListOfNumbers
Function Declaration:
  -none-
Jobs to be done:
  * Create a public class ListOfNumbers and 
  * Create a array name number in Static Integer Data type before declaring the main.
  * try 
     => Put number is equal to 2
  *catch using Number Format Exception 
     => Print the message which is occur during exception.      
  *catch using IndexOutOfBounds Exception 
     => Print the message which is occur during exception. 
     
 Psudocode:
 
public class ListOfNumbers {
  public static  int[] numbers = new int[10]; //create a array
  public static void main(String[] args) {
    TRY {
      NUMBER[10] = 2;
    } CATCH NumberFormatException {
      PRINT getMessage();
    } CATCH IndexOutOfBoundsException  {
      PRINT getMessage();
    }
  }
}       
 */


public class ListOfNumbers {
  public static  int[] numbers = new int[10];
  public static void main(String[] args) {
    try {
      numbers[10] = 2;
    } catch (NumberFormatException e1) {
      System.out.println("NumberFormatException " + e1.getMessage());
    } catch (IndexOutOfBoundsException e2) {
      System.out.println("IndexOutOfBoundsException " + e2.getMessage());
    }
  }
}
