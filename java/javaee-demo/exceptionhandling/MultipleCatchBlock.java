/* Mutiple catch block -  
       a]Explain with Example
       b]It is possible to have more than one try block? - Reason.
 Requirements:
    * Explain with Example
    * It is possible to have more than one try block? - Reason.
 Entity:
   * MultipleCatchBlock  
 Function Declaration:
   -none-
Jobs to be done:
  * Create a public class MultipleCatchBlock and declaring the main.
  * try 
     => create a array name arr in integer datatype 
     => Put arr is equal to 2 /0
     => print "Last Statement of try block" 
  * catch using ArrayIndexOutOfBoundsException 
     => Print "The given value is out of Bound"      
  * catch using ArithmeticException Exception 
     => Print "A Integer should not divide by zero"
  * catch using Exception 
     => Print "Some Other Exception"
  * Print  "Out of try-catch block"
  
Psudocode:

public class MultipleCatchBlock {   
   public static void main(String[] args){
      TRY{
         int arr[]=new int[6];
         arr[6] = 2/0;
         PRINT "Last Statement of try block";
      }CATCH ArrayIndexOutOfBoundsException {
         PRINT "The given value is out of Bound";
      }
      CATCH ArithmeticException {
         PRINT "A Integer should not divide by zero";
      }
      CATCH Exception e{
         PRINT "Some Other Exception";
      }
      PRINT "Out of try-catch block";
   }
}
Code:*/
public class MultipleCatchBlock {  
  
   public static void main(String[] args){
      try{
         int arr[]=new int[6];
         arr[6] = 2/0;
         System.out.println("Last Statement of try block");
      }catch(ArrayIndexOutOfBoundsException e){
         System.out.println("The given value is out of Bound");
      }
      catch(ArithmeticException e){
         System.out.println("A Integer should not divide by zero");
      }
      catch(Exception e){
         System.out.println("Some Other Exception");
      }
      System.out.println("Out of try-catch block");
   }
}
//In this We Get ArithmeticException A Integer should not divide by zero
/*
Reason:
           No,it is not possible to have more than one try block,each try block must be followed by catch or finally.
We cannot have multiple try blocks with a single catch block.Still if we try to have single catch block for multiple 
try blocks a compile time error is generated.*/

