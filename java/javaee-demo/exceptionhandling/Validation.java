/*Why validation is important?
      When receiving input that needs to be validated before it can be used,validate all input before using it,
      to avoid leaving the application in a half valid state.
Example:
           check if user already exists
           validate user name
           validate user age
           insert user name

b)Yes,the following code can be done as it is.
Code:
           check if user already exists
           validate user name
           validate user age
           insert user name

*/