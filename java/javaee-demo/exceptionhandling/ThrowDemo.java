/*DIFFERENCE BETWEEN THROWS AND THROW:
* Thorw
   * Throw is a keyword which is used to throw an exception explicitly in the program inside a function or inside a block of code.
    Example for Throw:
    
    
   void throwMethod() {
   try {
      throw new ArithmeticException("Something went wrong in code");
   } 
   catch (Exception e) {
      System.out.println("Error in code ");
      }
   }

* Throws:
  * Throws is a keyword used in the method signature used to declare an exception 
    which might get thrown by the function while executing the code.
    Example for Throws:
             void sample() throws ArithmeticException{
                          }*/