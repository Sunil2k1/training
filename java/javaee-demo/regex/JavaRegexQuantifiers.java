/*write a program for java regex quantifier?
Requirements:
   * String
Entities
   * JavaRegexQuantifiers
Function Declaration
   -none-
Jobs to be done
 * Create a Class name JavaRegexQuantifiers and declare main method.
 * store the  text As type String
 * Create a patten o+
 * invoking matcher method by passing the String a and stored in matcher. 
 * Invoke the matcher() method for matcher and store it in matchFound as Boolean
 * Check while m.find
      => Print start index element and end index element
Psudocode:

  public class JavaRegexQuantifiers {
    public static void main(String[] args) 
    {   String text = "Google is good";
        Pattern p = Pattern.compile("o+");   
        Matcher m = p.matcher(text);    
        while (m.find()) 
            PRINT "Pattern found from " + m.start() +" to " + (m.end()-1); 
   
    } 
}

 */
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JavaRegexQuantifiers {
    public static void main(String[] args) 
    {   String text = "Google is good";
        Pattern p = Pattern.compile("o+");   
        // Making an instance of Matcher class 
        Matcher m = p.matcher(text);    
        while (m.find()) 
            System.out.println("Pattern found from " + m.start() + 
                               " to " + (m.end()-1)); 
   
    } 
}
