/*create a pattern for password which contains
   8 to 15 characters in length
   Must have at least one uppercase letter
   Must have at least one lower case letter
   Must have at least one digit

Requirements:
   * A string contains length 8 to 15 atleast one digit,lower case letter,uppercase letter
Entities
   * CreatePatten
Function Declaration
   -none-
Jobs to be done
 * Create a Class name CreatePatten and declare main method.
 * Create A string contains length 8 to 15 atleast one digit,lower case letter,uppercase letter
 * Create a patten ^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$
 * invoking matcher method by passing the String a and stored in matcher. 
 * Invoke the find() method for matcher and store it in matchFound as Boolean
 * Check if
    => match is found
    => print "Match found"
 * Else
    => print "Match not found"   

 Psudocode:
 
public class CreatePatten {
	public static void main (String [] args) {
		String a = "Lamb0gini";
		Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$");
	    Matcher matcher = pattern.matcher(a);
	    boolean matchFound = matcher.find();
	    IF matchFound {
	      PRINT "Match found";
	    } else {
	      PRINT "Match not found";
	    }
	}
}
Code:*/
import java.util.regex.Matcher;
import java.util.regex.Pattern;
public class CreatePatten {
	public static void main (String [] args) {
		String a = "Lamb0gini";
		Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$");
	    Matcher matcher = pattern.matcher(a);
	    boolean matchFound = matcher.find();
	    if(matchFound) {
	      System.out.println("Match found");
	    } else {
	      System.out.println("Match not found");
	    }
	}
}
