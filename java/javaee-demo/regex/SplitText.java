/*Split any random text using any pattern you desire

* Requirements:
   * Split any random text using any pattern you desire
* Entities
   * SplitText
* Function Declaration
   -none-
* Jobs to be done
 * Create a Class name SplitText and declare main method.
 * test is splitted and Stored in String array name splitString
 * For every splitString
    => print the elements of splitStrings

Psudocode:

public class SplitText{  
	public static void main(String args[]){  
		ADD A STRING TO text;  
		test IS SPLITTED AND STORED IN A STRING ARRAY NAME splitString;
		FOR  (int i=0; i < splitString.length; i++){
			PRINT splitString[i];
    }
  }
} 
*/


public class SplitText{  
	public static void main(String args[]){  
		String text ="split any random text using any pattern you desire"; 
		String[] splitString = text.split("\\s", 5);
		for (int i=0; i < splitString.length; i++){
			System.out.println(splitString[i]);
    }
  }
}