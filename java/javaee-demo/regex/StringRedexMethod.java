/* write a program for Java String Regex Methods?
Requirements:
   * String
Entities
   * StringRedexMethod
Function Declaration
   -none-
Jobs to be done
 * Create a Class name StringRedexMethod and declare main method.
 * store the  text As type String
 * Create a patten t.
 * invoking matcher method by passing the String a and stored in matcher. 
 * Invoke the matches() method for matcher and store it in b as Boolean
 * print b
Psudocode:
public class StringRedexMethod {

		public static void main(String[] args) {
			String text = "to";
			Pattern p = Pattern.compile("t.");//. represents single character  
			Matcher m = p.matcher(text);  
			boolean b = m.matches();  		  
			PRINT b; 
		}

	}

*/

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class StringRedexMethod {

		public static void main(String[] args) {
			String text = "to";
			Pattern p = Pattern.compile("t.");//. represents single character  
			Matcher m = p.matcher(text);  
			boolean b = m.matches();  		  
			System.out.println(""+b); 
		}

	}

