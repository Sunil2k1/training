/*  For the following code use the split method() and print in sentence
    String website = "https-www-google-com";
Requirements:
   * String
Entities
   * UseSplitMethod
Function Declaration
   -none-
Jobs to be done
 * Create a Class name UseSplitMethod and declare main method.
 * store the  text As type String
 * Split the Text by - 
 * For each word
     => print word 

Psudocode:

public class UseSplitMethod {
	public static void main(String args[]){  
		String text ="https-www-google-com"; 
		String[] splitString = text.split("-", 4);
		for (int i=0; i < splitString.length; i++){
			PRINT splitString[i];
			}
    }
}
 */
public class UseSplitMethod {
	public static void main(String args[]){  
		String text ="https-www-google-com"; 
		String[] splitString = text.split("-", 4);
		for (int i=0; i < splitString.length; i++){
			System.out.println(splitString[i]);
			}
    }
}
