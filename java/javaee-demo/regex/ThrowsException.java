/*
 2.Create a program which throws exception and fetch all the details of the exception. 
 
1.Requirements:
   * Create a program which throws exception and fetch all the details of the exception.
2.Entities
   * ExceptionHandling
3.Function Declaration
   * public static void main (String [] args)
4.Jobs to be done
  * Create a Class name ExceptionHandling and declare main method with throws FileNotFound Exception and IOException.
  * Try
      => call testExeption() Method by passing parameter in it.
  * Catch the  FileNotFoundException
      =>  Print result of printStackTrace() method 
  * Catch the  IOException
      =>  Print result of printStackTrace() method        
  * Finnaly
      => Print "Releasing resources"
  * call testExeption() Method by passing parameter in it.
  * In void  testException(int i) Throws FileNotFoundException, IOException 
      => Check if i < 0
             -> Invoke the FileNotFoundExeption and store it in myExeption
      => Else if i > 10
             -> throw new IOException          
  
Psudocode:

public class ThrowsException {

	public static void main(String[] args) THROWS FileNotFoundException, IOException {
		TRY{
			testException(-5);
			testException(-10);
		}CATCH FileNotFoundException {
			e.printStackTrace();
		}CATCH IOException {
			e.printStackTrace();
		}FINNALY{
			PRINT "Releasing resources";			
		}
		testException(15);
	}
	
	public static void testException(int i) THROWS FileNotFoundException, IOException{
		IF i < 0{
			FileNotFoundException myException = new FileNotFoundException("Negative Integer "+i);
			throw myException;
		} ELSE IF i > 10{
			throw new IOException("Only supported for 0 to 10");
		}

	}

}  
 
*/

import java.io.FileNotFoundException;
import java.io.IOException;

public class ThrowsException {

	public static void main(String[] args) throws FileNotFoundException, IOException {
		try{
			testException(-5);
			testException(-10);
		}catch(FileNotFoundException e){
			e.printStackTrace();
		}catch(IOException e){
			e.printStackTrace();
		}finally{
			System.out.println("Releasing resources");			
		}
		testException(15);
	}
	
	public static void testException(int i) throws FileNotFoundException, IOException{
		if(i < 0){
			FileNotFoundException myException = new FileNotFoundException("Negative Integer "+i);
			throw myException;
		}else if(i > 10){
			throw new IOException("Only supported for 0 to 10");
		}

	}

}