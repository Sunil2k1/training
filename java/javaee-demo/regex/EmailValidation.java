/* write a program for email-validation?
Requirements:
   * email
Entities
   * EmailValidation 
Function Declaration
   -none-
Jobs to be done
 * Create a Class name EmailValidation and declare main method.
 * Create a object for Scanner name sc.
 * Get the username from user and stor it in username As type String
 * Create a patten ^(.+)@(.+)$
 * invoking matcher method by passing the String a and stored in matcher. 
 * Invoke the matcher() method for matcher and store it in matchFound as Boolean
 * Check if
    => match is found
    => print "Given Email is vallid"
 * Else
    => print "Given Email is not vallid"  

Psudocode:
public class EmailValidation {
	public static void main(String args[]){  
		Scanner sc = new Scanner (System.in);
		String text =sc.nextLine(); 
		Pattern p = Pattern.compile("^(.+)@(.+)$");
		Matcher m = p.matcher(text); 
		IF m.find() {
			PRINT " Given Email is vallid ";
		}
		else {
			PRINT "Given Email is not vallid"; 
		}
    }
}     
*/
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class EmailValidation {
	public static void main(String args[]){  
		Scanner sc = new Scanner (System.in);
		String text =sc.nextLine(); 
		Pattern p = Pattern.compile("^(.+)@(.+)$");
		Matcher m = p.matcher(text); 
		if(m.find()) {
			System.out.println(" Given Email is vallid ");
		}
		else {
			System.out.println("Given Email is not vallid"); 
		}
    }
}
