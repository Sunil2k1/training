/*create a username for login website which contains
   8-12 characters in length
   Must have at least one uppercase letter
   Must have at least one lower case letter
   Must have at least one digit

Requirements:
   * A string contains length 8 to 15 atleast one digit,lower case letter,uppercase letter
Entities
   * CreateUserName 
Function Declaration
   -none-
Jobs to be done
 * Create a Class name CreateUserName  and declare main method.
 * Create a object for Scanner name sc.
 * Get the username from user and stor it in username As type String
 * Create a patten ^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$
 * invoking matcher method by passing the String a and stored in matcher. 
 * Invoke the find() method for matcher and store it in matchFound as Boolean
 * Check if
    => match is found
    => print "Match found"
 * Else
    => print "Match not found"   

 Psudocode:
 
public class CreatePatten {
	public static void main (String [] args) {
		Scanner sc = new Scanner (System.in);
		String username = sc.nextLine();
		Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$");
	    Matcher matcher = pattern.matcher(username);
	    boolean matchFound = matcher.find();
	    IF matchFound {
	      PRINT "Username is valid";
	    } 
	    ELSE {
	      System.out.println("Username is not valid");
	    }
	}
}
Code:
 */
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CreateUserName {
	public static void main(String [] args) {
		Scanner sc = new Scanner (System.in);
		String username = sc.nextLine();
		Pattern pattern = Pattern.compile("^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[a-zA-Z\\d]{8,15}$");
	    Matcher matcher = pattern.matcher(username);
	    boolean matchFound = matcher.find();
	    if(matchFound) {
	      System.out.println("Username is valid");
	    } else {
	      System.out.println("Username is not valid");
	    }
	}


}
