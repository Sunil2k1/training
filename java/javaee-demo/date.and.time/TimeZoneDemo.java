/*Display the default time zone and Any three particular time zone as mentioned below.
     Sample output: 
        Displaying current of the particular TimeZones
        GMT-27-09-2020 Sunday 08:36:28 AM
        Europe/Copenhagen-27-09-2020 Sunday 10:36:28 AM
        Australia/Perth-27-09-2020 Sunday 04:36:28 PM
        America/Los_Angeles-27-09-2020 Sunday 01:36:28 AM

Requirement :
    * Program to Display the default time zone and Any three particular time 
zone as mentioned below. 
Entity:
    * TimeZoneDemo
Method declaration:
    -none-
Jobs To be Done:
    * Create a Class name TimeZoneDemo and Declare main Method.
    * Invoke differentTZTimings static method.
    * Invoke TimeZone class getTimeZone method to get time zone.
         => Set Date Format using SimpleDateFormat class setTimeZone method.
         => Using date using format method
         => Print current time zone.
    * Get the 
         => Asia/Kolkata
         => Australia/Perth Zone
         => Using getTimeZone method
    * Set the time zone using setTimeZone method.
    * Set the date format using format method.
        
    
Pseudo code:
public class TimeZoneDemo {
	public static void differentTZTimings() {
		TimeZone timeZone = TimeZone.getTimeZone("GMT");
		SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
		date_format.setTimeZone(timeZone);
		Date date = new Date();
		String current_date_time = date_format.format(date);
		PRINT "GMT-" + current_date_time;

		timeZone = TimeZone.getTimeZone("Asia/Kolkata");
		date_format.setTimeZone(timeZone);
		current_date_time = date_format.format(date);
		PRINT "Asia/Kolkata-" + current_date_time;

		timeZone = TimeZone.getTimeZone("Australia/Perth");
		date_format.setTimeZone(timeZone);
		current_date_time = date_format.format(date);
		PRINT "Australia/Perth-" + current_date_time;
	}

	public static void main(String[] args) {
		PRINT "Displaying current of the particular TimeZones";
		differentTZTimings();
	}
}
 
*/

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class TimeZoneDemo {
	public static void differentTZTimings() {
		TimeZone timeZone = TimeZone.getTimeZone("GMT");
		SimpleDateFormat date_format = new SimpleDateFormat("dd-MM-yyyy EEEE hh:mm:ss a");
		date_format.setTimeZone(timeZone);
		Date date = new Date();
		String current_date_time = date_format.format(date);
		System.out.println("GMT-" + current_date_time);
		timeZone = TimeZone.getTimeZone("Asia/Kolkata");
		date_format.setTimeZone(timeZone);
		current_date_time = date_format.format(date);
		System.out.println("Asia/Kolkata-" + current_date_time);
		timeZone = TimeZone.getTimeZone("Australia/Perth");
		date_format.setTimeZone(timeZone);
		current_date_time = date_format.format(date);
		System.out.println("Australia/Perth-" + current_date_time);
	}
	public static void main(String[] args) {
		System.out.println("Displaying current of the particular TimeZones");
		differentTZTimings();
	}
}
