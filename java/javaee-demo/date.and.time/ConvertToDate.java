/*How do you convert a Calendar to Date and vice-versa with example?

Requirement :
    * Program to convert a Calendar to Date 

Entity:
    * ConvertToDate

Method Declaration:
    -none-

Jobs to be done :
     * Create a Class name ConvertToDate and Declare main method.
     * Invoke Calendar class getInstance method and store in calendar.
     * Using add method add one to Calender date.
          => Get the add one date using Calender class getTime method and store it in date.
     * Using SimpleDateFormat class set date and time that format.
          => Set the date format using format method. 
     * Print the date.
     
Pseudo Code:
public class ConvertToDate {
	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		java.util.Date date = calendar.getTime();             
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String dateFormat = format.format(date); 
		PRINT dateFormat;
		java.util.Date inActiveDate = null;
		TRY {
		    inActiveDate = format.parse(dateFormat);
		} CATCH ParseException e1 {
		    e1.printStackTrace();
		}
	}
}

Code:
*/



import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class ConvertToDate {
	public static void main(String[] args) {
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DATE, 1);
		java.util.Date date = calendar.getTime();             
		SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		String dateFormat = format.format(date); 
		System.out.println(dateFormat);
		@SuppressWarnings("unused")
		java.util.Date inActiveDate = null;
		try {
		    inActiveDate = format.parse(dateFormat);
		} catch (ParseException e1) {
		   
		    e1.printStackTrace(); // TODO Auto-generated catch block
		}
	}
}
