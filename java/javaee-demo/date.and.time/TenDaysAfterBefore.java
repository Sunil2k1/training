/*.Write a Java program to get the dates 10 days before and after today.
   6.1)using local date 
   6.2)using calendar
Requirement :
    * Program to get the dates 10 days before and after today. 
Entity:
    * TenDaysAfterBefore

Method declaration:
    * public void usingLocalDate()
    * public void usingCalender()
Jobs To be Done:
    * Create a Class name TenDaysAfterBefore
    * Invoke TenDaysAfterBefore class usingLocalDate and usingCalender method.
    * Invoke LocalDate and store current date in it
         => Print todays date , after 10 days and before 10 days using plusDays method
         

      
Pseudo code:
public class TenDaysAfterBefore {
	public void usingLocalDate() {
		LocalDate today = LocalDate.now(); 
	    PRINT today;
	    PRINT today.plusDays(-10);
	    PRINT today.plusDays(10)+"\n";
	}
	public void usingCalender() {
		Calendar calendar = new GregorianCalendar();
		int year       = calendar.get(Calendar.YEAR);  
		int month      = calendar.get(Calendar.MONTH); 
		int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH); 
		PRINT "\tCurrent Date: " +year +"-"+ month +"-"+dayOfMonth;	
	}
   public static void main(String[] args)
    {
	   TenDaysAfterBefore tenDays = new TenDaysAfterBefore();
	   tenDays.usingLocalDate();
	   tenDays.usingCalender();
   }
}
  
 
Code:
*/

import java.time.LocalDate;
import java.util.Calendar;

public class TenDaysAfterBefore {

	public void usingLocalDate() {
		LocalDate today = LocalDate.now();
		System.out.println("\tCurrent Date: " + today);
		System.out.println("\t10 days before today will be " + today.plusDays(-10));
		System.out.println("\t10 days after today will be " + today.plusDays(10) + "\n");

	}

	public void usingCalender() {
		Calendar calendar = Calendar.getInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int dayOfMonth = calendar.get(Calendar.DAY_OF_MONTH);
		System.out.println("\tCurrent Date: " + year + "-0" + month + "-0" + dayOfMonth);

	}

	public static void main(String[] args) {
		TenDaysAfterBefore tenDays = new TenDaysAfterBefore();
		System.out.println("Using Local Date:-");
		tenDays.usingLocalDate();
		System.out.println("Using Calendar:-");
		tenDays.usingCalender();
	}
}
