/* Write a Java program to demonstrate schedule method calls of Timer class ?

Requirement :
    *  Program to demonstrate schedule method calls of Timer class 

2.Entity:
    * Helper
    * TimerSchedule

3.Method Declaration:
    -none-

4.Jobs to be done :
     * Create new TimerSchedule class store as object.
     * Invoke Timer class and Helper class using TimerTask class.
     * Invoke Date class .
     * Schdule task and date using scheduleAtFixedRate method.
     * Run synchronized method object and object wait method to object.
          => Cancel timer using cancel method.
          => Print the timer purge method.
     * Check the number value is equal to 4 .
          => Synchronized TimerSchedule class object using synchronized method.
          => Notify the TimerSchedule object using notify method.
           
Pseudo Code:
class Helper extends TimerTask { 
	public static int i = 0; 
	public void run() { 
		PRINT "Timer ran " + ++i; 
		if(i == 4) 
		{ 
			synchronized(TimerSchedule.obj) 
			{ 
				TimerSchedule.obj.notify(); 
			} 
		} 
	} 
} 
public class TimerSchedule { 
	protected static TimerSchedule obj; 
	public static void main(String[] args) throws InterruptedException { 
		obj = new TimerSchedule();  
		Timer timer = new Timer(); 
		TimerTask task = new Helper(); 
		Date date = new Date(); 
		timer.scheduleAtFixedRate(task, date, 5000); 
		System.out.println("Timer running"); 
		synchronized(obj) 
		{  
			obj.wait(); 
			timer.cancel(); 
			PRINT timer.purge(); 
		} 
	} 
} 

 Code:
 */

import java.util.Date;
import java.util.Timer;
import java.util.TimerTask;

class Helper extends TimerTask {
	public static int i = 0;

	public void run() {
		System.out.println("Timer ran " + ++i);
		if (i == 4) {
			synchronized (TimerSchedule.obj) {
				TimerSchedule.obj.notify();
			}
		}
	}

}

public class TimerSchedule {
	protected static TimerSchedule obj;

	public static void main(String[] args) throws InterruptedException {
		obj = new TimerSchedule();	
		Timer timer = new Timer();// creating a new instance of timer class
		TimerTask task = new Helper();
		Date date = new Date();
		timer.scheduleAtFixedRate(task, date, 5000);
		System.out.println("Timer running");
		synchronized (obj) {
			obj.wait();
			timer.cancel();
			System.out.println(timer.purge());
		}
	}
}
