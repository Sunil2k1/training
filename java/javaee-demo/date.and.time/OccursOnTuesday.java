/*Given a random date, how would you find the date of the previous Friday?

Requirements:
   * A Random date
Entities
   *  OccursOnTuesday
   *  isTuesday implements TemporalQuery
Function Declaration
   * public Boolean queryFrom(TemporalAccessor date)
Jobs to be done 
   * Create class name isTuesday which implements temporalQuery generic in Boolean
   * In isTuesday class
        => Method public Boolean queryFrom(TemporalAccessor date)
             -> return  Calculate the nanoseconds  within the second for the 11 month and also 
                   Calculate the nanoseconds  within the second for 2 day of week.
   * Create a class name  OccursOnTuesday and Declare main method.
   * Give a date in string and store it in date type String
   * Get the local date and convert it in to type date and store it in todayDate
   * By using query for todayDate to Adjusts the specified temporal object to have the same date as this object in isTuesday() and Print the return value

PsudoCode:

class isTuesday IMPLEMENTS TemporalQuery<Boolean> {
	public Boolean queryFrom(TemporalAccessor date) {
	    RETURN (date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2);
	}
}
public class OccursOnTuesday {
	
	public static void main(String[] args ) {
		GIVE DATE IN STRING AND STORE IT IN date IN TYPE STRING;
		LocalDate todayDate = LocalDate.parse(date);
		PRINT todayDate.query(new isTuesday());
	}

}
 */
import java.time.LocalDate;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalQuery;

class isTuesday implements TemporalQuery<Boolean> {
	public Boolean queryFrom(TemporalAccessor date) {
		return ((date.get(ChronoField.DAY_OF_MONTH) == 11) && (date.get(ChronoField.DAY_OF_WEEK) == 2));
	}
}
public class OccursOnTuesday {
	
	public static void main(String[] args ) {
		String date = "2020-10-01";
		LocalDate todayDate = LocalDate.parse(date);
		System.out.println(todayDate.query(new isTuesday()));
	}

}
