/* Write a Java program to calculate your age 

Requirement :
    * Java program to calculate your age 
Entity:
    * CalculateAge
Method Declaration:
    -none-

Jobs to be done :
    * Create a Class name CalculateAge an Declare Main
    * Invoke the LocalDate class and pass my date of birth in of LocalDate class method.
    * Invoke the LocalDate class and get current time using now method.
    * Inoke Period class pass dateOfBirth and now parameters in between method
    * Get the my age using Period object with getYears, getMonths and getDays method
      
Pseudo Code:
 
public class CalculateAge {  
   public static void main(String[] args)
    {
        LocalDate dateOfBirth = LocalDate.of(2000, 9, 16);
        LocalDate now = LocalDate.now();
        Period difference = Period.between(dateOfBirth, now);
        PRINT "My age %d , %d months and %d days old.\n\n",difference.getYears(), difference.getMonths(), difference.getDays();
   }
}

Code:
 */


import java.time.LocalDate;
import java.time.Period;

public class CalculateAge {  
   public static void main(String[] args)
    {       
        LocalDate dateOfBirth = LocalDate.of(2000, 9, 16);  // date of birth
        LocalDate now = LocalDate.now();  // current date       
        Period difference = Period.between(dateOfBirth, now);  // difference between current date and date of birth
         System.out.printf("My age %d , %d months and %d days old.\n\n", 
                    difference.getYears(), difference.getMonths(), difference.getDays());
   }
}
