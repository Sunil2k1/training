/* Write a Java program to convert ZonedDateTime to Date.

Requirement :
    * Program to convert ZonedDateTime to Date.

Entity:
    * ConvertZoneToDate

Method Declaration:
    -none-

Jobs to be done :
     * Create a Class name ConvertZoneToDate and Declare main Method.
     * Invoke ZoneId class systemDefault method and store default zone time in defaultTimeZone.
     * Invoke LocalDateTime class now method and store local date time in localDateTime variable.
     * Convert the zone time to date using LocalDateTime class atZone method store it in zonedDateTime.
          => Print the zonedDateTime.
     * Get the current date and time using Date class from method store it in date.
          => Print the date.
     
Pseudo Code:
public class ConvertZoneToDate {
	public static void main(String[] args) {
		ZoneId defaultTimeZone = ZoneId.systemDefault();
		LocalDateTime localDateTime = LocalDateTime.now();
		ZonedDateTime zonedDateTime = localDateTime.atZone(defaultTimeZone);
		PRINT "ZonedDateTime : " + zonedDateTime;
		Instant instant = zonedDateTime.toInstant();
		Date date = Date.from(instant);
		PRINT "Date : " + date;
	}
}
Code:
*/

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Date;

public class ConvertZoneToDate {
	public static void main(String[] args) {		
		ZoneId defaultTimeZone = ZoneId.systemDefault(); // Get default time zone		
		LocalDateTime localDateTime = LocalDateTime.now();// Get current date and time.
		ZonedDateTime zonedDateTime = localDateTime.atZone(defaultTimeZone);
		System.out.println("ZonedDateTime : " + zonedDateTime);		
		Instant instant = zonedDateTime.toInstant(); // ZonedDateTime to Instant		
		Date date = Date.from(instant); // Convert Instant to Date.
		System.out.println("Date : " + date);

	}

}