/*Program to get the dates 10 days before and after today. 

Requirement :
    * Program to get the dates 10 days before and after today. 
Entity:
    * TenDaysAfterBefore

Method declaration:
    -none-
    
Jobs To be Done:
    * Create a Class name DefaultCalender and Declare main method. 
    * Invoke Calendar class getInstance method and store it in calender.
    * Print year, month, date, hour, minute using Calendar class get method.

Pseudo code:
public class DefaultCalender {
 public static void main(String[] args)
    {
        Calendar calendar = Calendar.getInstance();
        PRINT calendar.get(Calendar.YEAR);
        PRINT calendar.get(Calendar.MONTH);
        PRINT calendar.get(Calendar.DATE);
        PRINT calendar.get(Calendar.HOUR);
        PRINT calendar.get(Calendar.MINUTE);
    }
}

Code:
*/

import java.util.Calendar;
public class DefaultCalender {
 public static void main(String[] args)
    {
        Calendar calendar = Calendar.getInstance(); // Create a default calenda
        System.out.println("Year: " + calendar.get(Calendar.YEAR)); // Get and display information of current date from the calendar
        System.out.println("Month: " + calendar.get(Calendar.MONTH));
        System.out.println("Day: " + calendar.get(Calendar.DATE));
        System.out.println("Hour: " + calendar.get(Calendar.HOUR));
        System.out.println("Minute: " + calendar.get(Calendar.MINUTE));
	    System.out.println();
    }
}

