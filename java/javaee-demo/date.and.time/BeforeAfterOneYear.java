/*Write a Java program to get a date before and after 1 year compares to the current date. 


Requirement :
    * Program to get a date before and after 1 year compares to the current date.
Entity:
    * BeforeAfterOneYear
Method Declaration:
    -none-
Jobs to be done :
     * Create a Class name BeforeAfterOneYear and Declare main method.
     * Invoke Calendar class getInstance method and store in calendar.
     * Invoke Calendar class getTime method and store date in date variable.
     * Using add method add one year to Calender
          => Get the add one year using Calender class getTime method and store it in nextYear.
     * Using add method subract one year to Calender
          => Get the subract one year using Calender class getTime method and store it in  previousYear.
     * Print the current date, nextYear and previousYear.
     
Pseudo Code:
public class BeforeAfterOneYear {
   public static void main(String[] args)
    {
      Calendar calendar = Calendar.getInstance();
      Date date = calendar.getTime();
      calendar.add(Calendar.YEAR, 1); 
      Date nextYear = calendar.getTime();
      calendar.add(Calendar.YEAR, -2); 
      Date previousYear = calendar.getTime();
      PRINT "\nCurrent Date : " + date;
      PRINT "\nDate before 1 year : " + previousYear;
      PRINT "\nDate after 1 year  : " + nextYear+"\n";  	
    }
}

Code:*/
import java.util.Calendar;
import java.util.Date;
public class BeforeAfterOneYear {
   public static void main(String[] args)
    {
      Calendar calendar = Calendar.getInstance();
      Date date = calendar.getTime();     
      calendar.add(Calendar.YEAR, 1); // get next year
      Date nextYear = calendar.getTime();     
      calendar.add(Calendar.YEAR, -2); //get previous year
      Date previousYear = calendar.getTime();
      System.out.println("\nCurrent Date : " + date);
      System.out.println("\nDate before 1 year : " + previousYear);
      System.out.println("\nDate after 1 year  : " + nextYear+"\n");  	
    }
}
