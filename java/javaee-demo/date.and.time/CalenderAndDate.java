/* How do you convert a Calendar to Date and vice-versa with example? 

Requirement :
    * Convert a Calendar to Date 

Entity:
    * CalculateAge

Method Declaration:
    -none-

Jobs to be done :
    * Create a Class name CalculateAge and declare main method
    * Invoke the LocalDate class and pass my date of birth in of LocalDate class method.
    * Invoke the LocalDate class and get current time using now method.
    * Invoke Period class pass dateOfBirth and now parameters in between method
    * Get the my age using Period object with getYears, getMonths and getDays method
      
Pseudo Code:
 
public class CalculateAge {  
   public static void main(String[] args)
    {
        LocalDate dateOfBirth = LocalDate.of(2000, 9, 16);
        LocalDate now = LocalDate.now();
        Period difference = Period.between(dateOfBirth, now);
        PRINT "My age %d , %d months and %d days old.\n\n", 
                    difference.getYears(), difference.getMonths(), difference.getDays();
   }
}

Code:
 */

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class CalenderAndDate {	
	private Calendar dateToCalendar(Date date) {  // Convert Date to Calendar

		Calendar calendar = Calendar.getInstance();
		calendar.setTime(date);
		return calendar;
	}
	private Date calendarToDate(Calendar calendar) {  //Convert Calendar to Date
		return calendar.getTime();
	}

	public static void main(String[] argv) throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-M-yyyy hh:mm:ss");  //Create a Date from String
		String dateInString = "22-01-2015 10:20:56";
		Date date = sdf.parse(dateInString);
		CalenderAndDate convertor = new CalenderAndDate();
		Calendar calendar = convertor.dateToCalendar(date); //Test - Convert Date to Calendar
		System.out.println(calendar.getTime());
		Date newDate = convertor.calendarToDate(calendar);  // Test - Convert Calendar to Date
		System.out.println(newDate);

	}
}
