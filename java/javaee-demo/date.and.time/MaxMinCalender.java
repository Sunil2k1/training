/*  8. Write a Java program to get the maximum and minimum valueof the year, month, week, date 
from the current date of a default calendar.

Requirement :
    * Program to get the maximum and minimum valueof the year, month, week, date 
from the current date of a default calendar.
Entity:
    * MaxMinCalender
Method declaration:
    * public void maximumDate()
    * pubic void minimumDate()
Jobs To be Done:
    * Create a Class name MaxMinCalender. 
    * Invoke MaxMinCalender class maximumDate and minimumDate method.
    * Invoke Calender class getInstance method and store calender in it.
    * Print all maximum values in maximumDate method.
    * Print all minimum values in minimumDate method.

Pseudo code:
public class MaxMinCalender {	
	public void maximumDate() {
		Calendar calendar = Calendar.getInstance();
		System.out.println("\tCurrent Date and Time:" + calendar.getTime());
		int actualMaxYear = calendar.getActualMaximum(Calendar.YEAR);
		int actualMaxMonth = calendar.getActualMaximum(Calendar.MONTH);
		int actualMaxWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
		int actualMaxDate = calendar.getActualMaximum(Calendar.DATE);
		PRINT "\tActual Maximum Year: " + actualMaxYear);
		PRINT "\tActual Maximum Month: " + actualMaxMonth);
		PRINT "\tActual Maximum Week of Year: " + actualMaxWeek);
	    PRINT "\tActual Maximum Date: " + actualMaxDate + "\n");
	}
	public void minimumDate() {
		Calendar calendar = Calendar.getInstance();
		System.out.println("\tCurrent Date and Time:" + calendar.getTime());		
		int actualMaxYear = calendar.getActualMinimum(Calendar.YEAR);
		int actualMaxMonth = calendar.getActualMinimum(Calendar.MONTH);
		int actualMaxWeek = calendar.getActualMinimum(Calendar.WEEK_OF_YEAR);
		int actualMaxDate = calendar.getActualMinimum(Calendar.DATE);
		PRINT "\tActual Minimum Year: "+actualMaxYear;
		PRINT "\tActual Minimum Month: "+actualMaxMonth;
		PRINT "\tActual Minimum Week of Year: "+actualMaxWeek;
		PRINT "\tActual Minimum Date: "+actualMaxDate+"\n";
	}
	public static void main(String[] args) {
		MaxMinCalender maxMinCalender = new MaxMinCalender();
		System.out.println("Maximum Value:-");
		maxMinCalender.maximumDate();
		PRINT "Minimum Value:-";
		maxMinCalender.minimumDate();
	}
}

Code:
*/


import java.util.Calendar;

public class MaxMinCalender {
	
	public void maximumDate() {
		Calendar calendar = Calendar.getInstance();
		System.out.println("\tCurrent Date and Time:" + calendar.getTime());
		int actualMaxYear = calendar.getActualMaximum(Calendar.YEAR);
		int actualMaxMonth = calendar.getActualMaximum(Calendar.MONTH);
		int actualMaxWeek = calendar.getActualMaximum(Calendar.WEEK_OF_YEAR);
		int actualMaxDate = calendar.getActualMaximum(Calendar.DATE);
		System.out.println("\tActual Maximum Year: " + actualMaxYear);
		System.out.println("\tActual Maximum Month: " + actualMaxMonth);
		System.out.println("\tActual Maximum Week of Year: " + actualMaxWeek);
		System.out.println("\tActual Maximum Date: " + actualMaxDate + "\n");
	}
	public void minimumDate() {
		Calendar calendar = Calendar.getInstance();
		System.out.println("\tCurrent Date and Time:" + calendar.getTime());		
		int actualMaxYear = calendar.getActualMinimum(Calendar.YEAR);
		int actualMaxMonth = calendar.getActualMinimum(Calendar.MONTH);
		int actualMaxWeek = calendar.getActualMinimum(Calendar.WEEK_OF_YEAR);
		int actualMaxDate = calendar.getActualMinimum(Calendar.DATE);		
		System.out.println("\tActual Minimum Year: "+actualMaxYear);
		System.out.println("\tActual Minimum Month: "+actualMaxMonth);
		System.out.println("\tActual Minimum Week of Year: "+actualMaxWeek);
		System.out.println("\tActual Minimum Date: "+actualMaxDate+"\n");

	}
	
	public static void main(String[] args) {
		MaxMinCalender maxMinCalender = new MaxMinCalender();
		System.out.println("Maximum Value:-");
		maxMinCalender.maximumDate();
		System.out.println("Minimum Value:-");
		maxMinCalender.minimumDate();
	}
}
