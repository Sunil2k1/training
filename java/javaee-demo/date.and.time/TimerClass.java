/*  Find the time difference using timer class ?

Requirement :
    * Find the time difference using timer class

Entity:
    * TimerClass
    * RemindTask

Method Declaration:
    * public void run()
    * public TimerClass(int seconds)

Jobs to be done :
     * Create new TimerClass constructor with passing argument value to schedule display.
           => Print the "Task scheduled".
     * Invoke TimerClass class constructor 
           =>Store new Timer in timer.
           => Schedule to execute RemindTask class
     * RemindTask class extends the inbuild TimerTask class
           => Method runs the schedule time comes.
           
Pseudo Code:
public class TimerClass {
    Timer timer;
    public TimerClass(int seconds) {
        timer = new Timer();
        timer.schedule(new RemindTask(), seconds*1000);
	}
    class RemindTask extends TimerTask {
        public void run() {
            PRINT "Time's up!";
            timer.cancel(); 
        }
    }
    public static void main(String args[]) {
        new TimerClass(5);
        PRINT "Task scheduled.";
    }
}

Code:*/

import java.util.Timer;
import java.util.TimerTask;
public class TimerClass {
    Timer timer;
    public TimerClass(int seconds) {
        timer = new Timer();
        timer.schedule(new RemindTask(), seconds*1000);
	}
    class RemindTask extends TimerTask {
        public void run() {
            System.out.println("Time's up!");
            timer.cancel(); //Terminate the timer thread
        }
    }
    public static void main(String args[]) {
        new TimerClass(5);
        System.out.println("Task scheduled.");
    }
}