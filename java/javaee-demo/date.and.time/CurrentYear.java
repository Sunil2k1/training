/*Write a Java program to get the information of current/given year

Requirement :
    * Java program to get the information of current/given year   
Entity:
    * CurrentYear
Method declaration:
    -none-
Jobs To be Done:
    * Create a Class name CurrentYear and Declare main method.
    * Invoke Year class get the current year using now method and print it.
    * Check the year leap year or not using isLeap method
    * Print length of the year using length method and store it in length integer.
    * Print the length of the year.
      
Pseudo code:
public class CurrentYear {
	public static void main(String[] args) {
	 Year year = Year.now();
     PRINT "Current Year: " + year;  
     if(year.isLeap()) {
    	 PRINT "Current is leap year";
     } else {
    	 PRINT "Current is not leap year";
     }
     int length = year.length();
     PRINT "Length of the year: " + length+" days\n"; 
	}
}

Code:
 */


import java.time.Year;

public class CurrentYear {
	public static void main(String[] args) {
	 Year year = Year.now();
     System.out.println("Current Year: " + year);  
     if(year.isLeap()) {
    	 System.out.println("Current is leap year");
     } else {
    	 System.out.println("Current is not leap year");
     }
     int length = year.length(); // 365
     System.out.println("Length of the year: " + length+" days"); 
	}
}
