/*Write an example that, for a given month of the current year, lists all of the Saturdays in that month.

Requirements:
   * A Month to list all of Saturdays
Entities
   *  ListAllSaturdays
Function Declaration
   -none-
Jobs to be done 
   * Create a class name ListAllSaturdays and Declare main method.
   * Create a object for Scanner name scanner.
   * Get month type String value as input.
   * convert month value into UpperCase .
   * Find the next Saturday from the given month and store it in Date
   * Get the month from date and store it in newMonth
   * Check While new month is equal to mon
        => print date
        =>find the next saturday and store it in new month.
        => Get the month from date and store it in newMonth
 
Psudocode:

public class ListAllSaturdays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        PRINT "Enter the month ";
        String month = scanner.next();
        Month mon = Month.valueOf(month.toUpperCase());
        PRINT "For the month of "+ mon+":";
        LocalDate date = Year.now().atMonth(mon).atDay(1)
                         .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        Month newMonth = date.getMonth();
        WHILE newMonth == mon {
            PRINT "\n"+date;
            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            newMonth = date.getMonth();
        }
        scanner.close();
    }
}        
*/
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;

public class ListAllSaturdays {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter the month ");
        String month = scanner.next();
        Month mon = Month.valueOf(month.toUpperCase());
        System.out.println("For the month of "+ mon+":");
        LocalDate date = Year.now().atMonth(mon).atDay(1)
                         .with(TemporalAdjusters.firstInMonth(DayOfWeek.SATURDAY));
        Month newMonth = date.getMonth();
        while (newMonth == mon) {
            System.out.print("\n"+date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.SATURDAY));
            newMonth = date.getMonth();
        }
        scanner.close();
    }
}
