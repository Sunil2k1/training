import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Scanner;

/*Write an example that, for a given year, reports the length of each month within that particular year

Requirements:
   * A month
Entities
   *  LengthofMonth
Function Declaration
   * public static int yearLength(Year year)
   * public static int monthLength(Month month)
Jobs to be done 
   * Create a class name LengthofMonth
   * In method yearLength(Year year)
         => Return the length of year 
   * In Method  monthLength(Month month) {
		return the length of  month    
   * Declare the main 	 
   * Create a object for Scanner name scanner.
   * Get month type integer value as input and store it month
   * Print the length of month and year.

Psudocode:

public class LengthofMonth {
	public static int yearLength(Year year) {
		return year.length();
	}
	
	public static int monthLength(Month month) {
		return month.maxLength();
	}
	
	public static void main(String[] args ) {
		
		Year yearNow = Year.of(2020);
		Scanner scanner = new Scanner(System.in);
		PRINT "Enter the Month..";
		int monthIn = scanner.nextInt();
		Month month = Month.of(monthIn);
		
		PRINT "Number of Days in 2020 is : "+LengthofMonth.yearLength(yearNow);
		PRINT "Number of Days in month "+ monthIn +" is : "+LengthofMonth.monthLength(month);
	}
}
 */
public class LengthofMonth {
	public static int yearLength(Year year) {
		return year.length();
	}
	
	public static int monthLength(Month month) {
		return month.maxLength();
	}
	
	public static void main(String[] args ) {
		
		Year yearNow = Year.of(2020);
		Scanner scanner = new Scanner(System.in);
		System.out.println("Enter the Month..");
		int monthIn = scanner.nextInt();
		Month month = Month.of(monthIn);
		
		System.out.println("Number of Days in 2020 is : "+LengthofMonth.yearLength(yearNow));
		System.out.println("Number of Days in month "+ monthIn +" is : "+LengthofMonth.monthLength(month));
	}
}
