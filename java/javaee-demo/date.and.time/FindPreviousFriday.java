/* Given a random date, how would you find the date of the previous Friday?
Requirements:
   * A Random date
Entities
   *  FindPreviousFriday
Function Declaration
   -none-
Jobs to be done 
   * Create a class name FindPreviousFriday  and Declare main method.
   * Create a object for Scanner name scanner.
   * Get date,month,year type integer value as input and store it in date,month,year.
   * Set the local date in dateNow
   * Find the date of the previous Friday and store it in dateFound
   * print dateFound

Psudocode:

public class FindPreviousFriday {
	public static void main(String[] args ) {
		
		Scanner scanner = new Scanner(System.in);		
		PRINT "Enter the Date";		
		int date = scanner.nextInt();
		PRINT "Enter the month";	
		int month = scanner.nextInt();
		System.out.println("Enter the Year");	
		int year = scanner.nextInt();		
		LocalDate dateNow = LocalDate.of(year, month, date);		
		LocalDate dateFound = dateNow.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY));		
		PRINT "The Previous Friday from the date provided is :" + dateFound;
	}
	}

*/

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.Scanner;


public class FindPreviousFriday {
	public static void main(String[] args ) {
		
		Scanner scanner = new Scanner(System.in);		
		System.out.println("Enter the Date");		
		int date = scanner.nextInt();
		System.out.println("Enter the month");	
		int month = scanner.nextInt();
		System.out.println("Enter the Year");	
		int year = scanner.nextInt();		
		LocalDate dateNow = LocalDate.of(year, month, date);		
		LocalDate dateFound = dateNow.with(TemporalAdjusters.previous(DayOfWeek.FRIDAY));		
		System.out.println("The Previous Friday from the date provided is :" + dateFound);
	}
	}
