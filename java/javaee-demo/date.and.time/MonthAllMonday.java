/* Write a Java program for a given month of the current year, lists all of the Mondays in that month.

Requirement :
    * Program for a given month of the current year, lists all of the Mondays in that month.

Entity:
    * MonthAllMonday

Method Declaration:
    -none-

Jobs to be done :
     * Create a Class name MonthAllMonday and Declare Main method.
     * Invoke Month class valueOf method pass parameters as month with toUpperCase method to convert to uppercase
          => Get first monday using with TemporalAdjusters class firstInMonth methof to get first monday.
          => Print the month.
     * Get the current year, month and date using now, atMonth and atDay method.
          => Store current date in LocalDate class date.
     * Get the given month using getMonth method store in monthMonday.
     * Check monthMonday is equal to given month
          => Print date of monday in month
          => Change date next monday in month using with TemporalAdjusters class next method.
          => Get the date using getMonth method and store in date.
     
Pseudo Code:
public class MonthAllMonday {
    public static void main(String[] args) {
        Month month = Month.valueOf("March".toUpperCase());
        System.out.printf("For the month of %s:%n", month);
        LocalDate date = Year.now().atMonth(month).atDay(1).
              with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        Month monthMonday = date.getMonth();
        while (monthMonday == month) {
            PRINT "%s%n", date;
            date = date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            monthMonday = date.getMonth();
        }
    }
}

Code:*/
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.Month;
import java.time.Year;
import java.time.temporal.TemporalAdjusters;

public class MonthAllMonday {
    public static void main(String[] args) {
        Month month = Month.valueOf("March".toUpperCase());
        System.out.printf("For the month of %s:%n", month);
        LocalDate date = Year.now().atMonth(month).atDay(1).
              with(TemporalAdjusters.firstInMonth(DayOfWeek.MONDAY));
        Month monthMonday = date.getMonth();
        while (monthMonday == month) {
            System.out.println(date);
            date = date.with(TemporalAdjusters.next(DayOfWeek.MONDAY));
            monthMonday = date.getMonth();
        }
    }
}