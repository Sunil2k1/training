/* Object to Input Streams and Vice Versa
Requirement:
  * Object to Input Streams  

Entity:
  * ObjectToInputStream 

Method Signature:
  -none-

Jobs to be done:
  * Create a Class name ObjectToInputStream and declare main method
  * Create a Object for Scanner name sc
  * Get input for name,stuentid,address,phonenumber and store it in  name,stuentid,address,phonenumber.
  * Use try
      => Set the path to store ser file and store it in fileout
      => Create a object for OutputStream name out for fileOut
      => Write s in out
  * Catch IOException
      => call method printStackTrace()

Psudocode:

public class ObjectToInputStream {
	   public static void main(String [] args) {
		      Scanner sc = new Scanner(System.in);
		      Student s = new Student();
		      PRINT "Name: ";
		      s.name = sc.next();
		      PRINT "Student Id: ";
		      s.studentId = sc.next();
		      PRINT "Address: ";
		      s.address = sc.next();
		      PRINT "Phone Number: ";
		      s.phonenumber = sc.nextLong();
		      TRY {
		         FileOutputStream fileOut = new FileOutputStream("C:\\Users\\Sunil kumar\\eclipse-workspace\\javaee-demo\\serialization.streamtoken\\student.ser");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(s);
		         out.close();
		         fileOut.close();
		         PRINT Object to Input Stream is Converted;
		      } CATCH IOException i {
		         i.printStackTrace();
		      }
		   }
}
 */
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Scanner;
public class ObjectToInputStream {
	   public static void main(String [] args) {
		      Scanner sc = new Scanner(System.in);
		      Student s = new Student();
		      System.out.println("Name: ");
		      s.name = sc.next();
		      System.out.println("Student Id: ");
		      s.studentId = sc.next();
		      System.out.println("Address: ");
		      s.address = sc.next();
		      System.out.println("Phone Number: ");
		      s.phonenumber = sc.nextLong();
		      try {
		         FileOutputStream fileOut = new FileOutputStream("C:\\Users\\Sunil kumar\\eclipse-workspace\\javaee-demo\\serialization.streamtoken\\student.ser");
		         ObjectOutputStream out = new ObjectOutputStream(fileOut);
		         out.writeObject(s);
		         out.close();
		         fileOut.close();
		         System.out.println("Object to Input Stream is Converted");
		      } catch (IOException i) {
		         i.printStackTrace();
		      }
		   }
}
