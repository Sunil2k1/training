/*Java program to write and read a file using output streams.

Requirement :
    * Program to write and read a file using output streams.

Entity:
    * FileOutputStreams

Method Declaration:
    -none-

Jobs to be done :
     * Create a Class name FileOutputStreams and declare main method.
     * Try block Pass the file path in File class argument.
         => Pass the file object in FileInputStream and FileOutputStream class.
         => Check the readFile is not equal to -1 and store a character using read method.
         => Write file readed content to another file.
     * Catch FileNotFoundException and IOException errors.
     
Pseudo Code:
public class FileOutputStreams {
    public static void main(String[] args) {
        TRY {
            File inputFile = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/Content");
            File outputFile = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/AnotherContent");
            FileInputStream readFile = new FileInputStream(inputFile);
            FileOutputStream writeFile = new FileOutputStream(outputFile);
            int character;
            WHILE (character = readFile.read()) != -1 {
               writeFile.write(character);
            }
            PRINT "Successfully Copied";
            readFile.close();
            writeFile.close();
        } CATCH FileNotFoundException {
            PRINT "FileStreamsTest: " + e;
        } CATCH IOException  {
            PRINT "FileStreamsTest: " + e;
        }
    }
}
Code:
*/


import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

public class FileOutputStreams {
    public static void main(String[] args) {
        try {
            File inputFile = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/Content");
            File outputFile = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/AnotherContent");

            FileInputStream readFile = new FileInputStream(inputFile);
            FileOutputStream writeFile = new FileOutputStream(outputFile);
            String text = "This Content writtern by using Writer";
            int character;

            while ((character = readFile.read()) != -1) {
               writeFile.write(character);
            }
            System.out.println("Successfully Copied");
            readFile.close();
            writeFile.close();
        } catch (FileNotFoundException e) {
            System.err.println("FileStreamsTest: " + e);
        } catch (IOException e) {
            System.err.println("FileStreamsTest: " + e);
        }
    }
}