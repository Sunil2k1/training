/* Write some String content using OutputStream
Requirement:
    * Write some String content using OutputStream

Entity:
   * WriteStringUsingOutputStream
   
Method Signature:
    -none-

Jobs to be done:
    * Create a Class name WriteOutputStream and Declare main.
    * Get the file using file path and store it in source String.
       => Check file exist using FileOutputStream class.
    * Write the file using OutputStream.
    * Invoke the write of byte array to write content to file and close an outStream .
    * OutputStream decodes the array and store as String content.

Pseudo Code:
public class WriteStringUsingOutputStream {
    public static void main(String[] args) {
        OutputStream outStream = new FileOutputStream(source);
        String text = "This Content writtern by OutputStream";
        byte[] byteArray = data.getBytes();
        outStream.write(byteArray);
        outStream.close();
        PRINT "File writed successfully";
    }

}

Code:
*/

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;

public class WriteOutputStream {
    public static void main(String[] args) {        
        String source = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/user";// file path
        try {
            OutputStream outStream = new FileOutputStream(source);
            String text = "Using OutputStream Cintent is Inserted";          
            byte[] byteArray = text.getBytes();// encodes the string to byte array           
            outStream.write(byteArray);// decodes and store as string
            outStream.close();
            System.out.println("File writed successfully");
        } catch (FileNotFoundException fileNotFound) {
            fileNotFound.getMessage();
        } catch (IOException ioe) {
            ioe.getMessage();
        }
    }

}