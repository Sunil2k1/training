/* Reading a file using InputStream


Requirement:
   * Read a file using InputStream

Entity:
  * InputStreamDemo

Method Signature:
  -none-

Jobs to be done:
     * Create a Class name ReadInputStream and Declare main.
     * Get the file in InputStream using file path to read data byte by byte. 
         => Store it in source String.
         => Check file exist using FileInputStream class.
     * Read method to read data byte by byte.
         => Check the file data less than zero, It assumes there is no more data in that file.
         => Data present convert the integer to character.

Pseudo Code:

public class InputStreamDemo {
    public static void main(String[] args) throws IOException {
        InputStream input = new FileInputStream(source); 
        int character;
        WHILE(character = input.read()) != -1) {
            PRINT (char) data;
        }
    }
}

Code:
*/

import java.io.FileInputStream;
import java.io.IOException;

public class ReadInputStream {

    public static void main(String[] args) throws IOException {
        String source = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/user";
        try {           
            @SuppressWarnings("resource")
			FileInputStream file = new FileInputStream(source);// get the file through input stream
            int character;          
            while ((character = file.read()) != -1) {   // read to get a single byte
                System.out.println((char) character);
            }
        } catch (IOException ioe) {
            System.out.println(ioe.getMessage());
        }
    }

}