/* program to write primitive datatypes to file using by dataoutputstream.
 
Requirement :
    - Program to primitive datatypes to file using by dataoutputstream.

Entity:
    - BufferReadWrite

Method Declaration:
    - public static void main(String[] args)

Jobs to be done :
     * Create a Class name PrimitiveDataOutput and Declare main method.
     * Store int, String, float values
     * Pass file path in FileOutputStream class argument
     * Using DataOutputStream class write method of with type adding to the dat file
     * Print the added content in separate key.
     
Pseudo Code:

public class PrimitiveDataOutput {

  public static void main(String[] args) throws Exception{
    int idA = 1;
    String nameA = "Bugati";
    int topspeedA = 400;
    float zerotohundredA = 3.0f;
    int idB = 2;
    String nameB = "Meclaren";
    int topspeedB = 380;
    float zerotohundredB = 4.2f;
    FileOutputStream fos = new FileOutputStream("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/car.dat");
    DataOutputStream dos = new DataOutputStream(fos);
    WRITE THE DETAIS IN THE car.dat FILE;
    dos.flush();
    dos.close();
    FileInputStream fis = new FileInputStream("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/car.dat");
    DataInputStream dis = new DataInputStream(fis);
    int carId = dis.readInt();
    PRINT "Car Id: " + carId;
    String carName = dis.readUTF();
    System.out.println("Car Name: " + carName);
    int topspeed = dis.readInt();
    System.out.println("Car Top Speed: " + topspeed);
    float zerotohundred = dis.readFloat();
    PRINT "Car time to take 0 to 100 speed:  " + zerotohundred;
Code:
*/

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;

public class PrimitiveDataOutput {
  public static void main(String[] args) throws Exception{
    int idA = 1;
    String nameA = "Bugati";
    int topspeedA = 400;
    float zerotohundredA = 3.0f;
    int idB = 2;
    String nameB = "Meclaren";
    int topspeedB = 380;
    float zerotohundredB = 4.2f;
    FileOutputStream fos = new FileOutputStream("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/car.dat");
    DataOutputStream dos = new DataOutputStream(fos);
    dos.writeInt(idA);
    dos.writeUTF(nameA);
    dos.writeInt(topspeedA);
    dos.writeFloat(zerotohundredA);
    dos.writeInt(idB);
    dos.writeUTF(nameB);
    dos.writeInt(topspeedB);
    dos.writeFloat(zerotohundredB);
    dos.flush();
    dos.close();
    FileInputStream fis = new FileInputStream("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/car.dat");
    DataInputStream dis = new DataInputStream(fis);
    int carId = dis.readInt();
    System.out.println("Car Id: " + carId);
    String carName = dis.readUTF();
    System.out.println("Car Name: " + carName);
    int topspeed = dis.readInt();
    System.out.println("Car Top Speed: " + topspeed);
    float zerotohundred = dis.readFloat();
    System.out.println("Car time to take 0 to 100 speed:  " + zerotohundred);
  }
}
