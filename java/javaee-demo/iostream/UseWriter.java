/*Write some String content using Writer

Requirement:
   * Write some String content using Writer

Entity:
   * WriteStringUsingWriter

Method Signature:
   -none-

Jobs to be done:
   * Create a Class name  WriteStringUsingWriterand Declare main.
   * Get the file using file path and store file in source String.
       => Check file exist using Writer class.
   * Store line in text String  
   * Write the text String in file using FileWriter 
   * Invoke the writer method to write content to file and close the writer.

Pseudo Code:
public class WriteStringUsingWriter {

    public static void main(String[] args) {
        Writer writer = new FileWriter(source);
        String text = "This Content writtern by using Writer";
        writer.write(text);
        writer.close();
        PRINT "File writed successfully";
    }

}

Code:
*/

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;

public class UseWriter {

    public static void main(String[] args) {       
        String source = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/user";  // source as file path
        try {            
            Writer writer = new FileWriter(source);  // FileWriter for writing data to file
            String text = "Edited by Editor";
            writer.write(text);
            writer.close();
            System.out.println("File writed successfully");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}