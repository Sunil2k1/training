/* Write a Java program reads data from a particular file using FileReader 
and writes it to another, using FileWriter.

Requirement :
    - Program reads data from a particular file using FileReader and writes it to another, 
using FileWriter.

Entity:
    - FileReaderWriter

Method Declaration:
    - public static void main(String[] args)
    
Jobs to be done :
     * Create a Class name FileReaderWriter and Declare main method.
     * Pass the file path in FileReader class as argument.
     * Pass the another file path to write a file in FileWriter class as argument.
     * Read file using check while reader read method is not equal to -1.
         => Store the each character in charRead and write in a file using write method.
     
     
Pseudo Code:
public class FileReaderWriter {
	public static void main(String[] args) throws IOException {
		String readerPath = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/Content";
		FileReader reader = new FileReader(readerPath);
		String writerPath = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/AnotherContent";
		FileWriter writer = new FileWriter(writerPath);
		
		int charRead = -1;
		WHILE (charRead = reader.read()) != -1 {
			writer.write((char)charRead);
		}
		PRINT "File Successfully Copied";
		writer.close();
	}
}

code:
*/

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileReaderWriter {
	public static void main(String[] args) throws IOException {
		String readerPath = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/Content";
		@SuppressWarnings("resource")
		FileReader reader = new FileReader(readerPath);
		String writerPath = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/AnotherContent";
		FileWriter writer = new FileWriter(writerPath);		
		int charRead = -1;
		while ((charRead = reader.read()) != -1) {
			writer.write((char)charRead);
		}
		System.err.println("File Copied  Successfully ");
		writer.close();
	}
}
