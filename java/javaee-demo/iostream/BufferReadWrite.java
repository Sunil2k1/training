/* Java program to write and read a file using buffer input.
Requirement :
    * Program to write and read a file using buffer input.

Entity:
    * PrimitiveDataOutput

Method Declaration:
    -none-

Jobs to be done :
     * Create a Class name BufferReadWrite and Declare main method.
     * Store file path in writerPath String.
          => Create FileWriter with file path in argument of BufferedWriter class.
          => Write some content in file and close the bufferWriter using close method.
     * Pass file path in File argument in BufferedReader.
     * Read the file till the file null using while loop. 
          => Store reader String to the file null.
     
PseudoCode:
public class BufferReadWrite {
	public static void main(String[] args) throws Exception {
		String writerPath = "C:\Users\Sunil kumar\eclipse-workspace\javaee-demo\iostream\BufferFileDemo.txt";
		BufferedWriter bufferWriter = new BufferedWriter(new FileWriter(writerPath));
		bufferWriter.write("Welcome to BufferWriter file");
		bufferWriter.close();
		PRINT "Successfully Done";
		File file = new File("C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/BufferFile.txt");
		BufferedReader bufferReader = new BufferedReader(new FileReader(file));
		String reader;
		WHILE (reader = bufferReader.readLine()) != null
			PRINT reader;
	}
}

Code:
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class BufferReadWrite {
	public static void main(String[] args) throws Exception {
		String writerPath = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/BufferFileDemo.txt";
		BufferedWriter bufferWriter = new BufferedWriter(new FileWriter(writerPath));
		bufferWriter.write("This is BufferWriter file");
		bufferWriter.close();
		System.out.println("Successfully Writtern");

		File file = new File("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/BufferFileDemo.txt");

		@SuppressWarnings("resource")
		BufferedReader bufferReader = new BufferedReader(new FileReader(file));

		String reader;
		while ((reader = bufferReader.readLine()) != null)
			System.out.println(reader);
	}
}
