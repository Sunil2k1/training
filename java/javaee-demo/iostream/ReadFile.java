/* Read a file using java.io.File


Requirement:
   * Read a file using java.io.File
Entity:
   * ReadFile
Method Signature:
   * public static void main(String[] args)

Jobs to be done:
   * Create a Class name ReaderDemo and Declare main.
   * Get the file
       => Store the file in source String
       => Check file is in given path using File class.
   * Print the length of size using length method
   * Print file path using getPath method.

Pseudo Code:
public class ReaderDemo {
   public static void main(String[] args) throws IOException {
        String source = "C:/Users/santh/eclipse-workspace/JavaEE-Demo/io.stream/Content";       
        File file = new File(source);
        PRINT file.length();      
        PRINT file.getPath();
    }
}

Code:
*/


import java.io.File;

public class ReadFile {
    public static void main(String[] args) {
    	String source = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/user";// source as file path
        File file = new File(source); // opening the file
        System.out.println(file.length());  // getting the length of file        
        System.out.println(file.getPath()); // file or file.getPath() returns the path of the file
        System.out.println(file);
    }

}