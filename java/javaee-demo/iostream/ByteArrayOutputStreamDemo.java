/*write a program that allows to write data to multiple files together using bytearray outputstream 

Requirement :
    * Program to write data to multiple files together using bytearray outputstream.

Entity:
    * ByteArrayOutputStreamDemo

Method Declaration:
    -none-

Jobs to be done :
     * Create a class name ByteArrayOutputStreamDemo and declare main method.
     * Store FileOutputStream and ByteArrayOutputStream as null value.
     * Try block pass the file path FileOutputStream class argument to create file.
           => Store the content in string.
           => Get the string as bytes using getBytes method.
           =>Write the bytes in the files.
     * Using writeTo method write the content in th files.
     * Flush the files using flush method
     
Pseudo Code:
public class ByteArrayOutputStreamDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fileOutputStream1 = null;
        FileOutputStream fileOutputStream2 = null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            fileOutputStream1 = new FileOutputStream("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/fileoutput1.txt");
            fileOutputStream2 = new FileOutputStream("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/fileoutput2.txt");
            byteArrayOutputStream = new ByteArrayOutputStream();
            String string = "Sample File Created";
            byte[] byteArray = string.getBytes();
            byteArrayOutputStream.write(byteArray);           
            byteArrayOutputStream.writeTo(fileOutputStream1);
            byteArrayOutputStream.writeTo(fileOutputStream2);
            byteArrayOutputStream.flush();
            System.out.println("Successfully written to two files...");
        }
    }
}

Code:
*/


import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class ByteArrayOutputStreamDemo {
    public static void main(String[] args) throws IOException {
        FileOutputStream fileOutputStream1 = null;
        FileOutputStream fileOutputStream2 = null;
        ByteArrayOutputStream byteArrayOutputStream = null;
        try {
            fileOutputStream1 = new FileOutputStream("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/fileoutput1.txt");
            fileOutputStream2 = new FileOutputStream("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/fileoutput2.txt");
            byteArrayOutputStream = new ByteArrayOutputStream();
            String string = "Sample File Created";
            byte[] byteArray = string.getBytes();
            byteArrayOutputStream.write(byteArray);           
            byteArrayOutputStream.writeTo(fileOutputStream1);
            byteArrayOutputStream.writeTo(fileOutputStream2);
            byteArrayOutputStream.flush();
            System.out.println("Write job is done Successfully");
        }
        finally {
            if (fileOutputStream1 != null)
            {
                fileOutputStream1.close();
            }
            if (fileOutputStream2 != null)
            {
                fileOutputStream2.close();
            }
            if (byteArrayOutputStream != null)
            {
                byteArrayOutputStream.close();
            }
        }
    }
}