/*How to read the file contents  line by line in streams 

Requirement :
    * Program to read the file contents  line by line in streams.

Entity:
    * ReadFileStream

Method Declaration:
    -none-

Jobs to be done :
     * Create a Class name ReadFileStream.
     * Store file path in fileName String.
     * Get the file path using Paths class get method and pass parameter to lines method.
     * Print the file content using for each.
          
     
Pseudo Code:

public class ReadFileStream {
	public static void main(String args[]) {
		String fileName = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/Content";
		TRY (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			stream.forEach(System.out::println);
		} CATCH (IOException e) {
			e.printStackTrace();
		}
	}
}

Code:*/

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class ReadFileStream {
	public static void main(String args[]) {
		String fileName = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/Content";		
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) { //read file into stream, try-with-resource
			stream.forEach(System.out::println);
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

}