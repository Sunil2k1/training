/*write a program for ByteArrayInputStream class to read byte array as input stream.

Requirement :
    * Program for ByteArrayInputStream class to read byte array as input stream.

Entity:
    * ByteArrayAsInput

Method Declaration:
    -none-

Jobs to be done :
     * Create a class name ByteArrayAsInput
     * Store in byteArray an array values of integer values.
     * Pass the byteArray in ByteArrayInputStream class of argument 
     * Read using ByteArrayInputStream class of object bufferRead
     * Check bufferRead read not equal to -1 and store in number.
     
Pseudo Code:

public class ByteArrayAsInput {  
  public static void main(String[] args) throws IOException {  
    byte[] byteArray = { 5, 1, 2, 3 };  
    ByteArrayInputStream bufferRead = new ByteArrayInputStream(byteArray);  
    int numbers;  
    WHILE (number = bufferRead.read()) != -1 {     
      PRINT number;
    }  
  }  
} 

Code:*/


import java.io.ByteArrayInputStream;
import java.io.IOException;  
public class ByteArrayAsInput {  
  public static void main(String[] args) throws IOException {  
    byte[] byteArray = { 5, 1, 2, 3 };    
    ByteArrayInputStream bufferRead = new ByteArrayInputStream(byteArray);   // Create the new byte array input stream  
    int numbers;  
    while ((numbers = bufferRead.read()) != -1) {   //Conversion of a byte into character       
      System.out.println(numbers);
    }  
  }  
} 