/* Read a any text file using BufferedReader and print the content of the file

Requirement:
   * Read a any text file using BufferedReader and print the content of the file
Entity:
   * ReadFileContent
Method Signature:
   -none-
Jobs to be done:
     * Create a Class name ReadFileContent and Declare main.
     * Create a file to write content.
     * Write content to the file created using write method.
     * Close the file after written using close method.
     * Using FileReader to read the content of file.
     * Buffered reader takes the Reader file.
     * Read each line
         => If a line exists on file show the line of data.
         =>If no more line exists, stop reading the file.
     * Close the BufferedReader properly using close method.

Pseudo Code:
public class ReadFileContent {
    public static void main(String[] args) throws IOException {
        FileWriter file = new FileWriter(source);
        file.write("Bugati car is Faster than Meclaren car");
        file.close();
        BufferedReader bufferedReader = new BufferedReader(new FileReader(source));
        String line;
        WHILE (line = bufferedReader.readLine()) != null) {
            PRINT line;
        }
        bufferedReader.close();
    }
}

Code:
*/

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class ReadFileContent {

    public static void main(String[] args) throws IOException {        
        String source = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/user";// Create a file on specified location
        FileWriter file = new FileWriter(source);
        file.write("Bugati car is Faster than Meclaren car");
        file.close();        
        BufferedReader bufferedReader = new BufferedReader(new FileReader(source));  // reader to read the data in file
        String line;        
        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }
        bufferedReader.close();
    }

}