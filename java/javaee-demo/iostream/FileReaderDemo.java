
/*Reading a file using Reader


Requirement:
   * Read a file using Reader

Entity:
   * ReaderDemo

Method Signature:
   -none-

Jobs to be done:
    * Create a Class name ReaderDemo and Declare main.
    * Get the file using file path and store file in source String.
        => Check file exist using File class.
    * Read the file with Reader type for FileReader.
    * Create a character array of file length.
    * Read the file of size of char array store to char array.
    * Display the file content and close the reader.

Pseudo Code:

public class ReaderDemo {
    public static void main(String[] args) throws IOException {
        File file = new File(source);
        Reader reader = new FileReader(file);
        char[] chars = new char[file.length()];
        reader.read(chars);
        Print chars;
    }

}

code:
*/


import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;

public class FileReaderDemo {

    public static void main(String[] args) throws IOException {
        String source = "C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/iostream/user";// source file location
        File file = new File(source);// getting the file
        Reader reader = new FileReader(file); // Reader to read the data present in file
        char[] character = new char[(int) file.length()];// creating a char array of size of file
        reader.read(character);
        System.out.println(new String(character));
        reader.close();
    }
}