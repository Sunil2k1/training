/* Write a program which implement multithreading using sleep(), setPriority(), getPriorty(), Name(), getId() methods.


Requirement:
   * Write a program which implement multithreading using sleep(), setPriority(), getPriorty(), Name(), getId() methods.

Entity:
   * ThreadDemo

3.Function declaration
   * public void run()

4.Jobs to be done: 
   * Create class name ThreadDemo implements Thread
        => In run() Method
           -> For each number 
               -> print the current Thread using getNama
               -> Try
                    -> Print Current Thread  using getId
                    -> Thread Sleeps upto 1000ms Using sleep() Method  
               -> Catch the Exception
                     -> Print the Exception
   * Declare the Main Method
   * Create Object for Thread name thread
   * Start the thread using start() Method.
   * Set the Priority 6 using setPriority() method              
   * Print the Current Thread and priority.
 
Psudeocode:

public class ThreadDemo extends Thread  {
   public void run() {
      FOR int i = 10; i < 13; i++ {
         PRINT Thread.currentThread().getName();
         TRY {
             PRINT Thread.currentThread().getId();  
            Thread.sleep(1000);
         } CATCH Exception  {
            PRINT EXCEPTION;
         }
      }
   }
   public static void main(String[] args) throws Exception {
      Thread thread = new Thread(new ThreadDemo());
      thread.start();
  	  Thread.currentThread().setPriority(6); 
	  PRINT thread.currentThread().getPriority() 
   }
}
  
Code:
*/
public class ThreadDemo extends Thread  {
   public void run() {
      for (int i = 10; i < 13; i++) {
                                                  //Using getName() method
         System.out.println(Thread.currentThread().getName() + "  " + i);
         try {
             System.out.println ("Thread " + 
                   Thread.currentThread().getId() + " is running");  //getId() method
            Thread.sleep(1000);
         } catch (Exception e) {
            System.out.println(e);
         }
      }
   }

   public static void main(String[] args) throws Exception {
      Thread thread = new Thread(new ThreadDemo());
       thread.start();
  	   Thread.currentThread().setPriority(6); //The processor is assigned to the thread by the thread scheduler based on its priority.
	   System.out.println("main thread priority : " + Thread.currentThread().getPriority()); 
   } 
} 
 
