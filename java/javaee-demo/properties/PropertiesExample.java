/*Write a program to perform the following operations in Properties.
    i) add some elements to the properties file.
    ii) print all the elements in the properties file using iterator.
    iii) print all the elements in the properties file using list method.

Requirement:
  - Program to perform the following operations in Properties.
    i) add some elements to the properties file.
    ii) print all the elements in the properties file using iterator.
    iii) print all the elements in the properties file using list method.

Entity:
  - PropertiesExample 

Method Signature:
  -none-

Jobs to be done:
    * Create a class name PropertiesExample and Declare main.
    * Create an object properties for Properties class.
    * Set some properties with key and values to properties object.
    * Display all property using Iterator object with keySet.
    * Using Stream to display all property in entrySet.

Pseudo Code:
public class PropertiesExample {
    public static void main(String[] args) {
        Properties properties = new Properties();
        properties.setProperty("username", "Santheesh");
        properties.setProperty("email", "santheesh62@gmail.com");
        properties.setProperty("password", "16092000santh");
        Iterator<?> propertiesIterator = properties.keySet().iterator();
        WHILE propertiesIterator.hasNext() {
            String key = (String) propertiesIterator.next();
            PRINT key + " : " + properties.get(key);
        }
        properties.entrySet().stream().forEach(System.out::println);
    }
}

Code:
*/


import java.util.Iterator;
import java.util.Properties;

public class PropertiesExample {

    public static void main(String[] args) {

        // Creating an object of Properties class
        Properties properties = new Properties();
        properties.setProperty("username", "Sunil");
        properties.setProperty("email", "sunilkpr2k1@gmail.com");
        properties.setProperty("password", "Sunil@21");       
        Iterator<?> propertiesIterator = properties.keySet().iterator();   // Using Iterator to display key and values on properties
        while (propertiesIterator.hasNext()) { 
            String key = (String) propertiesIterator.next();
            System.out.println(key + " : " + properties.get(key));
        }
        properties.entrySet().stream().forEach(System.out::println);
    }

}
