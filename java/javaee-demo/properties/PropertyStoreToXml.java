/*Write a program to add 5 elements to a xml file and print the elements
in the xml file using list. Remove the 3rd element from the xml file
and print the xml file.


Requirement:
   * Program to add 5 elements to a xml file and print the elements
in the xml file using list. Remove the 3rd element from the xml file
and print the xml file.

Entity:
   * PropertyStoreToXml

Method Signature:
   * public static List<String> toList(Set<Object> set

Jobs to be done:
    * Create a class name PropertyStoreToXml
    * Create a object properties of Properties class.
         => Set some properties to properties object.
    * Create a new XML file.
         => Storing properties to XML file.
    * Create a new object newProperties of Properties class.
         => Loading data from XML file.
         => Remove a specified property from properties.
         => Display all the properties in newProperties object.

Pseudo Code:

public class PropertyStoreToXml {
    public static List toList(Set set) {
        List list = new ArrayList();
        FOR Object element : set {
            list.add(element);
        } RETURN list;
    }
    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        properties.setProperty(key, value);
        properties.setProperty(key, value);
        properties.setProperty(key, value);
        FileOutputStream newFile = new FileOutputStream("C:\Users\Sunil kumar\eclipse-workspace\javaee-demo\properties\\Info.xml");
        properties.storeToXML(newFile, "Information about the project path");
        FileInputStream existingFile = new FileInputStream("C:\Users\Sunil kumar\eclipse-workspace\javaee-demo\properties\\Info.xml");
        Properties newProperties = new Properties();
        newProperties.loadFromXML(existingFile);
        List<String> keysList = toList(newProperties.keySet());
        keysList.remove(key);
    }

}

Code:
*/

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;
import java.util.Set;

public class PropertyStoreToXml {

    // toList method to convert Set elements to list
    public static List<String> toList(Set<Object> set) {
        List<String> list = new ArrayList<>();
        for (Object element : set) {
            list.add((String) element);
        } return list;
    }

    public static void main(String[] args) throws IOException {
        Properties properties = new Properties();
        properties.setProperty("path", "JavaEE-Demo\\properties");
        properties.setProperty("project", "JavaEE-Demo");
        properties.setProperty("password", "1234");
        properties.setProperty("resource", "https://java jenkov.com");
        properties.setProperty("username", "Jenkov");       
        FileOutputStream newFile = new FileOutputStream("C:\\Users\\Sunil kumar\\eclipse-workspace\\javaee-demo\\properties\\Info.xml"); //Create a XML file        
        properties.storeToXML(newFile, "Information about the project path");//Store properties to xml file
        //Import a XML file
        FileInputStream existingFile = new FileInputStream("C:\\Users\\Sunil kumar\\eclipse-workspace\\javaee-demo\\properties\\Info.xml");
        //Creating a newProperties object of Property class
        Properties newProperties = new Properties();       
        newProperties.loadFromXML(existingFile);        //Loading properties from XML file to newProperties object
        List<String> keysList = toList(newProperties.keySet()); //Converting set to list
        for (String key : keysList) {
            System.out.println(key + " -> " + newProperties.getProperty(key));
        }       
        keysList.remove("resource");//Removing a key from keysList
        System.out.println();
        for (String key : keysList) {
            System.out.println(key + " -> " + newProperties.getProperty(key));
        }
    }

}