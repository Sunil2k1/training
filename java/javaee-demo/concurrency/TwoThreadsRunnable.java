/* Write a program of performing two tasks by two threads that implements Runnable interface.

Requirements:
   - Program of performing two tasks by two threads that implements Runnable interface.
Entities
   * ThreadRunOne
   * ThreadRunTwo
   * TwoThreadsRunnable
Function Declaration
   * public void run() 
Jobs to be done:
   * Create class name ThreadRunOne implements Runnable
      => In method run()
           -> For each number 
               -> Try
                    -> Print "First Thread"
                    -> Thread Sleeps upto 1000ms Using sleep() Method  
               -> Catch the InterruptedException
                     -> Call the printStackTrace() method 
   * Create class name ThreadRunTwo implements Runnable
      => In method run()
           -> For each number 
               -> Try
                    -> Print "SecondThread"
                    -> Thread Sleeps upto 500ms Using sleep() Method
               -> Catch the InterruptedException
                    -> Call the printStackTrace() method
   * Declare the Main Method
   * Create Object for ThreadRunOne name thread1
   * Create Object for ThreadRunOne name thread2 
   * Invoke the method Thread() by passing parameter thread1 and storing it in t1
   * Invoke the method Thread() by passing parameter thread2 and storing it in t2
   * Start the thread t1 using start() Method.
   * Start the thread t2 using start() Method.                         
                     
Psudeocode:
class ThreadRunOne implements Runnable {
	public void run() {
		FOR int i = 0;i <= 5; i++ {
			TRY {
				PRINT "First Thread";
				Thread.sleep(1000);
			} CATCH InterruptedException  {
				printStackTrace();
			}
		}
	}
}

class ThreadRunTwo implements Runnable {	
	public void run() {
	  FOR int i = 0;i <= 5; i++ {
		TRY {
			PRINT "Second Thread";
			Thread.sleep(500);
		}CATCH  InterruptedException {
			printStackTrace();
		}
	}
  }
}

public class TwoThreadsRunnable {
	public static void main(String[] args) {	
		ThreadRunOne thread1 = new ThreadRunOne();
		ThreadRunTwo thread2 = new ThreadRunTwo();
		Thread t1 = new Thread(thread1);
		Thread t2 = new Thread(thread2);
		t1.start();
		t2.start();
	}
}  

Code:*/
class ThreadRunOne implements Runnable {
	
	public void run() {
		for (int i = 0;i <= 5; i++) {
			
			try {
				System.out.println("First Thread");
				Thread.sleep(1000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}

class ThreadRunTwo implements Runnable {
	
	public void run() {
		for (int i = 0;i <= 5; i++) {
		try {
			System.out.println("Second Thread");
			Thread.sleep(500);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
  }
}

public class TwoThreadsRunnable {
	public static void main(String[] args) {
		
		ThreadRunOne thread1 = new ThreadRunOne();
		ThreadRunTwo thread2 = new ThreadRunTwo();
		
		Thread t1 = new Thread(thread1);
		Thread t2 = new Thread(thread2);
		t1.start();
		t2.start();
	}
}
