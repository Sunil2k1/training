/* Write a generic method to count the number of elements in a collection that have a specific
property (for example, odd integers, prime numbers, palindromes).

Requirements : 
 	*	Write a generic method to count the number of elements in a collection that have a specific
 property (for example, odd integers, prime numbers, palindromes).
 
Entities :
 	* public class CountProperties.
Function Declaration :
 	-none-
Jobs To Be Done:
   *  Create a class as CountProperties
   *  In countnum (ArrayList<Integer> list) method
        => Declare and initialize c is equal to Zero
        => For each elements in list
             -> Check if
                 *> Modulo of element by 2 is not equal to zero
                 *> Increment c
        => Return c
  *  Declare  the main method
  *  Create a Arraylist name list
  *  Add 5 Elements to list
  *  Call countnum by passing parameter list and print result.
     
Psudocode:

public class CountProperties {	
	public static int countnum(ArrayList<Integer> list) {
		INTEGER C = 0;
		FOR INTEGER elements : list) {
			IF elements % 2 != 0 {
				c++;
			}
		}
		RETURN c;
	}
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();  // Create a arraylist using Generic
		ADD 5 ELEMENTS TO list
		PRINT countnum(list);

	}

}
Code:
 */

import java.util.ArrayList;

public class CountProperties {	
	public static int countnum(ArrayList<Integer> list) {
		int c = 0;
		for(int elements : list) {
			if(elements % 2 != 0) {
				c++;
			}
		}
		return c;
	}
	public static void main(String[] args) {
		ArrayList<Integer> list = new ArrayList<>();
		list.add(1);
		list.add(23);
		list.add(12);
		list.add(18);
		list.add(20);
		System.out.println("Number of odd Numbers are : " + countnum(list));

	}

}
