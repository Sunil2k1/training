/* * Write a program to demonstrate generics - class objects as type literals.
   * Write a program to demonstrate generics - for loop, for list, set and map. 


Requirement:
    * Write a program to demonstrate generics - class objects as type literals,for loop, for list, set and map.

Entity:
    * public class car

Function Declaration:
    * public void fuel()
    * public static <T> boolean checkInterface(Class<?> theClass)
    
Jobs to be Done:
   * In Interface  Vehical
       => Put public void fuel() method.
   *  Create a class Car Implements Vehical.    
   *  In method checkInterface is in static using Generic in method  it can call any method in the class 
       => Return the result of isIterface() method.
   *  In  void fuel() method 
       => print "Petrol"   
   *  Declare main method
   *  Create a class name intClass using Generic as Integer .
   *   invoke checkInterface by passing intClass and store it in one as boolean
   *   print the result for getting from getClass() method.
   *   print the result for getting from getName() method. 
   *   invoke checkInterface by passing Car.class and store it in two as boolean      
   *   print the result for getting from getClass() method.
   *   print the result for getting from getName() method. 
   *   invoke checkInterface by passing Vehical.class and store it in three as boolean      
   *   print the result for getting from getClass() method.
   *   print the result for getting from getName() method. 
   *   try
         => invoke the method forName() and passing the parameter "Car"    and storing the class er
         => print er invoking getClass() method.
   *  Catch ClassNotFoundException
         => print  the result by calling toString() Method.        


Psudocode:

INTERFACE Vehical {
	public void fuel();
}

public class Car IMPLEMENTS Vehical {

    public static <T> boolean checkInterface(Class<?> theClass) {
        RETURN theClass.isInterface();
    }

    public void fuel() {
        PRINT "Perol";
    }

    public static void main(String[] args) {
        Class<Integer> intClass = int.class;            
        boolean one = checkInterface(intClass);
        PRINT onE;                   
        PRINT intClass.getClass();        
        PRINT intClass.getName();         
        boolean two = checkInterface(Car.class);
        PRINT two;                   
        PRINT Car.class.getClass();       
        PRINT Car.class.getName();        
        boolean three = checkInterface(Vehical.class);
        PRINT three;                   
        PRINT Vehical.class.getClass();    
        PRINT Vehical.class.getName();     
        TRY {
            Class<?> er = Class.forName("Car");
            PRINT er.getClass();
            PRINT er.getName());
        } CATCH ClassNotFoundException {
            PRINT e.toString();
        }
    }
}

Code:
*/

interface Vehical {
	public void fuel();
}

public class Car implements Vehical {

    public static <T> boolean checkInterface(Class<?> theClass) {
        return theClass.isInterface();
    }

    public void fuel() {
        System.out.println("Perol");
    }

    public static void main(String[] args) {
        Class<Integer> intClass = int.class;            
        boolean one = checkInterface(intClass);
        System.out.println(one);                   
        System.out.println(intClass.getClass());        
        System.out.println(intClass.getName());         
        boolean two = checkInterface(Car.class);
        System.out.println(two);                   
        System.out.println(Car.class.getClass());       
        System.out.println(Car.class.getName());        
        boolean three = checkInterface(Vehical.class);
        System.out.println(three);                   
        System.out.println(Vehical.class.getClass());    
        System.out.println(Vehical.class.getName());     
        try {
            Class<?> er = Class.forName("Car");
            System.out.println(er.getClass());
            System.out.println(er.getName());
        } catch (ClassNotFoundException e) {
            System.out.println(e.toString());
        }
    }
}
