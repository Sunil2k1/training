/* Why except iterator() method in iterable interface, are not neccessary to define in the implemented class?
  
Ans:
   The Java Iterable interface has three methods of which only one needs to be implemented and
the other two have default implementations.
 */