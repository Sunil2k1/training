/*Will the following class compile? If not, why?
   	public final class Algorithm {
           public static <T> T max(T x, T y) {
               return x > y ? x : y;
           }
       }

Requirements : 
    * Will the following class compile? If not, why?
Entities :
  	* Algorithm.
Function Declaration :
    * public static <T> T max(T x, T y)
Jobs To Be Done:
    * Finding the code compiles or not.
Ans:
The code does not compile,
      because the greater than (>) operator applies only to primitive numeric types.
 


public class Algorithm {

	public static <T> T max(T x, T y) {
        return (x > y) ? x : y;
    }

}*/
