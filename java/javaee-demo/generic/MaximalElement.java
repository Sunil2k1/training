/*  Write a generic method to find the maximal element in the range [begin, end) of a list.
 
Requirement:
    * To find the maximum element of the given program using generic method.

Entity:
    * MaximalElement

Function Declaration:
    * public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int start, int end)
    
Jobs to be Done:
   * Create a class as MaximalElement.
   * In max() Method
        => Get the starting element from list and stored in maximumElement as T
        => For each element
            -> Check if maximum element is compare to start
                  -> maximumElement is equal to start element in list
        => return maximumElement
   * Declare main method
   * create a list in Generic type Integer name list and insert the values in list
   * call the method max() by passing parameters in it and print it                
            

Psudocode:

public class MaximalElement {    
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int start, int end) {
        T maximumElement = list.get(start);
        FOR ++start; start < end; ++start {
            IF maximumElement.compareTo(list.get(start)) < 0 {
                maximumElement = list.get(start);
            }
        }   
        RETURN maximumElement;
    }
    
    public static void main(String[] args) {
        CREATE A LIST IN GENERIC TYPE IN INTEGER AND INSERT THE VALUES AS LIST.
        PRINT max(list, 20,50);
    }
    
    Code:
*/

import java.util.Arrays;
import java.util.List;

public class MaximalElement {
    
    public static <T extends Object & Comparable<? super T>> T max(List<? extends T> list, int start, int end) {
        T maximumElement = list.get(start);
        for (++start; start < end; ++start) {
            if (maximumElement.compareTo(list.get(start)) < 0) {
                maximumElement = list.get(start);
            }
        }   
        return maximumElement;
    }
    
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(12,24,62,45,25,11,9,21);
        System.out.println(max(list, 20,50));
    }

}
