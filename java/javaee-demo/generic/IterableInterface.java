/* Write a program to print employees name list by implementing iterable interface.

Requirements:
  		To write a program to print employees name list by implementing iterable interface.
Entity:
  	 * MyIterable
  	 *  IterableInterface
  
Function Declaration:
  	 *  public MyIterable(T[] t),
  	 *	public Iterator<T> iterator().
  
Jobs To Be Done:
  * Create a class as MyIterable in <T> implments Iterable in <T> 
       => Create a Private  List in <T> type name list.
       => In  MyIterable(T[] t) Method
             -> list is equal to Convert the t Arrays to List
       => In iterator() Method
             -> return list.iterator() method
  * Create a class as IterableInterface and declare main. 
  * Create a Array and insert the values    
  * Create a object for Iterator in String as Generic Type  name emp and invoking the employee to iterator
  * For each employee
       => Print each employee            

Psudocode:

class MyIterable<T> IMPLEMENTS Iterable<T> {
	PRIVATE List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		RETURN list.iterator();
	}
}
public class IterableInterface {

	public static void main(String[] args) {
		CREATE A ARRAY IN STRING DATATYPE AND INSERT THE VALUES;
		MyIterable<String> emp = new MyIterable<>(employee);
		FOR String string : emp {
			PRINT string;
		}
	}
}
 */

import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

class MyIterable<T> implements Iterable<T> {
	private List<T> list;
	public MyIterable(T[] t) {
		list = Arrays.asList(t);
	}
	public Iterator<T> iterator() {
		return list.iterator();
	}
}
public class IterableInterface {

	public static void main(String[] args) {
		String[] employee = {"Raj", "Kavin", "Kumar", "Sunil", "Gokul"};
		MyIterable<String> emp = new MyIterable<>(employee);
		for (String string : emp) {
			System.out.println(string);
		}
	}
}