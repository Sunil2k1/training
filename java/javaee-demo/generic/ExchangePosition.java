/*Write a generic method to exchange the positions of two different elements in an array.

Requirement:
    * Write a generic Method to swap the elements.

Entity:
    * public class SwapDemo

Function Declaration:
    * public static <S> void swap(S[] list, int firstNumber, int lastNumber)
    * public static <S> void printSwap(List<S> list1, int firstNumber, int secondNumber)
Jobs to be done:
    * Create a class as ExchangePosition
    * Inside swap(S[] list, int firstNumber, int lastNumber) Method 
        => First number in the list is stored in temperaryVariable in S type
        => The first number in the list is equal to The last number in the list
        => The last number in the list is equal to The temperaryVariable in the list
        => Convert the array to list and print 
   * Inside printSwap(List<S> list1, int firstNumber, int secondNumber) Method
        => Using Collections call swap method by passing parameter in it
   * Declare Main Method
   * Create a Array name list in Integer and insert the values
   * Swap the numbers by calling Swap method
   * Create a List implementing Arraylist using Generic in Integer DataType
   * Call the method printswap.        

Psudocode:

public class ExchangePosition {   
    public static <S> void swap(S[] list, int firstNumber, int lastNumber) {
        S temperaryVariable = list[firstNumber];
        list[firstNumber] = list[lastNumber];
        list[lastNumber] = temperaryVariable;
        PRINT Arrays.toString(list) ;
    }
    public static <S> void printSwap(List<S> list1, int firstNumber, int secondNumber) {
        Collections.<S> swap(list1, firstNumber, secondNumber);
    }
    
    public static void main(String[] args) {
        CREATE A ARRAY IN INTEGER DATATYPE AND INSERT THE VALUES;
        SWAP THE NUMBERS BY CALLING SWAP METHOD swap(list, 1, 3);        
        List<Integer> list1 = new ArrayList<>(Arrays.asList(list)); // Create a List
        CALL THE METHOD printSwap(list1, 1, 3);
        
    }
}
Code:*/

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class ExchangePosition {
    
    public static <S> void swap(S[] list, int firstNumber, int lastNumber) {
        S temperaryVariable = list[firstNumber];
        list[firstNumber] = list[lastNumber];
        list[lastNumber] = temperaryVariable;
        System.out.println("The swapped element is " + Arrays.toString(list) );
    }
    public static <S> void printSwap(List<S> list1, int firstNumber, int secondNumber) {
        Collections.<S> swap(list1, firstNumber, secondNumber);
    }
    
    public static void main(String[] args) {
        Integer[] list = {1,2,3,4,5,6,7};
        swap(list, 1, 3);
        
        List<Integer> list1 = new ArrayList<>(Arrays.asList(list));
        printSwap(list1, 1, 3);
        
    }
}

