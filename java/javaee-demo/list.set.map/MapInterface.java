/*demonstrate java program to working of map interface using( put(), remove(), booleanContainsValue(), replace() ) .

Requirements:
   * the 4 values insert in the set
Entities
   * MapInterface
Function Declaration
   -none-
Jobs to be done
 * Create a Class name MapInterface and declaring the main.
 * Create a treemap name car with Integer and String Instance.
 * Put 4 elements in car
 * Print car
 * Remove a element from car Using remove() method
 * Replace a element from car Using replace() method
 * Check the value is present in car //Using containsValue() method
 * Print car 

 Psudocode:
 
 public class MapInterface {
	public static void main(String[] args) {				
	      TreeMap<Integer, String> car = new TreeMap<Integer, String>();  //Creating TreeMap and SortedMap
	      PUT 4 ELEMENTS IN car;
		  PRINT car;
	      REMOVE A ELEMENT USING remove() METHOD;
	      REPLACE A ELEMENT USING replace() METHOD;
	      CHECK THE VALUE IS PRESENT IN car USING containsValue() METHOD
		  PRINT car;
	}
}
 Code:*/
import java.util.TreeMap;
public class MapInterface {
	public static void main(String[] args) {				
	      TreeMap<Integer, String> car = new TreeMap<Integer, String>();  //Creating TreeMap and SortedMap
	      car.put(1, "Bugati");   //Using put() method
	      car.put(2, "Lambogini");
	      car.put(5, "Meclaren");
	      car.put(9, "BMW");
		  System.out.println(car);
	      car.remove(9, "BMW");  //Using remove() method
	      car.replace(1,"Lexus");  //Using replace() method
	      System.out.println(car.containsValue("Lexus")); //Using containsValue() method
		  System.out.println(car);
	}
}
