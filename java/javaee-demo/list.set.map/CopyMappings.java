/* Write a Java program to copy all of the mappings from the specified map to another map? 

Requirements
   * For HashMap, in this code we need integer and a String.
Entities
   * CopyMappings
Function Declaration
   -none-
Jobs to be done
   * Create a class CopyMappings and declaring the main.
   * Create a HashMap name sportscar with Integer and String instance.
   * Add 4 values to sportscar
   * Again Create a HashMap name racecar with Integer and String instance.
   * Copy all of the mappings from the specified map to another map
   * print racecar

Psudocode:

public class CopyMappings {
   public static void main(String args[]) {
       HashMap<Integer, String> sportscar = new HashMap<Integer, String>();  //Creating hashMap 
       ADD 4 VALUES TO sportscar;
       HashMap<Integer, String> racecar = new HashMap<Integer, String>();   
       COPY ALL OF THE MAPPING FROM THE SPECIFIED MAP TO ANOTHER MAP;
       PRINT racecar;
   }
}
Code:
*/


import java.util.HashMap;

public class CopyMappings {

   public static void main(String args[]) {
	   
	      //Creating hashMap 
	      HashMap<Integer, String> sportscar = new HashMap<Integer, String>();  //Creating hashMap 
	      sportscar.put(1, "Bugati");
	      sportscar.put(2, "Lambogini");
	      sportscar.put(5, "Meclaren");
	      sportscar.put(10, "BMW");       
          HashMap<Integer, String> racecar = new HashMap<Integer, String>();   
          racecar.putAll(sportscar);  //Copy all of the mappings from the specified map to another map
          System.out.println(racecar);
   }
}