/*demonstrate linked hash set to array() method in java

Requirements:
   * the 6 values insert in the set
Entities
   * LinkedHashSetToArray
Function Declaration
   -none-
Jobs to be done
   * Create a class LinkedHashSetToArray and declaring the main.
   * Create a set name cars by Implementing LinkedHashset using String Instance
   * add 6 values to cars
   * print cars
   * create a array name car
   * for each cars
      => print each element in cars
      
Psudocode:

 public class LinkedHashSetToArray {
	public static void main(String [] args) {
	    Set<String> cars = new LinkedHashSet<>(); // create a set
	    ADD 6 VALUES TO cars;
	    PRINT cars;
	    Object[] car = cars.toArray();   //create a array
	    for (int i = 0; i < car.length; i++){
	         PRINT car[i];
	    }
	}
}
 Code:*/
import java.util.LinkedHashSet;
import java.util.Set;
public class LinkedHashSetToArray {
	public static void main(String [] args) {
        Set<String> cars = new LinkedHashSet<>();
	    cars.add("Mazda");     //Adding 6 Values
	    cars.add("Lexus");
	    cars.add("Audi");
	    cars.add("BMW");   
	    cars.add("Mazda");
	    cars.add("Mecleran"); 
	    System.out.println("The LinkedHashSet: \n"+cars);
	    System.out.println("The Array:");
	    
	  /*form an array of the same elements as that of the LinkedHashSet 
	      using LinkedHashSet.toArray() method */
	    
        Object[] car = cars.toArray();  
        for (int i = 0; i < car.length; i++) { 
            System.out.println(car[i]); 
        }
		
	}

}
