/* java program to demonstrate adding elements, displaying, removing, and iterating in hash set

Requirements:
   * the 5 values insert in the set
Entities
   * HashSetDemo
Function Declaration
   -none-
Jobs to be done
   * Create a class HashSetDemo  and declaring the main.
   * Create a set name cars by Implementing Hashset using String Instance
   * add 5 values to cars
   * remove a value from cars
   * Create a Object for Iterator name car
   * check while 
      => car.hasNext()
      => print car.hasNext()
      
Psudocode:

 public class HashSetDemo {
	public static void main(String [] args) {
	    Set<String> cars = new HashSet<>(); // create a set
	    ADD 5 VALUES TO cars;
	    REMOVE A VALUE FROM cars;
	    Iterator<String> car = cars.iterator(); //create a iterator
	    WHILE car.hasNext() {
	         PRINT car.hasNext();
	    }
	}
}
 Code:*/
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class HashSetDemo {
	public static void main(String [] args) {
		Set<String> cars = new HashSet<>();
        cars.add("BMW"); 	//Add 5 values in the set
	    cars.add("Mazda");
	    cars.add("Lexus");
	    cars.add("Audi");
	    cars.add("Mecleran");
	    cars.remove("Audi");  //Remove a value in set
        Iterator<String> car = cars.iterator();  //Displaying all the set elements using Iterator interface
        System.out.println("Displaying all the set elements using Iterator :");
        while (car.hasNext()) {
            System.out.println(car.next());
        }  	    
	}
	
}
