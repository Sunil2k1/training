/*demonstrate program explaining basic add and traversal operation of linked hash set

Requirements:
   * the 6 values insert in the set
Entities
   * LinkedHashSetDemo
Function Declaration
   -none-
Jobs to be done
   * Create a class LinkedHashSetDemo and declaring the main.
   * Create a set name cars by Implementing LinkedHashset using String Instance
   * add 6 values to cars
   * Create a Object for Iterator name setIterator
   * check while 
      => setIterator.hasNext()
      => print setIterator.hasNext()
      
Psudocode:

 public class LinkedHashSetDemo {
	public static void main(String [] args) {
	    Set<String> cars = new LinkedHashSet<>(); // create a set
	    ADD 6 VALUES TO cars;
	    Iterator<String> setIterator = cars.iterator(); //create a iterator
	    WHILE setIterator.hasNext() {
	         PRINT setIterator.hasNext();
	    }
	}
}
 Code:*/
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.Iterator;
public class LinkedHashSetDemo {
	public static void main(String [] args) {
        Set<String> cars = new LinkedHashSet<>();	//Creating a LinkedHashSet 
	    cars.add("Mazda");     //Adding 6 Values
	    cars.add("Lexus");
	    cars.add("Audi");
	    cars.add("BMW");   
	    cars.add("Mazda");
	    cars.add("Mecleran");  
        //creating iterator to traverse over cars
	    Iterator<String> setIterator = cars.iterator();
        while(setIterator.hasNext()){
            System.out.println(setIterator.next());
          }  		
	}

}