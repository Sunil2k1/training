/*  Create a list
    => Add 10 values in the list
    => Create another list and perform addAll() method with it
    => Find the index of some value with indexOf() and lastIndexOf()
    => Print the values in the list using 
        - For loop
        - For Each
        - Iterator
        - Stream API
    => Convert the list to a set
    => Convert the list to a array
+ Explain about contains(), subList(), retainAll() and give example
 

Requirements:
   * Program to print difference of two numbers using lambda expression and the single method interface
Entities
   * SetDemo
Function Declaration
   -none-
Jobs to be done
    * Create a class ListDemo and declaring the main.
    * Create a list name car by Implementing ArrayList using String Instance
    * Add 10 values in car
    * Print car
    * Create another list name newcar by Implementing ArrayList using String Instance
    * add 3 values in newcar
    * Add all values newcar to car
    * Print car
    * Print the Index value for a element in car using indexOf() method
    * Print the Index value for a element in car using lastIndexOf() method
    * using for loop , for each car
         => print the elements of car
    * using for each, for each car
         => print the elements of car     
    * Create a object for Iterator name carIterator
    * Check while     
        => carIterator.hasNext()
        => print carIterator.hasNext() 
    * Using Stream API's forEach print
    * Create a set name carSet implementing HashSet with String Instance
    * print carSet
    * Convert the list to a array 
    * Using contains() if given element present in the list it returns true or else it returns false 
    * use subList newAddercar and print it
    * print car and newAddercar Using retainall() method  

Psudocode:

public class ListDemo {
	public static void main(String[] args) {
	List<String> car = new ArrayList<String>(); //Create a List
	ADD 10 VALUES IN car;
	PRINT car;
	List<String> newcar = new ArrayList<String>();  //Create another list and perform addAll() method with it
	ADD 3 VALUES IN newcar;
	PRINT newcar;
	PRINT THE INDEX OF THE VALUE USING indexOf() METHOD;
	PRINT THE INDEX OF THE VALUE USING LastIndexOf() METHOD;
    for (int index = 0; index < car.size(); index++) {  	
        PRINT car.get(index);
    }   
    for (String cars : car) {
        PRINT cars;
    } 
    Iterator<String> carIterator = car.iterator(); //create a iterator
    WHILE carIterator.hasNext() {
        PRINT carIterator.next();
    }
    car.forEach(System.out::println);   
    Set<String> carSet = new HashSet<>(car);   //Convert the list to a set
    PRINT carSet;    
    String[] carArray = new String[car.size()]; //Convert the list to a array
    PRINT carArray;
    USING contains() METHOD ,CHECK IF VALUE IS PRESENT OR NOT;
    List<String> newAddedcar = car.subList(10, car.size());
    PRINT newAddedcar;
    PRINT car.retainAll(newAddedcar);
	}
}


*/

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

public class ListDemo {
	public static void main(String[] args) {
	List<String> car = new ArrayList<String>();
	car.add("BMW"); 	//Add 10 values in the list
	car.add("Mazda");
	car.add("Lexus");
	car.add("Audi");
	car.add("Mecleran");
	car.add("RR");
	car.add("Dodge");
	car.add("Bugati");
	car.add("Pagani");
	car.add("Lambogini");
    System.out.println("Car list:\n"+car);	
	List<String> newcar = new ArrayList<String>();  //Create another list and perform addAll() method with it
	newcar.add("Honda");
	newcar.add("Benze");
	newcar.add("Ford");
	car.addAll(newcar);
	System.out.println("Car list:\n"+car);
	
	//Find the index of some value with indexOf() and lastIndexOf()
	//The indexOf return the very first index where the element matched.
	System.out.println("Index for Lexus car is \t" + car.indexOf("Lexus"));
	
	// lastIndexOf returns the last index where the element matched.
	System.out.println("Last index for Pagani\t" + car.lastIndexOf("Pagani"));
	    
	//Print the values in the list using 
	System.out.println("Using For loop");      //Using For loop
    for (int index = 0; index < car.size(); index++) {  	
        System.out.println(car.get(index));
    }


    System.out.println("Foreach");     //Using For each
    for (String cars : car) {
        System.out.println(cars);
    }


    System.out.println("while loop");      //Using While loop
    Iterator<String> carIterator = car.iterator();
    while(carIterator.hasNext()) {
        System.out.println(carIterator.next());
    }

    System.out.println("Stream API");   //Using Stream API's forEach
    car.forEach(System.out::println);

    
    Set<String> carSet = new HashSet<>(car);   //Convert the list to a set
    System.out.println(carSet);

    
    String[] carArray = new String[car.size()]; //Convert the list to a array
    System.out.println(carArray);
    
    //contains() if given element present in the list it returns true or else it returns false 
    System.out.println(car.contains("Bugati"));

    //subList(startIndex, lastIndex)
    List<String> newAddedcar = car.subList(10, car.size());
    System.out.println(newAddedcar);

    //retainAll()
    System.out.println("After using retainAll: " + car.retainAll(newAddedcar) + "\n" + car);
	}
}
