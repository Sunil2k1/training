/*+ Create a set 
    => Add 10 values
    => Perform addAll() and removeAll() to the set.
    => Iterate the set using 
        - Iterator
        - for-each
+ Explain the working of contains(), isEmpty() and give example.

Requirements:
   * the 10 values
Entities
   * SetDemo
Function Declaration
   -none-
Jobs to be done
    * Create a class SetDemo and declaring the main.
    * Create a list name car by Implementing ArrayList using String Instance
    * Add 10 values in cars
    * Print cars
    * Create another list name newCars by Implementing ArrayList using String Instance
    * add 4 values in newCars
    * Add all values newCars to car
    * Print cars
    * remove all in new cars
    * clear newCars;
    * Create a Iterator name car
    * Check While
        => car.hasNext()
        => print car.next()
    * Displaying all the set elements using ForEach
    * check if the value is present or not
    * print the car is empty or not using newCars.isEmpty() method

 psudocode:
 
 public class SetDemo {
	public static void main(String[] args) {				
		Set<String> cars = new HashSet<>(); //create a set
        ADD 10 VALUES IN cars;
	    PRINT cars;
        Set<String> newCars = new HashSet<>(); //create a set
        ADD 4 VALUES IN newCars;
        ADD ALL ELEMENTS FROM newCars TO cars;
        PRINT CARS;
        REMOVE ALL IN new cars;
        CLEAR newCars;
        Iterator<String> car = cars.iterator();  //Displaying all the set elements using Iterator interface
        WHILE car.hasNext() {
            PRINT car.next();
        }      
        cars.forEach(System.out::println);
        CHECK IF THE VALUE IS PRESENT OR NOT;
        PRINT THE car IS EMPTY OR NOT BY USING newCars.isEmpty() METHOD;
	}

}
 code:
*/


import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class SetDemo {

	public static void main(String[] args) {
		
		//Creating a set and adding 10 elements to it.
		Set<String> cars = new HashSet<>();
        cars.add("BMW"); 	//Add 10 values in the list
	    cars.add("Mazda");
	    cars.add("Lexus");
	    cars.add("Audi");
	    cars.add("Mecleran");
	    cars.add("RR");
	    cars.add("Dodge");
	    cars.add("Bugati");
	    cars.add("Pagani");
	    cars.add("Lambogini");       
        System.out.println("Creating a set and adding 10 elements to it:\n" + cars );
        
        //Creating another set with some elements.
        Set<String> newCars = new HashSet<>();
        newCars.add("Tesla");
        newCars.add("Toyota");
        newCars.add("Volvo");
        newCars.add("Volkswagen");
        
        //Perform addAll() method and removeAll() to the set.
        cars.addAll(newCars);
        System.out.println("\taddAll() method ");
        System.out.println(cars);
        System.out.println("\tremoveAll() method");
        cars.removeAll(newCars);
        System.out.println(cars);
        newCars.clear();
             
        Iterator<String> car = cars.iterator();  //Displaying all the set elements using Iterator interface
        System.out.println("\tUsing While loop");
        while (car.hasNext()) {
            System.out.println(car.next());
        }      
        System.out.println("\tUsing ForEach");  //Displaying all the set elements using ForEach
        cars.forEach(System.out::println);

        //contains()  if given element present in the list it returns true or else it returns false 
        System.out.println("\t contains() method");
        System.out.println("Checks car set contains Tata Motors " + cars.contains("Pagani"));

        //isEmpty() if set is empty it returns true or else it returns false 
        System.out.println("\tisEmpty() method");
        System.out.println("Checks newCars is empty or not? " + newCars.isEmpty());
	}

}