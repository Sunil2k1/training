/*  * Write a Java program to test if a map contains a mapping for the specified key?
    * Count the size of mappings in a map?

Requirements
   * For HashMap, in this code we need integer and a String.
Entities
   * SpecifiedKey
Function Declaration
   -none-
Jobs to be done
   * Create a class as SpecifiedKey and declaring the main.
   * Create a hashmap name car with Integer and String Instance.
   * Add 4 values in car
   * check the value is present in car using containsKey() method and print it.
   * Print the size.   

Psudocode:

public class SpecifiedKey {
	 public static void main(String args[]) {
	      HashMap<Integer, String> car = new HashMap<Integer, String>();  //Creating hashMap 
	      ADD 4 VALUES IN car;
	      CHECK THE VALUE IS PRESENT IN car USING containsKey() METHOD AND PRINT IT; 
	      PRINT THE SIZE OF car;	      
	   }
}
Code:
*/


import java.util.HashMap;

public class SpecifiedKey {
	 public static void main(String args[]) {
	      HashMap<Integer, String> car = new HashMap<Integer, String>();  //Creating hashMap 
	      car.put(1, "Bugati");
	      car.put(2, "Lambogini");
	      car.put(5, "Meclaren");
	      car.put(10, "BMW"); 
	      System.out.println("Checking 10 key value map containing in the car or not"); //mapping for the specified key
	      System.out.println(car.containsKey(10));
	      //Count the size of mappings in a map
	      System.out.println("The size of mappings in a map");
	      System.out.println(car.size());
	      
	   }
}
