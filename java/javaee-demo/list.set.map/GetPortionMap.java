/*Write a Java program to get the portion of a map whose keys range from a given key to another key?

Requirements
   * For HashMap, in this code we need integer and a String.
Entities
   * GetPortionMap
Function Declaration
   -none-
Jobs to be done
   * Create a class as GetPortionMap and declaring the main.
   * Create a HashMap name car with Integer and String instance.
   * add 4 values to car.
   * Printing the Sub map using subMap() method.

Psudocode:

public class GetPortionMap {
   public static void main(String args[]) {
       HashMap<Integer, String> car = new HashMap<Integer, String>();  //Creating hashMap 
       ADD 4 VALUES TO car;
       PRINT car.subMap(3, 10);
   }
}
Code:
*/


import java.util.TreeMap; 

public class GetPortionMap {

	public static void main(String[] args) {				
	      TreeMap<Integer, String> car = new TreeMap<Integer, String>();  //Creating TreeMap and SortedMap
	      car.put(1, "Bugati");
	      car.put(2, "Lambogini");
	      car.put(5, "Meclaren");
	      car.put(9, "BMW");              
		  System.out.println("" + car.subMap(3, 10));
	}

}
