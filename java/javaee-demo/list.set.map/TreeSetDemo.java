/*java program to demonstrate insertions and string buffer in tree set

Requirements:
   * the 4 values insert in the set
Entities
   * TreeSetDemo
Function Declaration
   -none-
Jobs to be done
 * Create a class TreeSetDemo implements comparator and declaring the main.
 * in compare (StringBuffer s1, StringBuffer s2) method
     => return s1.toString().compareTo(s2.toString())
 * In main
     => Create a set name car implementing TreeSet with String Instance
     => Create String buffer name cars
     => Add 2 values in car
     => print car
     => Add 2 values in cars
     => Print cars

Psudocode:

public class TreeSetDemo implements Comparator<StringBuffer>{
    public int compare(StringBuffer s1, StringBuffer s2) 
    { 
        RETURN s1.toString().compareTo(s2.toString()); 
    } 
    public static void main(String[] args) 
    {   Set<String> car = new TreeSet<String>();  //creating set as String
        Set<StringBuffer> cars = new TreeSet<>(new TreeSetDemo()); //creating set as StringBuffer
        ADD 2 ELEMENTS TO CAR;
        PRINT CAR;
        ADD 2 ELEMENTS TO CARS;
        PRINT CARS;
    } 

}         
 
 Code:*/
import java.util.Comparator; 
import java.util.TreeSet;
import java.util.Set;
public class TreeSetDemo implements Comparator<StringBuffer>{
    public int compare(StringBuffer s1, StringBuffer s2) 
    { 
        return s1.toString().compareTo(s2.toString()); 
    } 
    public static void main(String[] args) 
    {   Set<String> car = new TreeSet<String>();  //creating set as String
        Set<StringBuffer> cars = new TreeSet<>(new TreeSetDemo()); //creating set as StringBuffer
        car.add("BMW"); 
        car.add("Meclaren"); 
        System.out.println(car); 
        cars.add(new StringBuffer("RR")); 
        cars.add(new StringBuffer("Audi")); 
        System.out.println(cars); 
    } 

}

