
//Java Program illustrating use of commentChar() method 

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StreamTokenizer;

public class StreamTokenMethods {
	
	public void commentCharMethod() throws IOException {
		FileReader reader = new FileReader("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/serialization.streamtoken/readtoken.txt");
		BufferedReader bufferread = new BufferedReader(reader);
		StreamTokenizer token = new StreamTokenizer(bufferread);
		// Use of commentChar() method
		token.commentChar('a');
		int t;
		while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) {
			switch (t) {
			case StreamTokenizer.TT_NUMBER:
				System.out.println("Number : " + token.nval);
				break;
			case StreamTokenizer.TT_WORD:
				System.out.println("Word : " + token.sval);
				break;
			}
		}
	}
	public void eollsSignificantMethod() throws IOException {
		FileReader reader = new FileReader("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/serialization.streamtoken/readtoken.txt"); 
        BufferedReader bufferread = new BufferedReader(reader); 
        StreamTokenizer token = new StreamTokenizer(bufferread); 
  
        boolean arg = true; 
        // Use of eolIsSignificant() method 
        token.eolIsSignificant(arg); 
        // Here the 'arg' is set true, so EOL is treated as a token 
  
        int t; 
        while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) 
        { 
            switch (t) 
            { 
            case StreamTokenizer.TT_EOL: 
                System.out.println("End of Line encountered."); 
                break; 
            case StreamTokenizer.TT_NUMBER: 
                System.out.println("Number : " + token.nval); 
                break; 
            case StreamTokenizer.TT_WORD: 
                System.out.println("Word : " + token.sval); 
                break; 
  
            } 
        } 
        
	}
	public void linenoMethod() throws IOException {
		FileReader reader = new FileReader("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/serialization.streamtoken/readtoken.txt"); 
        BufferedReader bufferread = new BufferedReader(reader); 
        StreamTokenizer token = new StreamTokenizer(bufferread); 
          
        token.eolIsSignificant(true); 
        // Use of lineno() method  
        // to get current line no. 
        System.out.println("Line Number:" + token.lineno()); 
  
        token.commentChar('a'); 
        int t; 
        while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) 
        { 
            switch (t) 
            { 
            case StreamTokenizer.TT_EOL: 
                System.out.println(""); 
                System.out.println("Line No. : " + token.lineno()); 
                break; 
            case StreamTokenizer.TT_NUMBER: 
                System.out.println("Number : " + token.nval); 
                break; 
            case StreamTokenizer.TT_WORD: 
                System.out.println("Word : " + token.sval); 
                break; 
  
            } 
        } 
    } 
	public void lowerCaseModeMethod() throws IOException {
		FileReader reader = new FileReader("C:/Users/Sunil kumar/eclipse-workspace/javaee-demo/serialization.streamtoken/readtoken.txt"); 
        BufferedReader bufferread = new BufferedReader(reader); 
        StreamTokenizer token = new StreamTokenizer(bufferread); 
  
        /* Use of lowerCaseMode() method to 
           Here, the we have set the Lower Case Mode ON 
        */
        boolean arg = true; 
        token.lowerCaseMode(arg); 
  
        int t; 
        while ((t = token.nextToken()) != StreamTokenizer.TT_EOF) 
        { 
            switch (t) 
            { 
            case StreamTokenizer.TT_NUMBER: 
                System.out.println("Number : " + token.nval); 
                break; 
            case StreamTokenizer.TT_WORD: 
                System.out.println("Word : " + token.sval); 
                break; 
  
            } 
        } 

	}
	public static void main(String[] args) throws IOException {
		StreamTokenMethods tokenMethods = new StreamTokenMethods();
		System.out.println("Using commentCharMethod");
		tokenMethods.commentCharMethod();
		System.out.println("Using eollsSignificantMethod");
		tokenMethods.eollsSignificantMethod();
		System.out.println("Using linenoMethod");
		tokenMethods.linenoMethod();
		System.out.println("Using  lowerCaseModeMethod");
		tokenMethods.lowerCaseModeMethod();
	}
}
