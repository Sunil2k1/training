/*Write a code to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.

Requirement:
     * Program to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.
     
Entity:
     * FractionDigits
     
Function Declaration:
  	 -none-
  	 
Jobs To Be Done:
    * Create a Class name FractionDigits and declare main method.
  	* Invoke NumberFormat class getInstance method and store it in numberFormat.
  	* Set Minimun and Maximun fraction digits using
              =>  setMinimumFractionDigits and
              =>  setMaximumFractionDigits method.
    * Invoke format method with float value to print. 
    
PseudoCode:
public class FractionDigits {
	public static void main(String[] args) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMinimumFractionDigits(1);
		numberFormat.setMaximumFractionDigits(2);
		PRINT numberFormat.format(21.2001f);
	}
}
Code:
*/ 

import java.text.NumberFormat;

public class FractionDigits {
	public static void main(String[] args) {
		NumberFormat numberFormat = NumberFormat.getInstance();
		numberFormat.setMinimumFractionDigits(1);
		numberFormat.setMaximumFractionDigits(2);
		System.out.println(numberFormat.format(21.2001f));
	}
}
