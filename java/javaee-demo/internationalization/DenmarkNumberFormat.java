/* Write a code to change the number format to Denmark number format .
 
Requirement:
     * Program to change the number format to Denmark number format.
     
Entity:
     * DenmarkNumberFormat
     
Function Declaration:
  	 -none-
  	 
Jobs To Be Done:
    * Create a Class name DenmarkNumberFormat and Declare main method
  	* Invoke the NumberFormat class getInstance method parameter as create Locale class with argument Denmark number format.
    * Print the Denmark number format.
    
PseudoCode:
public class DenmarkNumberFormat {
	public static void main(String[] args) {
		PRINT NumberFormat.getInstance(new Locale("Denmark"))
				                       .format(21007));
	}
}
Code:
*/ 
 
import java.text.NumberFormat;
import java.util.Locale;

public class DenmarkNumberFormat {
	public static void main(String[] args) {
		System.out.println("Denmark Number Foramt:");
		System.out.println(NumberFormat.getInstance(new Locale("Denmark"))
				                       .format(21007));

	}
}
