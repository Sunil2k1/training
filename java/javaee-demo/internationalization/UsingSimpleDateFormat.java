/* To print the following pattern of the Date and Time using SimpleDateFormat.
    "yyyy-MM-dd HH:mm:ssZ"
 
Requirement:
     * Program to print the following pattern of the Date and Time using SimpleDateFormat.
           "yyyy-MM-dd HH:mm:ssZ"
     
Entity:
     * UsingSimpleDateFormat
     
Function Declaration:
  	 -none-
JobsToBeDone: 
    * Create a Class name and Declare Main method.
  	* Store date pattern in pattern String.
  	* Create SimpleDateFormat class pass argument pattern.
  	* Get pattern format using invoke format method.
  	* Print the format date.      
    
PseudoCode:
public class PatternDate {
	public static void main(String[] args) {
		String pattern = "yyyy-MM-dd HH:mm:ssZ";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date(0));
		Print date;
	}
}
Code:
*/  
import java.sql.Date;
import java.text.SimpleDateFormat;
public class UsingSimpleDateFormat {
	public static void main(String[] args) {
		String pattern = "yyyy-MM-dd HH:mm:ssZ";
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
		String date = simpleDateFormat.format(new Date(0));
		System.out.println(date);
	}
}
