/*To print the following Date Formats in Date using locale.
        DEFAULT, MEDIUM, LONG, SHORT, FULL. 
 
Requirement:
     * Program to rounding the value to maximum of 3 digits by setting maximum and minimum Fraction digits.
     
Entities:
     * DateFormats
     
Function Declaration:
  	 -none-
  	 
Jobs To Be Done:
  	* Create Date class to get current and print.
  	* Create Locale class to get a time in specified laguage and country.
  	* Using DateFormat class getTimeInstance method pass parameters 
  	      => DEFAULT with locale object.
  	      => MEDIUM with locale object.
  	      => LONG with locale object.
  	      => SHORT with locale object.
  	      => FULL with locale object.
    * Print the all Date format. 	      
    
PseudoCode:
public class DateForamt{
   public static void main(String args[]){	   
       Date currentDate = new Date();  
       System.out.println("Current date is: "+currentDate);      
       Locale locale = new Locale("fr", "FR");
       String timeDefault = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale).format(currentDate);  
       PRINT "Formatting the Time using DateFormat.DEFAULT: "+timeDefault;   
       String dateMedium = DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(currentDate);  
       PRINT "Formatting the Date using DateFormat.MEDIUM: "+dateMedium;         
       String dateLong = DateFormat.getDateInstance(DateFormat.LONG, locale).format(currentDate);  
       PRINT "Formatting the Date using DateFormat.LONG: "+dateLong;    
       String dateShort = DateFormat.getDateInstance(DateFormat.SHORT, locale).format(currentDate);  
       PRINT "Formatting the Date using DateFormat.SHORT: "+dateShort; 
       String timeFull = DateFormat.getTimeInstance(DateFormat.FULL, locale).format(currentDate);  
       PRINT "Formatting the Time using DateFormat.FULL: "+timeFull;  
         
       
   }
}
Code:
*/ 

import java.text.DateFormat;
import java.util.Date;
import java.util.Locale; 
public class DateFormats {
	   public static void main(String args[]){		  		   
	       Date currentDate = new Date();  //Current Date
	       System.out.println("Current date is: "+currentDate); 	       
	       Locale locale = new Locale("English", "IN"); //Default time
	       String timeDefault = DateFormat.getTimeInstance(DateFormat.DEFAULT, locale).format(currentDate);  
	       System.out.println("Formatting the Time using DateFormat.DEFAULT: "+timeDefault);   		       
	       String dateMedium = DateFormat.getDateInstance(DateFormat.MEDIUM, locale).format(currentDate);  //Medium date
	       System.out.println("Formatting the Date using DateFormat.MEDIUM: "+dateMedium);  	       
	       String dateLong = DateFormat.getDateInstance(DateFormat.LONG, locale).format(currentDate);  //Long date
	       System.out.println("Formatting the Date using DateFormat.LONG: "+dateLong); 	       
	       String dateShort = DateFormat.getDateInstance(DateFormat.SHORT, locale).format(currentDate);   //Short date
	       System.out.println("Formatting the Date using DateFormat.SHORT: "+dateShort); 	       
	       String timeFull = DateFormat.getTimeInstance(DateFormat.FULL, locale).format(currentDate);  //Full time
	       System.out.println("Formatting the Time using DateFormat.FULL: "+timeFull);  
	         
	       
	   }
}
