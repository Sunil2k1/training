/*Find out the IP Address and host name of your local computer.
Requirement:
     - IP address and host name of localpc    
Entities:
     - AddressHostName     
Method signature:
  	 -none-
JobsToBeDone:
    * create a class name AddressHostName and Declare main method.
  	* Try block
  	     => Create InetAddress class to use get method.
  	     => Invoke a method to get ipaddress and host name.
         => Print the values.
    * Catch UnknownHostException
         => Print error message  
  
PseudoCode:
public class AddressHostName {
   public static void main(String[] args) {
      TRY{
         IP address and host name using methods      
         }
      CATCH UnknownHostException e{
         PRINT  error message   
        }
   }
}

Code:
*/

import java.net.InetAddress;
import java.net.UnknownHostException;

public class AddressHostName {
	public static void main(String[] args) {
		try {
			InetAddress my_address = InetAddress.getLocalHost();
			System.out.println("LocalHost is : " + my_address);
			System.out.println("The IP address is : " + my_address.getHostAddress());
			System.out.println("The host name is : " + my_address.getHostName());
			my_address = InetAddress.getByName("google.com.sa");	// ip address of a website
			System.out.println("Google inetaddress is : " + my_address);
		} catch (UnknownHostException e) {
			System.out.println("Couldn't find the local address.");
		}
	}
}
