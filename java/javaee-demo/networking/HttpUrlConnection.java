/*Create a program using HttpUrlconnection in networking.


Requirements:
    * Create a program using HttpURLConnection in networking.
    
Entities:
    * HttpUrlConnection
    
Method signature:
    -none-
  
Jobs to be done:
    * Create a class calledHttpUrlConnection.
    * Try catch 
        => Create a url path and set the  path name.
        => Create a object http and openConnection method is used to open the connection.
        => Using forloop to iterate the values and print.
        => Disconnect method to disconnect the connection.
        => Print the result.
    * Catch Exception
        => Print message for exception.    

Pseudo Code:
public class HttpUrlConnection {
	public static void main(String[] args) {
		TRY {
			URL url = new URL("https://www.linkedin.com/");
			HttpURLConnection http = (HttpURLConnection) url.openConnection();
			for (int i = 1; i <= 8; i++) {
				PRINT http.getHeaderFieldKey(i) + " = " + http.getHeaderField(i);
			}
			huc.disconnect();
		} CATCH Exception e {
			PRINT e;
		}
	}
}
*/

import java.net.HttpURLConnection;
import java.net.URL;
public class HttpUrlConnection {
	public static void main(String[] args) {
		try {
			URL url = new URL("https://www.linkedin.com/");
			HttpURLConnection http = (HttpURLConnection) url.openConnection();
			for (int i = 1; i <= 8; i++) {
				System.out.println(http.getHeaderFieldKey(i) + " = " + http.getHeaderField(i));
			}
			http.disconnect();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
