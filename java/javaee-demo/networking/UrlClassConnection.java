/* Create a program  for url class and url connection class in networking.

Requirements:
   * Create a program for url class  in networking
Entity:
   * UrlClassConnection
Method signature:
  -none-
Jobs to be done:
    * Create a class called UrlClass 
    * Try  
         => Create a url path and set the  path name.
         => getProtocol method to get protocol name of website through path name.
         => getHost method to  get hostname .
         => getPort method to get the portnumber.
         => getFile method to get the file name.
         => Print the results.
    * Catch Exception
        => Print message for exception.        

Pseudo Code:
public class UrlClassConnection {
	public static void main(String[] args) {
		TRY {
			URL url = new URL("https://www.linkedin.com/");
			PRINT "Protocol: " + url.getProtocol();
			PRINT "Host Name: " + url.getHost();
			PRINT "Port Number: " + url.getPort();
			PRINT"File Name: " + url.getFile();
		} CATCH Exception e {
			PRINT e;
		}
	}
}
Code:
*/

import java.net.URL;
public class UrlClassConnection {
	public static void main(String[] args) {
		try {
			URL url = new URL("https://www.linkedin.com/");
			System.out.println("Protocol: " + url.getProtocol());
			System.out.println("Host Name: " + url.getHost());
			System.out.println("Port Number: " + url.getPort());
			System.out.println("File Name: " + url.getFile());
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
