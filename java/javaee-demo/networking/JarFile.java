/*Create an jar file inside eclipse.
 
Requirement:
     * Create a jar file  
Procedure:

     * Either from the context menu or from the menu bar's File menu, select Export
     * Expand the Java node and select JAR file. Click Next.
     * In the JAR File Specification page, select the resources that you want to export in the Select the resources to export field.
     * Select the appropriate checkbox to specify whether you want to Export generated class files and resources or Export Java source 
       files and resources.  
     * In the Select the export destination field, either type or click Browse to select a location for the JAR file.
     * Select or clear the Compress the contents of the JAR file checkbox.
     * Select or clear the Overwrite existing files without warning checkbox. 
     * If you clear this checkbox, then you will be prompted to confirm the replacement of each file that will be overwritten.
     * You have two options:
	      => Click Finish to create the JAR file immediately.
	      => Click Next to use the JAR Packaging Options page to set advanced options, create a JAR description, or change the default manifest.
     * Go ahead and click finish.
     * Now, navigate to the location you specified for the jar. The icon you see and the behavior you get if you double 
       click it will vary depending on how your computer is set up.
*/