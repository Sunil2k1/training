/*Build a client side program using ServerSocket class for Networking.

Requirements:
    * Build a client side program using ServerSocket class for Networking.
Entities:
    * MyClient
Method signature:
    -none-
Jobs to be done:
    * Create class name MyClient and Declare main method.
    * Try  
        => Create a Socket class with localhost.
        => Pass argument of DataOutputStream class with getOutputStream method.
        => Write to server using writeUTF method
        => Close and flush the dataOutput.
    * Catch Exception.

Pseudo Code:
public class MyClient {
	public static void main(String[] args) {
		TRY {
			Socket socket = new Socket("localhost", 007);
			DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());
			dataOutput.writeUTF("Client Server");
			dataOutput.flush();
			dataOutput.close();
			socket.close();
		} CATCH Exception e {
			PRINT e;
		}
	}
}

Code:
*/


import java.io.DataOutputStream;
import java.net.Socket;

public class MyClient {
	public static void main(String[] args) {
		try {
			Socket socket = new Socket("localhost", 007);
			DataOutputStream dataOutput = new DataOutputStream(socket.getOutputStream());
			dataOutput.writeUTF("Client Server");
			dataOutput.flush();
			dataOutput.close();
			socket.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
