/* Build a server side program using Socket class for Networking.


Requirements:
    * Build a client side program using ServerSocket class for Networking.
Entities:
    * MyClient
Method signature:
    -none-
Jobs to be done:
    * Create a Class name MyServer and Declare main method
    * Try catch 
        => Create a ServerSocket class with localhost.
        => Accept serverSocket using accpet mathod.
        => Pass argument of DataOutputStream class with getInputStream method.
        => Read to client passed string using readUTF method
        => Print and close the dataOutput.
    * Catch Exception.

Pseudo Code:

public class MyServer {
	public static void main(String[] args) {
		TRY {
			ServerSocket serverSocket = new ServerSocket(007);
			Socket socket = serverSocket.accept();// establishes connection
			DataInputStream dataInput = new DataInputStream(socket.getInputStream());
			String string = (String) dataInput.readUTF();
			PRINT string;
			serverSocket.close();
		} CATCH Exception e {
			PRINT e;
		}
	}
}

Code:
*/



import java.io.DataInputStream;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer {
	public static void main(String[] args) {
		try {
			ServerSocket serverSocket = new ServerSocket(007);
			Socket socket = serverSocket.accept();// establishes connection
			DataInputStream dataInput = new DataInputStream(socket.getInputStream());
			String string = (String) dataInput.readUTF();
			System.out.println(string);
			serverSocket.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
}
