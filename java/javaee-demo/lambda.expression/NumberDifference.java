/*Write a program to print difference of two numbers using lambda expression and the single method interface


Requirements
   * two number required
Entities
   * Different - Interface
   * NumberDifference
Function Declaration
   * int numbers(int value1,int value2);
Jobs to be done
   * In Interface Difference
       => Put int numbers(int value1,int value2) Method.
   * Create a class as NumberDifference and declare main.
   * Create the object for  Difference name difference and Using lambda expression 
             => find the difference between 2 values.
   * Call the method difference.numbers() passing the parameters in the method and print it

Psudocode:

INTERFACE Difference{
	int numbers(int value1,int value2);
}

public class NumberDifference {
	public static void main(String[] args) {
		Difference difference = (value1,value2) -> VALUE1 - VALUE2;
		PRINT difference.numbers(20,10);
	}
}
Code: */


interface Difference{
	int numbers(int value1,int value2);
}

public class NumberDifference {
	public static void main(String[] args) {
		Difference difference = (value1,value2) -> value1 - value2;
		System.out.println(difference.numbers(20,10));
	}
}
