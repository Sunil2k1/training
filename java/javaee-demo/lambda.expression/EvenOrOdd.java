/*Convert the following anonymous class into lambda expression.
interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = new CheckNumber() {
            public boolean isEven(int value) {
                if (value % 2 == 0) {
                    return true;
                } else return false;
            }
        };
        System.out.println(number.isEven(11));
    }
}

Requirements
   * A integer
Entities
   * CheckNumber - Interface
   * EvenOrOdd
Function Declaration
   * int numbers(int number1,int number2);
Jobs to be done
   * In Interface  CheckNumber 
       => Put isEven(int value) method in Boolean.
   * Create a class as EvenOrOdd and declare main.
   * Check number
        => if value 5 % 2 is Equal to Zero
            ->Return true
        => Else Return true      
   * Print number.isEven(2)

Psudocode:
INTERFACE CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = (value) -> { // Convert into Java Lambda Expression 
                IF value % 2 == 0 {
                    RETURN true;
                } ELSE RETURN false;
            };
        PRINT number.isEven(2);
    }
}
Code:*/ 	

interface CheckNumber {
    public boolean isEven(int value);
}

public class EvenOrOdd {
    public static void main(String[] args) {
        CheckNumber number = (value) -> { // Convert into Java Lambda Expression 
                if (value % 2 == 0) {
                    return true;
                } else return false;
            };
        System.out.println(number.isEven(2));
    }
}