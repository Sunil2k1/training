/*  Write a program to print the volume of a Rectangle using lambda expression.

Requirements
   * This code Required 
Entities
   * Volume - Interface
   * Rectangle
3.Function Declaration
   * int Values(int base,int length,int height);
4.Jobs to be done
   * In Interface Volume
       => Put int Values(int base,int length,int height) Method.
   * Create a class as Rectangle and declare main.
   * Create the object for  Volume name volume and Using lambda expression 
       => find the volume of the Rectangle.
   * Call the method volume.Values() passing the parameters in the method and print it

Psudocode:


INTERFACE Volume {
	int Values(int base,int length,int height);
}


public class Rectangle {

	public static void main(String[] args) {
		Volume volume = (base,length,height) -> VOLUME OF RECTANGLE;
		PRINT volume.Values(10,5,18);

	}

}
*/



interface Volume {
	int Values(int base,int length,int height);
}


public class Rectangle {

	public static void main(String[] args) {
		Volume volume = (base,length,height) -> base * length * height;
		System.out.println(volume.Values(10,5,18));

	}

}
