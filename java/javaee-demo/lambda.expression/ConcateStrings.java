/*Write a Lambda expression program with a single method interface to concatenate two strings
  
Requirements
   * In this we need two Strings
Entities
   * SingleInterface - Interface
   * ConcateStrings
Function Declaration
   * String string(String car, String bike);
Jobs to be done
   * In Interface  SingleInterface
       => Put string(String car, String bike) method in String.
   * Create a class as ConcateStrings and declare main.
   * Create the object for  SingleTnterface name singleTnterface and using lambda expression 
       => concatinate 2 String.
   * Call the method singleInterface.string() passing the parameters in the method and print it

Psudocode:
   
INTERFACE SingleInterface {
	String string(String car, String bike);
}

public class ConcateStrings {
	public static void main(String[] args) {
		SingleInterface singleInterface = (car,bike) -> car.concat(bike);
		PRINT singleInterface.string;
	}

}
Code  */
interface SingleInterface {
	String string(String car, String bike);
}

public class ConcateStrings {

	public static void main(String[] args) {
        
		//Assigning the lambda expression to return two strings concatenate
		SingleInterface singleInterface = (car,bike) -> car.concat(bike);
        
		//invoking the interface single method with string values 
		System.out.println(singleInterface.string("Best Car = Maclaren And ","Best Bike = Dugati"));
	}

}
