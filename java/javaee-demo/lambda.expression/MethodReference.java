/* What is Method Reference and its types.Write a program for each types with suitable comments.
 
   
Requirements
   * Write a program for each types with suitable comments.
Entities
    * Addition
    * MethodReference
    * Interfaces
       - StaticMethod
       - ParameterMethod
       - InstanceMethod
       - ConstructorReference
   
Function Declaration
   * public static void display()
   * public void printer()
   * public int find(String string1, String string2)
   * public void print(String string)
   * Addition printAdd(int number1,int number2)
   * Addition(int number1,int number2)(Constructor)
Jobs to be done   
   * In Interface  StaticMethod
       => Put public void printer() method.
   * In Interface ParameterMethod 
	   => Put public int find(String car, String bike) method. 
   * In Interface InstanceMethod 
	   => Put public void print(String car);
   * In Interface ConstructorReference 
	   => Put Addition printAdd(int value1,int value2).  	      
   * Create a class as MethodReference
   * In void display () method
       => Print"=> It displays Static Method Reference"  
   * In main Method
       => Referring Static method and put in printStatement.
       => Calling the method printer() by usins printStatemnt
       => Find a word in a sentence by using find() method
       => Referring ConstructorReference and put in constructorReference.  
       => Call the printAdd() method and passing parameter in it by using constructorReference and print it.
Psudocode:

INTERFACE StaticMethod {
	public void printer(); 
}

INTERFACE ParameterMethod {
	public int find(String car, String bike);
}

INTERFACE InstanceMethod {
	public void print(String car);
}

interface ConstructorReference {  

	Addition printAdd(int value1,int value2);  
}

class Addition{       // Addition class
	Addition(int value1,int value2) {  
        PRINT value1 + value2;  
    }  
}  

public class MethodReference {
	public static void display(){  
        PRINT "=> It displays Static Method Reference";  
    }  
	
    public static void main(String[] args) {  
       
    	StaticMethod printStatement = MethodReference::display;   // Referring static method  
    	PRINT "Static Method Reference:";
    	printStatement.printer();           	
        ParameterMethod finder = String::indexOf;
        PRINT "\nParameter Method Reference";
        PRINT finder.find("Bugati is the fastest car in the world","car");        
        InstanceMethod myPrinter = System.out::println;
        PRINT "\nInstance Method Reference:";
        myPrinter.print("=> It displays Instance Method Reference");             
        ConstructorReference constructorReference = Addition::new;
        PRINT"\nConstructor Reference";
        constructorReference.printAdd(10,20);
    } 
Code:
*/
interface StaticMethod {
	public void printer(); 
}

interface ParameterMethod {
	public int find(String car, String bike);
}

interface InstanceMethod {
	public void print(String car);
}

interface ConstructorReference {  

	Addition printAdd(int value1,int value2);  
}

class Addition{       // Addition class
	Addition(int value1,int value2) {  
        System.out.print(value1 + value2);  
    }  
}  

public class MethodReference {

	public static void display(){  
        System.out.println("=> It displays Static Method Reference");  
    }  
	
    public static void main(String[] args) {  
       
    	StaticMethod printStatement = MethodReference::display;   // Referring static method  
    	System.out.println("Static Method Reference:");
    	printStatement.printer();
        
    	
        ParameterMethod finder = String::indexOf;
        System.out.println("\nParameter Method Reference");
        System.out.println(finder.find("Bugati is the fastest car in the world","car"));
        
        
        InstanceMethod myPrinter = System.out::println;
        System.out.println("\nInstance Method Reference:");
        myPrinter.print("=> It displays Instance Method Reference");
        
        
        ConstructorReference constructorReference = Addition::new;
        System.out.println("\nConstructor Reference");
        constructorReference.printAdd(10,20);
    } 
}  