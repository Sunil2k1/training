/*What's wrong with the following program? And fix it using Type Reference

interface BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (int number1, int number2) ->  { 
        return number1 + number2;
        };
        
        int print = function.print(int 23,int 32);
        
        System.out.println(print);
    }
}
Output:
javac TypeInferenceExercise.java
Process started (PID=7904) >>>
TypeInferenceExercise.java:12: error: '.class' expected
        int print = function.print(int 23,int 32);
                                       ^
TypeInferenceExercise.java:12: error: <identifier> expected
        int print = function.print(int 23,int 32);
                                          ^
TypeInferenceExercise.java:12: error: not a statement
        int print = function.print(int 23,int 32);
                                              ^
TypeInferenceExercise.java:12: error: ';' expected
        int print = function.print(int 23,int 32);
                                                ^
4 errors
<<< Process finished (PID=7904). (Exit code 1)
================ READY ================   

Requirements
   * This code requires two integers
Entities
   * BiFunction - Interface
   * TypeInferenceExercise
Function Declaration
   * int print(int number1, int number2);
4.Jobs to be done
   * In Interface BiFunction
       => Put int print(int number1, int number2) Method.
   * Create a class as TypeInferenceExercise and declare main.
   * Create the object for  Volume name volume and Using lambda expression 
               => find the Addtion of 2 numbers.
   * Call the method function.print() passing the parameters in the method and print it
   
Psudocode:

INTERFACE BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (number1, number2) -> number1 + number2; 
        PRINT function.print(25,5); 
    }
}
Correct Code:*/ 
interface BiFunction{
    int print(int number1, int number2);
}

public class TypeInferenceExercise {
    public static void main(String[] args) {

        BiFunction function = (number1, number2) -> number1 + number2; 
        System.out.println(function.print(25,5)); 
    }
}