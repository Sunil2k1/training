/* Code a functional program that can return the sum of elements of varArgs, passed into the 
method of functional interface.

Requirements
   * Code requires multiple values of integers.
   
Entities
   * MultipleValues - Interface
   * VarArgsInterface
   
Function Declaration
   * int sum(int... numbers);
   
Jobs to be done
   * In Interface MultipleValues
       => Put int sum(int... numbers) Method.
   * Create a class as VarArgsInterface and declare main.
   * Create the object for  Difference name difference and Using lambda expression 
       => int sum is equal to zero
       => for each number
           -> add sum and i is equal to sum
       => return sum    
   * Call the method multipleValues.sum() passing the parameters in the method and print it

Psudocode:
INTERFACE MultipleValues {
	int sum(int... numbers);
}
public class VarArgsInterface {

	public static void main(String[] args) {
		MultipleValues multipleValues = (int... numbers) -> {
		INTEGER SUM IS EQUAL TO ZERO;
		FOR int i : numbers 
		    sum += i;  
		  RETURN sum; 
		};
		PRINT multipleValues.sum(5,20,4,3,6);
	}
Code:
*/


interface MultipleValues {
	int sum(int... numbers);
}
public class VarArgsInterface {

	public static void main(String[] args) {
		MultipleValues multipleValues = (int... numbers) -> {
		int sum = 0;
		for (int i : numbers)  
		    sum += i;  
		  return sum; 
		};
		System.out.println(multipleValues.sum(5,20,4,3,6));
	}

}
