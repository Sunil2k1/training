/*+ What's wrong with the following program? And fix it.

    public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect;
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }

Requirements :
   myRect.width = 40;
   myRect.height = 50;
Entitties:
   Rectangle
   SomethingIsWrong
Function Declaration:
   -none-
Job to be done : 
  * Create a public class and name is SomethingIsWrong. 
  * Then put public static void main(String [] args)
  * Then create a object myRect for Rectangle.
  * Initialize the value for width = 40 and myRect.height = 50;
  * print area for the rectangle.
Output:
javac SomethingIsWrong.java
Process started (PID=7760) >>>
SomethingIsWrong.java:3: error: cannot access Rectangle
            Rectangle myRect;
            ^
  bad source file: .\Rectangle.java
    file does not contain class Rectangle
    Please remove or make sure it appears in the correct subdirectory of the sourcepath.
1 error
<<< Process finished (PID=7760). (Exit code 1)
================ READY ================


After Fixing Problem:*/
import e.Rectangle;
public class SomethingIsWrong {
        public static void main(String[] args) {
            Rectangle myRect = new Rectangle();
            myRect.width = 40;
            myRect.height = 50;
            System.out.println("myRect's area is " + myRect.area());
        }
    }

/*Output:
java SomethingIsWrong
Process started (PID=6880) >>>
myRect's area is 2000
<<< Process finished (PID=6880). (Exit code 0)
================ READY ================*/