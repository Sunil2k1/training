/*+ Consider the following class:
    public class IdentifyMyParts {
        public static int x = 7;
        public int y = 3;
    }
    - What are the class variables?
    - What are the instance variables?

1. What are the class variables?

Requirements :
 x = 7
Entitties:
 IdentifyMyParts
Function Declaration:
 -none-
Job to be done :
   * Create a method name IdentifyMyParts.
   * Declare a variable with static keyword and initializing x =7.
   * Declare a variable and initializing y =3.
What are the class variables?

Class Variable :  
   Instance Variables are  declared  with a static keyword in a class ,but outside a method.
   In this case the Class variable is x.   

2. What are the instance variables?

Requirements :
 y = 3
Entitties:
 IdentifyMyParts
Function Declaration:
 public class IdentifyMyParts();
Job to be done :
   * Create a method name IdentifyMyParts.
   * Declare a variable with static keyword and initializing x =7.
   * Declare a variable and initializing y =3.
What are the instance variables?

Instance Variables:
   Instance Variables are  declared in a class ,but outside a method.
   In this case the Instance variable is y.       */ 