/*+ Given the following class, called NumberHolder,
    write some code that creates an instance of the class and initializes its two member variables with provided values,
    and then displays the value of each member variable.

    public class NumberHolder {
        public int anInt;
        public float aFloat;
    }

Requirements :
  anInt
  aFloat  
Entitties:
  NumberHolder
Function Declaration:
 public class NumberHolder();
Job to be done :
 * Create a public class name NumberHolder
 * Declare anInt as int and aFloat as float


Code :*/
    public class NumberHolder {
        public int anInt;
        public float aFloat;
        public static void main(String [] args) {
            int anInt = 20;
            float aFloat = 10;
            System.out.println("The value of anInt is : "+anInt);
            System.out.println("The value of aFloat is : "+aFloat);
        }
    }    
        




/*Output:

java NumberHolder
Process started (PID=14760) >>>
The value of anInt is : 20
The value of aFloat is : 10.0
<<< Process finished (PID=14760). (Exit code 0)
================ READY ================*/