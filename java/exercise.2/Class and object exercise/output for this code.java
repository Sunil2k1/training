/*+ What is the output from the following code:
    IdentifyMyParts a = new IdentifyMyParts();
    IdentifyMyParts b = new IdentifyMyParts();
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
    System.out.println("a.y = " + a.y);
    System.out.println("b.y = " + b.y);
    System.out.println("a.x = " + a.x);
    System.out.println("b.x = " + b.x);
    System.out.println("IdentifyMyParts.x = " + IdentifyMyParts.x);

Requirements :
    a.y = 5;
    b.y = 6;
    a.x = 1;
    b.x = 2;
Entitties:
 IdentifyMyParts
Function Declaration:
 public class IdentifyMyParts();
Job to be done :
   * Create a new object a for IdentifyMyParts method.
   * Create a new object b for IdentifyMyParts method.
   * Initialize a.y = 5,b.y = 6,a.x = 1 and b.x = 1.
   * And then printing the values for a.x.a.y,b.x and b.y and also print the value of x as IdentifyMyParts.x.

Output:
java Demo
Process started (PID=8260) >>>
a.y = 5
b.y = 6
a.x = 2
b.x = 2
IdentifyMyParts.x = 2
<<< Process finished (PID=8260). (Exit code 0)
================ READY ================*/