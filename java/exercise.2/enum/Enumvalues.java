/*+ compare the enum values using equal method and == operator
Requirements :
  Season,
  Month
Entitties:
 -none-
Function Declaration:
 public enum Season()
 public enum Month()
Job to be done :
 * Create a class name Enumvalues
 * Inside that create a method name Season and enter values.
 * Inside that create a method name Month and enter values.
 * inside that create public static void main
 * print the result.
*/
Code:
class Enumvalues{
    public enum Season {
        SUMMER,
        WINTER,
        AUTUMN,
        SPRING
    }
    public enum Month{
        JAN,
        FEB,
        MARCH,
        APRIL,
        MAY,
        JUNE,
        JULLY,
        AUGUST,
        SEPT,
        OCT,
        NOV,
        DEC
    }
    public static void main(Strin [] args) {
        //comparing two enum operator using equals() method.
        System.out.println(Month.May.equals(Season.Summer));
        //comparing two enum operator using == operator.
        System.out.println(Month.DEC == Season.Winter));
        
    }
}