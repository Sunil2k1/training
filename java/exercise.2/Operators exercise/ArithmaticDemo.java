/*+ Change the following program to use compound assignments:
       class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 1 + 2; // result is now 3
                 System.out.println(result);

                 result = result - 1; // result is now 2
                 System.out.println(result);

                 result = result * 2; // result is now 4
                 System.out.println(result);

                 result = result / 2; // result is now 2
                 System.out.println(result);

                 result = result + 8; // result is now 10
                 result = result % 7; // result is now 3
                 System.out.println(result);
            }
       }
Requirements :
  result = 1 + 2
Entitties:
 ArithmeticDemo 
Function Declaration:
 -none-
Job to be done :
  * Create a class with name  ArithmeticDemo.
  * Put public static void main.
  * Inside that declare that result with int datatype and intialize result = 1 + 2 and print result. 
  * And then update result value as result = result - 1 and print result.
  * And then update result value as result = result * 2 and print result.
  * And then update result value as result = result / 2 and print result.
  * And then update result value as result = result + 8.
  * And then update result value as result = result % 7 and print result.

Using Compund operators then code is:*/

       class ArithmeticDemo {

            public static void main (String[] args){

                 int result = 3; // result is now 3
                 System.out.println(result);

                 result -= 1; // result is now 2
                 System.out.println(result);

                 result *= 2; // result is now 4
                 System.out.println(result);

                 result /=2; // result is now 2
                 System.out.println(result);

                 result += 8; // result is now 10
                 result %=7; // result is now 3
                 System.out.println(result);
            }
       }

/*Output:

java ArithmeticDemo
Process started (PID=4116) >>>
3
2
4
2
3
<<< Process finished (PID=4116). (Exit code 0)
================ READY ================*/
 