/*+ demonstrate inheritance, overloading, overriding using Animal, Dog, Cat and Snake class of objects

code using inheritance, overloading, overriding using Animal, Dog class object:


Requirements :
  dogname
  catname
  snakename
  dogcount
  catcount
  snakecount
Entitties:
  Animal
  Dog
  Cat
  Snake
  Inheritance
Function Declaration:
* public void habitate()
    ->this method is present in th class Animal.
    ->When this method calls this prints Domestic Animal. 
* public void sigAmtDog()
    ->this method is present in th class Dog which is extends from class Animal.
    ->When this method calls ,it return the single dog value as 3000. 
* public void sigAmtCat()
    ->this method is present in th class Cat which is extends from class Animal.
    ->When this method calls ,it return the single cat value as 2000.     
* public void sigAmtSnake()
    ->this method is present in th class Dog which is extends from class Animal.
    ->When this method calls ,it return the single snake value as 3500.          
Job to be done :
  * Create a class name Animal.
  * Create a Class name Dog which is extend from class Animal.
  * Create a Class name Cat which is extend from class Animal.
  * Create a Class name Snake which is extend from class Animal.
  * Create a Class name Inheritance.
  * Inside that class create public class void main.
  * Declare and intialize dogcount = 20
  * Create a object d for class Dog.
  * Create a object cat for class Cat.
*/
class Animal{
    static String dogname;
    static String catname;
    static String snakename;
    public void habitate(){
        System.out.println("Domestic Animal");
    }

}
class Dog extends Animal{
    static int dogcount;
    public int sigAmtDog() {
        return 3000; 
    }    
}
class Cat extends Animal{
    static int catcount;
    public int sigAmtcat() {
        return 2000; 
    }    
}
class Snake extends Animal{
    static int snakecount;
    public int sigAmtSnake() {
        return 3500; 
    }    
}
public class Inheritance {
    public static void main(String [] args) {
        int dogcount = 20;    
        // method overloading    
        Dog d = new Dog();
        System.out.println(d.sigAmtDog()); 
        //method overriding
        Animal cat = new Animal();
        cat.habitate();
        
    }
}

/*Output:
java Demo
Process started (PID=14712) >>>
3000
Domestic Animal
<<< Process finished (PID=14712). (Exit code 0)
================ READY ================*/