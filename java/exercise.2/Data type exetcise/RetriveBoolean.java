/*Program to store and retrieve four boolean flags using a single int.

Requirments:
    * Program to store and retrieve four boolean flags using a single int.
Entities:
    * RetriveBoolean
Function Declaration:
    * public static void main(String[] args)
Jobs to be done:
     * Create a class called RetriveBoolean with main method and using boolean initialise a value.
     * put public static void main.
     * Initialize flag = false   
     * By using if Condition 
*/  
            
public class RetriveBoolean {
    public static void main(String[] args) {
        //To Invert the Value of the Boolean//
        boolean flag = false;
        String res = "Fail";
        if(res == "Pass") {
            System.out.println(flag);
        } else {
            flag = !flag;
            System.out.println(flag);
        }
    }
}