/*
   Overloading with Wrapper types
    
Entity
    * Wrapper Class
    * WrapperOverLoad Class
    
Jobs to be done
    * Created a Wrapper class to Demonstrate the Overloading with Wrapper Class.
    * Created a method which gets Primitive type int as the parameter .
    * Created a method which gets the Primitive type char as the parameter.
    * Created a method which gets the Primitiev type double as the parameter.
    * Calling the wrapCharSetter method and passing the Wrapper type Character as the argument.
    * Caliing the warpDoubleSetter method and passing the Wrapper type Double as the argument.
*/

class Wrapper {
    void wrapIntSetter(int a) {
        System.out.println("int Value changed to Integer "+a);
    }
    
    void wrapcharSetter(char b) {
        System.out.println("char Value changed to Character "+b);
    }
    
    void wrapDoubleSetter(double c) {
        System.out.println("double Value changed to Double "+c);
    }
}

public class WrapperOverload {
    public static void main(String[] args) {
        //Overloading Wrapper types//
        
        Integer number = 65;
        Character letter = 'S';
        Double decimal = 3.14;
        Wrapper wrap = new Wrapper();
        wrap.wrapIntSetter(number);
        wrap.wrapcharSetter(letter);
        wrap.wrapDoubleSetter(decimal);
    }
}