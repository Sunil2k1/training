/* What is the value of the following expression, and why?
Integer.valueOf(1).equals(Long.valueOf(1)).


Requirement:
    -What is the value of the following expression, and why?
Entity:
    -none-
Function Declaration:
    -none-
    
What is the value of the following expression, and why?
Integer.valueOf(1).equals(Long.valueOf(1)).

Ans: False, The two objects the Integer and the Long have different types.*/