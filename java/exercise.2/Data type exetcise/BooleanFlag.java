/*Program to store and retrieve four boolean flags using a single int.

Requirments:
    -Program to store and retrieve four boolean flags using a single int.
Entities:
    -BooleanFlag
Function Declaration:
    -public static void main(String[] args)
Jobs to be done:
    * Create a class called BooleanFlag.
    * Declare main method inside the class and declare five integer variables apple,mango,banana,
watermelon,orange    initialise a values.
    * Prints the variables initialised and create a myFruitBag integer variable for containing five
variables
    * Initialise myFruitBag variable with different variables in or condition and in the print 
statement every five variables check and condition greater than zero if condition is true prints
found with variable name if false print not found that variable name.
*/

public class BooleanFlag {
    public static void main(String[] args) {
        // define fruits
        int apple = 1;
        int mango = 2;
        int banana = 4;
        int watermelon = 8;
        int orange = 16;
        System.out.println("Weapons defined: apple,mango,banana,watermelon,orange");
        
        // create fruit bag
        int myFruitBag;
        System.out.println("Initiating fruit bag...");
        
        // store fruit in bag
        System.out.println("Storing a few fruits in bag (apple, mango, watermeloon)...");
        myFruitBag = apple | mango | watermelon;
        
        // check for fruits
        System.out.println("Looking for fruits...");
        System.out.println((myFruitBag & apple) > 0 ? "--> Found apple" : "NO apple");
        System.out.println((myFruitBag & mango) > 0 ? "--> Found mango" : "NO mango");
        System.out.println((myFruitBag & banana) > 0 ? "--> Found banana" : "NO banana");
        System.out.println((myFruitBag & watermelon) > 0 ? "--> Found watermelon" : "NO watermelon");
        System.out.println((myFruitBag & orange) > 0 ? "--> Found orange" : "NO orange");
    }
}