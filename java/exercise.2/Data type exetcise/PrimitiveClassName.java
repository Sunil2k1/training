/*
Find the ClassNames of the Primitive Datatype.
    
Entity
    * PrimitiveClassNames Class
    
Jobs Work Done
    * Getting the ClassName of a Primitive type by using the .class and getName method.
    * Storing the int class name into a String called intClassName and getting the className of the Respective 
datatype with .class and className method..
    * Storing the char class name into a String called charClassName and getting the className of the Respective 
datatype with .class and className method..
    * Storing the double class name into a String called doubleClassName and getting the className of the Respective 
datatype with .class and className method..
    * Storing the float class name into a String called floatClassName and getting the className of the Respective 
datatype with .class and className method..
*/

public class PrimitiveClassNames {
    public static void main(String[] args) {
        //ClassNames of Primitive Datatypes//
        
        String intClassName = int.class.getName();
        System.out.println("ClassName of Int : "+intClassName);
        
        String charClassName = char.class.getName();
        System.out.println("ClassName of Char : "+charClassName);
        
        String doubleClassName = double.class.getName();
        System.out.println("ClassName of double : "+doubleClassName);
        
        String floatClassName = float.class.getName();
        System.out.println("ClassName of float : "+floatClassName);
    }
}