/* Create a program that is similar to the previous one but instead of reading integer arguments, 
it reads floating-point arguments.
    It displays the sum of the arguments, using exactly two digits to the right of the decimal point.
    For example, suppose that you enter the following: java FPAdder 1 1e2 3.0 4.754
    The program would display 108.75. Depending on your locale, the decimal point might be a 
comma (,) instead of a period (.). 

Requirement:
    * To program that is similar to the previous one but instead of reading integer arguments, 
it reads floating-point arguments.
Entity:
    * Adder
Function Declaration:
    * public static void main(String[] args)
Jobs to be done
    * Create a class called FloatAdder.
    * Initialise numArgs variable for find the length arguments.
    * Check the number of numArgs is less than two and in else statement initialise value.
    * To calculate the arguments using Forloop and print the output.

*/

import java.text.DecimalFormat;

public class FloatAdder {
    public static void main(String[] args) {

    int numArgs = args.length;
    
    //this program requires at least two arguments on the command line
           if (numArgs < 2) {
               System.out.println("This program requires two command-line arguments.");
           } else {
        double sum = 0.0;
    
        for (int i = 0; i < numArgs; i++) {
            sum += Double.valueOf(args[i]).doubleValue();
        }
    
        //format the sum
        DecimalFormat myFormatter = new DecimalFormat("###,###.##");
        String output = myFormatter.format(sum);
    
        //print the sum
            System.out.println(output);
        }
    }
}