package shape;  //package name shape
public class Circle extends Shape {   
    public float area;
    public float perimeter; 
    public void circleArea() {
       area = 2 * 3.14f * radius * radius;
       System.out.println("Area of the circle is "+area);   
    }  
    public void circlePerimeter() {
       perimeter = 2 * 3.14f * radius;
       System.out.println("Perimeter of the circle is "+perimeter);        
    }    
}   