package shape; //package name shape
public abstract class Shape{   //abstract class
    public float length;
    public float radius;
    public void squareArea(float length) {
          System.out.println("Area of the Square");          
    }
    public void circleArea() {
          System.out.println("Area of the Circle");          
    }
    public void circlePerimeter() {
          System.out.println("Perimeter of the Circle");          
    }  
}    
    
 
