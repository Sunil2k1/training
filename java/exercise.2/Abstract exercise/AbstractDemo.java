/*+ Demonstrate abstract classes using Shape class.
    - Shape class should have methods to calculate area and perimeter
    - Define two more class Circle and Square by extending Shape class and implement the calculation for each class respectively
    
 Requirments:
    ->radius of the circle
    ->length of the square    
 Enities:
    ->Square
    ->Circle 
    ->AbstractDemo
 Function Declaration:
    -> public void  squareArea()
      * In this we are calculating area for the square by using formula.
    -> public void  circleArea()
      * In this we are calculating area for the circle by using formula.
    -> public void  circlePerimeter()
      * In this we are calculating perimeter for the circle by using formula.      
 Work to be done:
    -> Create a package name shape.
    -> Create a Abstract class Shape and inside that create methods circleArea,circlePerimeter and squareArea.
    -> Crete a class name Square which is extend from Shape. 
    -> Crete a class name Circle which is extend from Shape.
    -> Create a class AbstractDemo.
    -> Inside that public static void main
    -> By using switch case and calling the method we can calculate area and perimeter.    
    */



import java.util.Scanner;
import shape.Square;
import shape.Circle;
public class AbstractDemo{
    public static void main(String [] args){
    int a = 0;
    while(a == 0) {
            Scanner sc=new Scanner(System.in);
            System.out.println("1.Circle, 2.Square");
            int ch=sc.nextInt();
            switch(ch){
                  case 1:
                     Circle circle =new Circle();
                     System.out.println("enter a radius: ");
                     circle.radius=sc.nextFloat();
                     circle.circleArea();
                     circle.circlePerimeter();
                     break;     
                  case 2:
                     Square square =new Square();
                     System.out.println("enter a length: ");
                     square.length=sc.nextFloat();
                     square.squareArea();
                     break;  
                 default:
                     System.out.println("Enter a valid number");                 
            }
            System.out.println("If you want to continue enter 0 or exit enter 1");
            a = sc.nextInt();
      }        
    }    
}
