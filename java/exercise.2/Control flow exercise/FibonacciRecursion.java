/*+ print fibinocci using for loop, while loop and recursion

Requirements :
  n1,
  n2,
  n3,
  count
Entitties:
 FibonacciRecursion
Function Declaration:
 printFib(int count);
Job to be done :
 * create a class name FibonacciRecursion.
 * Create a method inside that class and calcuating and printing the fibonacci number
 * create public static void main
 * Inside that initialize and declare the values for count and call the printFib method 

Fibinocci using recursion:*/
class FibonacciRecursion{
    static int n1 = 0,n2 =1 ,n3=0;
    static void printFib(int count) {
        if(count > 0) {
            n3 = n1 + n2;
            n1 = n2;
            n2 = n3;
            System.out.println(n3);
            printFib(count-1);
        }
    }
    public static void main(String [] args) {
        int count = 10;
        System.out.println(n1+" "+n2);
        printFib(count-2);
    }
}

/*output:

Process started (PID=9420) >>>
0 1
1
2
3
5
8
13
21
34
<<< Process finished (PID=9420). (Exit code 0)
================ READY ================*/