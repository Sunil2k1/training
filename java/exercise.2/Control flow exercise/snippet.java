/*+ Consider the following code snippet.

        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else System.out.println("second string");
        System.out.println("third string");

        - What output do you think the code will produce if aNumber is 3?

        Write a test program containing the previous code snippet
        - and make aNumber 3. What is the output of the program?
        - Using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
        - Use braces, { and }, to further clarify the code.

Requirements :
  aNumber
Entitties:
 -none-
Function Declaration:
 -none-
Job to be done :
 * using if condition check aNumber is greater or equal to 0.
 *Inside if condition another if condition that aNumber == 0 if it satisfied then print firststring or else print second string.
 *at last print third string 

What output do you think the code will produce if aNumber is 3?

java Demo
Process started (PID=8136) >>>
second string
third string
<<< Process finished (PID=8136). (Exit code 0)
================ READY ================


Write a test program containing the previous code snippet
- and make aNumber 3. What is the output of the program?
- Using only spaces and line breaks, reformat the code snippet to make the control flow easier to understand.
- Use braces, { and }, to further clarify the code.

public class Demo{
    public static void main(String [] args) {
        int aNumber = 3;
        if (aNumber >= 0)
            if (aNumber == 0)
                System.out.println("first string");
        else {
            System.out.println("second string");
        }
        System.out.println("third string");
    }
}*/