/*What is wrong with the following interface? and fix it.
    public interface SomethingIsWrong {
        void aMethod(int aValue){
            System.out.println("Hi Mom");
        }
    }

Requirement:
    -int aValue
Entity:
    -No Entity
Function Declaration:
    -default void aMethod(int aValue);
Jobs to be done:
     Create an interface called FixedCode with default method aMethod with integer parameter
and print the statement.


What is wrong with the following interface?
   It has a method implementation in it. Only default and static methods have
implementations.

Fixed code.*/
public interface InterfaceFixedCode {
    default void aMethod(int aValue) {
        System.out.println("Hi Mom");
    }
}