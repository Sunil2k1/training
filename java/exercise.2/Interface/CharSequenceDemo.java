/*Write a class that implements the CharSequence interface found in the java.lang package.
  Your implementation should return the string backwards. Write a small main method to test your class; make sure to call all four methods.


Requirement:
      -Write a class that implements the CharSequence interface found in the java.lang package.
      -Your implementation should return the string backwards. Write a small main method to test your class; make sure to call all four methods.
Entity:
      -CharSequenceDemo
Function Declaration:
      -public CharSequenceDemo(String s)
      -private int fromEnd(int i)
      -public char charAt(int i)
      -public int length()
      -public CharSequence subSequence(int start, int end)
      -public String toString()
      -private static int random(int max)
      -public static void main(String[] args)
Jobs to be done:
    * Create an class called CharSequenceDemo with implements CharSequence.
    * Declare string variable and declare CharSequenceDemo constructor and initialise the string.
    * An integer method to return an end of the string passing string length and it is invoke by 
char method when the check condition is false and return returned value from integer method.
    * To find the length of the given string create an integer method and retruning the string 
length value.
    * Using interface create a subSequence method with two parameters start ans end.Check three if 
condition.
        -> start is less than zero and prints if true throw error StringIndexOutOfBoundsException 
passing start variable.
        -> end is greater than string length and prints if true throw error StringIndexOutOfBoundsException 
passing end variable.
        -> start is greater than end and prints if true throw error StringIndexOutOfBoundsException 
passing subracting start with end variable.
        -> If the above conditions are false create an object subString and invoke subSequence and 
in the parameters invoke fromEnd method passing each variable start and end returns the reverse of 
string. 
    * The toString method create a string object and invoke the string object reverse method and 
toString method.
    * To generates random values declare the random method with return the math random calculation
    * In the main method create an object for CharSequence class with passing arguments.
    * Using for loop initialise, check string length and increment string index value and invoke 
charAt method in print statement.
    * Initialise start and end variable invoke random method value in start variable subract one 
and in end variable subract one and start adding start value.
    * In the print statement, invoke subSequence method with start and end variable.Print the 
string   */

public class CharSequenceDemo implements CharSequence {
    private String string;

    public CharSequenceDemo(String string) {
        //It would be much more efficient to just reverse the string
        //in the constructor. But a lot less fun!
        this.string = string;
    }

    //If the string is backwards, the end is the beginning!
    private int fromEnd(int i) {
        return string.length() - 1 - i;
    }

    public char charAt(int i) {
        if ((i < 0) || (i >= string.length())) {
            throw new StringIndexOutOfBoundsException(i);
        }
        return string.charAt(fromEnd(i));
    }

    public int length() {
        return string.length();
    }

    public CharSequence subSequence(int start, int end) {
        if (start < 0) {
            throw new StringIndexOutOfBoundsException(start);
        }
        if (end > string.length()) {
            throw new StringIndexOutOfBoundsException(end);
        }
        if (start > end) {
            throw new StringIndexOutOfBoundsException(start - end);
        }
        StringBuilder subString = 
            new StringBuilder(string.subSequence(fromEnd(end), fromEnd(start)));
        return subString.reverse();
    }

    public String toString() {
        StringBuilder string = new StringBuilder(this.string);
        return string.reverse().toString();
    }

    //Random int from 0 to max. As random() generates values between 0 and 0.9999
    private static int random(int max) {
        return (int) Math.round(Math.random() * (max+1));
    }

    public static void main(String[] args) {
        CharSequenceDemo string =
            new CharSequenceDemo("Write a class that implements the CharSequence interface found in the java.lang package.");

        //exercise charAt() and length()
        for (int i = 0; i < string.length(); i++) {
            System.out.print(string.charAt(i));
        }
        
        System.out.println("");

        //exercise subSequence() and length();
        int start = random(string.length() - 1);
        int end = random(string.length() - 1 - start) + start;
        System.out.println(string.subSequence(start, end));

        //exercise toString();
        System.out.println(string);

    }
}