/*+ In the following program, called ComputeResult, what is the value of result after each numbered line executes?
    public class ComputeResult {
        public static void main(String[] args) {
            String original = "software";
            StringBuilder result = new StringBuilder("hi");
            int index = original.indexOf('a');

       result.setCharAt(0, original.charAt(0));
       result.setCharAt(1, original.charAt(original.length()-1));
       result.insert(1, original.charAt(4));
       result.append(original.substring(1,4));
       result.insert(3, (original.substring(index, index+2) + " "));

            System.out.println(result);
        }
    }

Requirements :
  original = software
  index = a 
Entitties:
 ComputerResult
Function Declaration:
 -none-
Job to be done :
  * Create a class with name   ComputeResult.
  * Put public static void main.
  * Inside that declare that original with String datatype and intialize "software" as value.
  * Create a new object name result.
  * declare index as int datatype  and original index value of a is the value for the index.
  * Now setting charecters.
  * then print the result.

Answer:
si
se
swe
sweoft
swear oft*/