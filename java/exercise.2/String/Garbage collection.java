/*+ The following code creates one array and one string object. How many references to those objects exist after the code executes?
  Is either object eligible for garbage collection?
    
    String[] students = new String[10];
    String studentName = "Peter Parker";
    students[0] = studentName;
    studentName = null;

Requirments:
    String[] students = new String[10];
    String studentName = "Peter Parker";
    students[0] = studentName;
    studentName = null;
Entities:
 - none-
Function Declaration:
 -none-
Job to be done:
 * create a new object name students.
 * declare and initial the values.

ans:
There is one reference to the students array and that array has one reference to the string Peter Parker.the array studentname is not eligible for garbage collection
because it has only one reference.the objevt studentName is not eligible either because students[0] still refers to it.*/