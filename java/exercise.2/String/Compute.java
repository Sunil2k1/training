/*+ Write a program that computes your initials from your full name and displays them.

Requirements:
  -> String name
  -> String Buffer
Entities:
  -> Compute
Funtion Declaration:
  -none-
Jobs to be done:
  ->Create a class name Compute
  ->Put public static void main
  ->create a string name name
  ->By using for loop we can find the initial and print the initial 
code:*/
public class Compute{
    public static void main(String [] args) {
         String name = 'G.Sunil';
         StringBuffer initial = new StringBuffer();
         int length = name.length();
         for(int i = 0;i < length;i++){
             if(Charecter.isUpperCase(name.charAt(i))){
                 initial.append(name.charAt(i));
             }
         } 
         System.out.println("My Initial is "+initial);         
    }
}