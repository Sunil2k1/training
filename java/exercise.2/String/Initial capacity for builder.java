/*+ What is the initial capacity of the following string builder?
    StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");

Requirements :
  StringBuilder sb = new StringBuilder("Able was I ere I saw Elba.");
Entitties:
 StringBuilder
Function Declaration:
 -none-
Job to be done :
  * Create a new object name sb for StringBuilder and set value as 'Able was I ere I saw Elba.'.

What is the initial capacity of the following string builder?
ans: 42*/
