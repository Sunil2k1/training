 /*Demonstrate object equality using Object.equals() vs ==, using String objects

Requirments:
    * Demonstrate object equality using Object.equals() vs ==, using String objects.
Entities:
    * ObjectEquality
Function Declaration:
    * public static void main( String[] args )
    * equals()
Jobs to be done:
    * Craete a class called ObjectEquality.
    * Create a two String object s1 and s2, passing a string argument. 
    * In the print statement using == operator and equals method check the both object are equal.
 */
public class ObjectEquality { 
    public static void main(String[] args) 
    { 
        String string1 = new String("HELLO"); 
        String sstring2 = new String("HELLO"); 
        System.out.println(string1 == string2); 
        System.out.println(string1.equals(string2)); 
    } 
} 

/*Demonstrate object equality using Object.equals() vs ==, using String objects

When using == operator for s1 and s2 comparison then the result is false as both have 
different addresses in memory.
Using equals, the result is true because its only comparing the values given in s1 and s2.

-------Output------
false
true */