/*Print the absolute path of the .class file of the current class


Requirments:
    -Print the absolute path of the .class file of the current class.
Entities:
    -AbsolutePath
Function Declaration:
    -public AbsolutePath()
    -public static void main( String[] args )
Jobs to be done:
    * Craete a class called AbsolutePath.
    * create class constructor and create an path object with getClass method, getClassLoader method
for get tha current path class.
    * Declare an URL current variable initialise using path object getResource methoad with passing 
current class file and print the current class path.
    * In the main method create an object for an AbsolutePath class.  */

import java.net.*;

class AbsolutePath {
   public AbsolutePath() {
      ClassLoader path = this.getClass().getClassLoader();
      URL current = path.getResource("AbsolutePath.class");
      System.err.println("Url=" + current);
   }
   public static void main( String[] args ) {
      AbsolutePath path = new AbsolutePath();
   }
}