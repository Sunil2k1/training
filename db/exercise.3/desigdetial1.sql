/*Select College details which are having CSC departments across all the universities and 
Assume that one of the Designation name is ‘HOD’ in Designation table*/
SELECT u.univ_code
      ,c.name
      ,u.univ_name
      ,c.city,c.state
      ,c.year_opend
      ,d.dep_name
      ,e.name AS hod_name
 FROM college c
    INNER JOIN university u 
    ON c.univ_code = u.univ_code
    INNER JOIN employee e
    ON c.id = e.college_id
    INNER JOIN designation de
    ON de.id = e.desig_id
    INNER JOIN college_department cd
    ON cd.cdept_id = e.cdept_id
    INNER JOIN department d 
    ON cd.udept_code = d.dept_code
 WHERE  d.dep_name = 'cse' 
 AND de.name = 'hod';