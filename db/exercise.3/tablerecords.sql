INSERT INTO university 
 VALUES(1,'anna university');
INSERT INTO university
 VALUES(2,'mgr university');
INSERT INTO department 
 VALUES (11,'cse',1);
INSERT INTO department 
 VALUES (12,'ece',1);
INSERT INTO department 
 VALUES (21,'cse',2);
INSERT INTO department 
 VALUES (13,'mech',1);
INSERT INTO department 
 VALUES (24,'eee',2);
INSERT INTO department 
 VALUES (14,'eee',1);
INSERT INTO department 
 VALUES (22,'ece',2); 
INSERT INTO department 
 VALUES (23,'mech',2); 
INSERT INTO college
 VALUES(1,10,'kpr institute',1,'coimbatore','tamil nadu',(2010));
INSERT INTO college
 VALUES(2,20,'psg institute',1,'coimbatore','tamil nadu',(1970));
INSERT INTO college
 VALUES(3,30,'sona institute',1,'salem','tamil nadu',(1985)); 
INSERT INTO college
 VALUES(4,40,'srm institute',2,'chennai','tamil nadu',(1980)); 
INSERT INTO college
 VALUES(5,50,'mgr institute',2,'chennai','tamil nadu',(1990)); 
INSERT INTO designation
 VALUES(1,'hod',1);
INSERT INTO designation
 VALUES(2,'professor',1);
INSERT INTO designation
 VALUES(3,'assistant professor',1);
INSERT INTO designation
 VALUES(4,'office staff',1);
INSERT INTO designation
 VALUES(5,'hod',2);
INSERT INTO designation
 VALUES(6,'professor',2);
INSERT INTO designation
 VALUES(7,'assistant professor',2);
INSERT INTO designation
 VALUES(8,'office staff',2);
INSERT INTO designation
 VALUES(9,'hod',3);
INSERT INTO designation
 VALUES(10,'professor',3);
INSERT INTO designation
 VALUES(11,'assistant professor',3);
INSERT INTO designation
 VALUES(12,'office staff',3);
INSERT INTO designation
 VALUES(13,'hod',4);
INSERT INTO designation
 VALUES(14,'professor',4);
INSERT INTO designation
 VALUES(15,'assistant professor',4);
INSERT INTO designation
 VALUES(16,'office staff',4); 
INSERT INTO designation
 VALUES(17,'hod',5);
INSERT INTO designation
 VALUES(18,'professor',5);
INSERT INTO designation
 VALUES(19,'assistant professor',5);
INSERT INTO designation
 VALUES(20,'office staff',5);
INSERT INTO designation
 VALUES(21,'professor',NULL);
INSERT INTO designation
 VALUES(22,'professor',NULL);
INSERT INTO designation
 VALUES(23,'assistant professor',NULL);
INSERT INTO designation
 VALUES(24,'office staff',NULL);
INSERT INTO designation
 VALUES(25,'office staff',NULL); 
INSERT INTO designation
 VALUES(26,'professor',NULL);
INSERT INTO designation
 VALUES(27,'professor',NULL);
INSERT INTO designation
 VALUES(28,'professor',NULL); 
INSERT INTO college_department 
 VALUES (1,11,1);
INSERT INTO college_department 
 VALUES (2,12,1);
INSERT INTO college_department 
 VALUES (3,13,1);
INSERT INTO college_department 
 VALUES (4,14,1);
INSERT INTO college_department 
 VALUES (5,11,2);
INSERT INTO college_department 
 VALUES (6,12,2);
INSERT INTO college_department 
 VALUES (7,13,2);
INSERT INTO college_department 
 VALUES (8,14,2);
INSERT INTO college_department 
 VALUES (9,11,3);
INSERT INTO college_department 
 VALUES (10,12,3);
INSERT INTO college_department 
 VALUES (11,13,3);
INSERT INTO college_department 
 VALUES (12,14,3);
INSERT INTO college_department 
 VALUES (13,21,4);
INSERT INTO college_department 
 VALUES (14,22,4);
INSERT INTO college_department 
 VALUES (15,23,4);
INSERT INTO college_department 
 VALUES (16,24,4); 
INSERT INTO college_department 
 VALUES (17,21,5);
INSERT INTO college_department 
 VALUES (18,22,5);
INSERT INTO college_department 
 VALUES (19,23,5);
INSERT INTO college_department 
 VALUES (20,24,5);
SELECT cdept_id
  ,udept_code
  ,college_id 
 FROM college_department; 
SELECT id
  ,code
  ,name
  ,univ_code
  ,city
  ,state
  ,year_opend 
 FROM college;
SELECT univ_code
  ,univ_name 
 FROM university;
SELECT dept_code
  ,dep_name
  ,univ_code 
 FROM department;
SELECT id
  ,name
  ,rnk 
 FROM designation;
INSERT INTO employee
 VALUES(1,'gokul',str_to_date('07-08-1969' , '%d-%m-%Y'),'gokulgk@gmail.com',9789117280,1,1,1);
INSERT INTO employee
 VALUES(2,'krish',str_to_date('14-02-1970' , '%d-%m-%Y'),'krish21@gmail.com',934657890,1,2,2);
insert into employee
 VALUES(3,'jack',str_to_date('26-09-1975' , '%d-%m-%Y'),'jacky@gmail.com',8846201520,1,3,3);
INSERT INTO employee
 VALUES(4,'vel',str_to_date('03-03-1969' , '%d-%m-%Y'),'vel1969@gmail.com',9643108712,1,4,4); 
INSERT INTO employee
 VALUES(5,'ganesh',str_to_date('21-12-1977' , '%d-%m-%Y'),'ganesh21@gmail.com',9645312348,2,5,9);
INSERT INTO employee
 VALUES(6,'kumar',str_to_date('01-01-1970' , '%d-%m-%Y'),'kumar1970@gmail.com',9345102831,2,6,10);
INSERT INTO employee
 VALUES(7,'sukumar',str_to_date('17-08-1971' , '%d-%m-%Y'),'sukumar@gmail.com',8810037891,2,7,11);
INSERT INTO employee
 VALUES(8,'saravanan',str_to_date('01-08-1976' , '%d-%m-%Y'),'saravana@gmail.com',9023467980,2,8,12);
INSERT INTO employee
 VALUES(9,'vignesh',str_to_date('06-09-1972' , '%d-%m-%Y'),'vignesh06@gmail.com',9631472580,3,9,13);
INSERT INTO employee
 VALUES(10,'kavin',str_to_date('24-12-1979' , '%d-%m-%Y'),'kavin24@gmail.com',9286431970,3,10,14);
INSERT INTO employee
 VALUES(11,'murugan',str_to_date('04-11-1973' , '%d-%m-%Y'),'murugan@gmail.com',8811756403,3,11,15);
INSERT INTO employee
 VALUES(12,'sanjay',str_to_date('02-09-1976' , '%d-%m-%Y'),'sanjay@gmail.com',9023467980,3,12,16);
INSERT INTO employee
 VALUES(13,'priyan',str_to_date('21-04-1970' , '%d-%m-%Y'),'priyan@gmail.com',9642131530,4,13,5);
INSERT INTO employee
 VALUES(14,'aravindh',str_to_date('16-06-1974' , '%d-%m-%Y'),'aravindh@gmail.com',9142713495,4,14,6);
INSERT INTO employee
 VALUES(15,'raj',str_to_date('03-03-1977' , '%d-%m-%Y'),'raj@gmail.com',8723574126,4,15,7);
INSERT INTO employee
 VALUES(16,'ravi',str_to_date('31-03-1974' , '%d-%m-%Y'),'ravi@gmail.com',9041239786,4,16,8);
INSERT INTO employee
 VALUES(17,'karthi',str_to_date('25-12-1970' , '%d-%m-%Y'),'karthi@gmail.com',974128630,5,17,17);
INSERT INTO employee
 VALUES(18,'aravindhan',str_to_date('17-11-1973' , '%d-%m-%Y'),'aravindhan@gmail.com',9114200369,5,18,18);
INSERT INTO employee
 VALUES(19,'rajkumar',str_to_date('03-07-1974' , '%d-%m-%Y'),'rajkumar@gmail.com',8775436971,5,19,19);
INSERT INTO employee
 VALUES(20,'aakash',str_to_date('31-01-1972' , '%d-%m-%Y'),'aakash@gmail.com',9041003978,5,20,20);
SELECT id
  ,name
  ,dob
  ,email
  ,phone
  ,college_id
  ,cdept_id
  ,desig_id 
 FROM employee;
ALTER TABLE university ADD INDEX(univ_name);
ALTER TABLE designation ADD INDEX(name);