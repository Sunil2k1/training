/*Select final year students details who are studying under a particular university and selected cities alone*/
SELECT s.roll_number
	  ,s.name AS student_name
	  ,s.gender
      ,s.dob
      ,s.address
      ,c.name
      ,d.dep_name
 FROM college c
    INNER JOIN university u 
    ON c.univ_code = u.univ_code
    INNER JOIN college_department cd
    ON cd.college_id = c.id
    INNER JOIN department d 
    ON cd.udept_code = d.dept_code
    INNER JOIN student s
    ON s.cdept_id = cd.cdept_id
 WHERE s.accademic_year = '2021'
 AND u.univ_name='anna university'
 AND c.city ='coimbatore';   
 