 /*Select students details who are studying under a particular university and selected cities alone*/
SELECT s.roll_number
      ,u.univ_name
      ,s.name AS student_name
      ,s.gender
      ,s.dob
      ,s.address
      ,c.name
      ,d.dep_name
      ,e.name AS HOD_Name
 FROM university u 
	INNER JOIN college c
    ON c.univ_code = u.univ_code
    INNER JOIN employee e
    ON c.id = e.college_id
    INNER JOIN designation de
    ON de.id = e.desig_id
    INNER JOIN college_department cd
    ON cd.cdept_id = e.cdept_id
    INNER JOIN department d 
    ON cd.udept_code = d.dept_code
    INNER JOIN student s
    ON s.cdept_id = cd.cdept_id
 WHERE u.univ_name = 'anna university'
 AND c.city ='coimbatore'
 ORDER BY roll_number;