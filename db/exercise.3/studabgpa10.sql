/*Shows students details who scored above 8 GPA for the given semester*/
SELECT u.univ_name
	  ,s.roll_number
	  ,s.name AS student_name
      ,s.gender
      ,s.dob
      ,s.address
      ,c.name AS college_name
      ,sr.semester
      ,sr.grade
      ,sr.credits
      ,sr.gpa
 FROM student s
     INNER JOIN college c
     ON s.college_id = c.id
     INNER JOIN university u
     ON c.univ_code = u.univ_code 
     INNER JOIN semester_result sr
     ON  s.id = sr.stud_id
WHERE  sr.semester = 7
AND sr.gpa > 5
UNION 
SELECT u.univ_name
      ,s.roll_number
	  ,s.name AS student_name
      ,s.gender
      ,s.dob
      ,s.address
      ,c.name AS college_name
      ,sr.semester
      ,sr.grade
      ,sr.credits
      ,sr.gpa
FROM student s
    INNER JOIN college c
    ON s.college_id = c.id
    INNER JOIN university u
    ON c.univ_code = u.univ_code 
    INNER JOIN semester_result sr
    ON  s.id = sr.stud_id
WHERE  sr.semester = 5
AND sr.gpa > 8;
