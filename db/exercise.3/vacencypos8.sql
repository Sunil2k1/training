/*Find employee vacancy position in all the departments from all the colleges and across the universities.*/
SELECT de.name AS designation_name 
      ,de.rnk 
      ,u.univ_code
      ,c.name AS college_name
      ,d.dep_name
      ,u.univ_name
      ,c.city
      ,c.state
      ,c.year_opend
 FROM college c
    INNER JOIN university u 
    ON c.univ_code = u.univ_code
    INNER JOIN employee e
    ON c.id = e.college_id
    INNER JOIN designation de
    ON de.id = e.desig_id
    INNER JOIN college_department cd
    ON cd.cdept_id = e.cdept_id
    INNER JOIN department d 
    ON cd.udept_code = d.dept_code
WHERE e.name IS NULL 
ORDER BY rnk;
