INSERT INTO student 
 VALUES(1,1,'sunil',str_to_date('21-05-2001' , '%d-%m-%Y'),'male','sunil2k1@gmail.com',8870927425,'dadagapatti,salem',(2022),1,1);
INSERT INTO student 
 VALUES(2,2,'kumar',str_to_date('24-12-2002' , '%d-%m-%Y'),'male','kumar@gmail.com',950127920,'gugai,salem',(2023),2,1);
INSERT INTO student 
 VALUES(3,3,'kavin',str_to_date('06-01-2000' , '%d-%m-%Y'),'male','kavin@gmail.com',8107358420,'new bus stand,salem',(2021),3,1); 
INSERT INTO student 
 VALUES(4,4,'krish',str_to_date('09-11-2001' , '%d-%m-%Y'),'male','krish@gmail.com',8107358420,'new bus stand,salem',(2022),4,1);  
INSERT INTO student 
 VALUES(5,5,'gokul',str_to_date('11-01-2003' , '%d-%m-%Y'),'male','gokul@gmail.com',8101742369,'t.nagar,chennai',(2023),5,2); 
INSERT INTO student 
 VALUES(6,6,'jayanthi',str_to_date('31-08-2001' , '%d-%m-%Y'),'female','jayanthi@gmail.com',9726450398,'anna nagar,chennai',(2022),6,2);
INSERT INTO student 
 VALUES(7,7,'priyan',str_to_date('12-10-2002' , '%d-%m-%Y'),'male','priyan@gmail.com',9100369789,'richy street,chennai',(2023),7,2);
INSERT INTO student 
 VALUES(8,8,'saravanan',str_to_date('23-06-2000' , '%d-%m-%Y'),'male','saravanan@gmail.com',8107358420,'gandhi nagar,coimbatore',(2021),8,2); 
INSERT INTO student 
 VALUES(9,9,'krishnan',str_to_date('06-10-2001' , '%d-%m-%Y'),'male','krishnan@gmail.com',8107358420,'kodambakam,chennai',(2022),9,3);  
INSERT INTO student 
 VALUES(10,10,'sanjay',str_to_date('10-07-2003' , '%d-%m-%Y'),'male','sanjay@gmail.com',8101742369,'t.nagar,chennai',(2023),10,3); 
INSERT INTO student 
 VALUES(11,11,'vel',str_to_date('04-04-2001' , '%d-%m-%Y'),'male','vel@gmail.com',8812034561,'anna nagar,chennai',(2022),11,3);
INSERT INTO student 
 VALUES(12,12,'kumaran',str_to_date('19-02-2002' , '%d-%m-%Y'),'male','kumaran@gmail.com',9574563219,'town hall,coimbatore',(2023),12,3);
INSERT INTO student 
 VALUES(13,13,'lakshmi',str_to_date('29-06-2000' , '%d-%m-%Y'),'female','lakshmi@gmail.com',817520130,'gugai,salem',(2021),13,4); 
INSERT INTO student 
 VALUES(14,14,'krishnan',str_to_date('31-08-2001' , '%d-%m-%Y'),'male','krishnan@gmail.com',8130246970,'velacheri,kanchipuram',(2022),14,4);  
INSERT INTO student 
 VALUES(15,15,'sanmugan',str_to_date('23-04-2003' , '%d-%m-%Y'),'male','sanmugan@gmail.com',8113026497,'kodampakam,chennai',(2023),15,4); 
INSERT INTO student 
 VALUES(16,16,'jayanthi',str_to_date('6-08-2001' , '%d-%m-%Y'),'female','jayanthi@gmail.com',9713021364,'dadagapatti,salem',(2022),16,4);
INSERT INTO student 
 VALUES(17,17,'mugan',str_to_date('22-03-2002' , '%d-%m-%Y'),'male','murugan@gmail.com',9114793265,'gandhi nagar,coimbatore',(2023),17,5);
INSERT INTO student 
 VALUES(18,18,'ganesh',str_to_date('29-08-2000' , '%d-%m-%Y'),'male','ganesh@gmail.com',8110236970,'town hall,coimbatore',(2021),18,5); 
INSERT INTO student 
 VALUES(19,19,'surya',str_to_date('06-10-2001' , '%d-%m-%Y'),'male','surya@gmail.com',8146310290,'t.nagar,chennai',(2022),19,5);  
INSERT INTO student 
 VALUES(20,20,'ram',str_to_date('11-07-2003' , '%d-%m-%Y'),'male','ram@gmail.com',8130267980,'anna,nagar',(2023),20,5); 
INSERT INTO student 
 VALUES(21,21,'rathesh',str_to_date('21-08-2001' , '%d-%m-%Y'),'male','rathesh@gmail.com',8870927425,'dadagapatti,salem',(2022),1,1);
INSERT INTO student 
 VALUES(22,22,'vignesh',str_to_date('20-12-2002' , '%d-%m-%Y'),'male','vignesh@gmail.com',950127920,'gugai,salem',(2023),2,1);
INSERT INTO student 
 VALUES(23,23,'kapur',str_to_date('16-01-2000' , '%d-%m-%Y'),'male','kapur@gmail.com',8107358420,'new bus stand,salem',(2021),3,1); 
INSERT INTO student 
 VALUES(24,24,'sukumar',str_to_date('09-01-2001' , '%d-%m-%Y'),'male','sukumar@gmail.com',8107358420,'new bus stand,salem',(2022),4,1);  
INSERT INTO student 
 VALUES(25,25,'madhan',str_to_date('31-01-2003' , '%d-%m-%Y'),'male','madhan@gmail.com',8101742369,'t.nagar,chennai',(2023),5,2); 
INSERT INTO student 
 VALUES(26,26,'priya',str_to_date('03-08-2001' , '%d-%m-%Y'),'female','priya@gmail.com',9726450398,'anna nagar,chennai',(2022),6,2);
INSERT INTO student 
 VALUES(27,27,'surander',str_to_date('12-10-2002' , '%d-%m-%Y'),'male','surander@gmail.com',9100369789,'richy street,chennai',(2023),7,2);
INSERT INTO student 
 VALUES(28,28,'tharun',str_to_date('21-06-2000' , '%d-%m-%Y'),'male','tharun@gmail.com',8107358420,'gandhi nagar,coimbatore',(2021),8,2); 
SELECT id
  ,roll_number
  ,name
  ,dob
  ,gender
  ,email
  ,phone
  ,address
  ,accademic_year
  ,cdept_id
  ,college_id 
 FROM student; 
 