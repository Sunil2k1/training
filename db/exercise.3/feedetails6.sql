/*Create new entries in SEMESTER_FEE table for each student from all the colleges and across all the universities
These entries should be created whenever new semester starts*/
INSERT INTO semester_fee 
 VALUES(1,1,5,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee (cdept_id,stud_id,semester)
 VALUES(2,2,3); 
INSERT INTO semester_fee 
 VALUES(3,3,7,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(4,4,5,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(5,5,3,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(6,6,5,30000,30000,0,(2020),'paid'); 
INSERT INTO semester_fee 
 VALUES(7,7,3,30000,0,30000,(2020),'unpaid');  
INSERT INTO semester_fee 
 VALUES(8,8,7,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(9,9,5,30000,30000,0,(2020),'paid'); 
INSERT INTO semester_fee 
 VALUES(10,10,3,30000,0,30000,(2020),'unpaid');  
INSERT INTO semester_fee 
 VALUES(11,11,5,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(12,12,3,30000,0,30000,(2020),'unpaid');   
INSERT INTO semester_fee 
 VALUES(13,13,7,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(14,14,5,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(15,15,3,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(16,16,5,30000,30000,0,(2020),'paid'); 
INSERT INTO semester_fee 
 VALUES(17,17,3,30000,0,30000,(2020),'unpaid');   
INSERT INTO semester_fee 
 VALUES(18,18,7,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(19,19,5,30000,30000,0,(2020),'paid'); 
INSERT INTO semester_fee 
 VALUES(20,20,3,30000,0,30000,(2020),'unpaid');  
INSERT INTO semester_fee 
 VALUES(1,21,5,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(2,22,3,30000,0,30000,(2020),'unpaid');  
INSERT INTO semester_fee 
 VALUES(3,23,7,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(4,24,5,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(5,25,3,30000,30000,0,(2020),'paid');
INSERT INTO semester_fee 
 VALUES(6,26,5,30000,30000,0,(2019),'paid'); 
INSERT INTO semester_fee 
 VALUES(7,27,3,30000,0,30000,(2020),'unpaid');  
INSERT INTO semester_fee 
 VALUES(8,28,7,30000,30000,0,(2019),'paid');
SELECT cdept_id
  ,stud_id
  ,semester
  ,amount
  ,paid_year
  ,paid_status 
 FROM semester_fee;