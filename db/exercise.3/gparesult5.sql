/*List Students details along with their GRADE,CREDIT and GPA details from all universities. Result should be sorted by college_name and semester
 Apply paging also*/
SELECT s.id
      ,s.roll_number
      ,s.name
      ,s.gender
      ,s.phone 
      ,c.name AS 'College_Name'
      ,sr.semester
      ,sr.credits
      ,sr.grade
      ,sr.gpa
 FROM student s
      INNER JOIN semester_result sr
      ON s.id = sr.stud_id
      INNER JOIN college c
	  ON s.college_id = c.id 
      INNER JOIN university u 
      ON c.univ_code = u.univ_code
	  ORDER BY c.NAME
         ,sr.semester
      LIMIT 14
      OFFSET 0;
SELECT s.id
      ,s.roll_number
      ,s.name
      ,s.gender
      ,s.phone 
      ,c.name AS 'College_Name'
      ,sr.semester
      ,sr.credits
      ,sr.grade
      ,sr.gpa
 FROM student s
     INNER JOIN semester_result sr
     ON s.id = sr.stud_id
	 INNER JOIN college c
     ON s.college_id = c.id 
	 INNER JOIN university u 
     ON c.univ_code = u.univ_code
     ORDER BY c.NAME
         ,sr.semester
     LIMIT 14
     OFFSET 15;
 
