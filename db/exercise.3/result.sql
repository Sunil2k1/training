INSERT INTO semester_result 
 VALUES (1,1,5,'o',15,9.0,str_to_date('21-08-2020' , '%d-%m-%Y'));
INSERT INTO semester_result 
 VALUES (2,2,3,'a+',14,8.0,str_to_date('21-08-2020' , '%d-%m-%Y'));
INSERT INTO semester_result 
 VALUES (3,3,7,'a',13,7.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (4,4,5,'b',11,5.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (5,5,3,'o',15,9.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (6,6,5,'b+',12,6.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (7,7,3,'a+',14,8.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (8,8,7,'a',13,7.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (9,9,5,'o',15,9.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (10,10,3,'b',12,6.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (11,11,5,'o',15,9.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (12,12,3,'a+',14,8.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (13,13,7,'o',15,9.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (14,14,5,'a',13,8.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (15,15,3,'b+',12,6.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (16,16,5,'b',11,5.0,str_to_date('21-08-2020' , '%d-%m-%Y'));  
INSERT INTO semester_result 
 VALUES (17,17,3,'o',15,9.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (18,18,7,'o',15,9.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (19,19,5,'a',13,7.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (20,20,5,'a',13,7.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (21,1,5,'a+',14,8.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (22,2,3,'a+',14,8.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (23,3,7,'o',15,9.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (24,4,5,'b',11,5.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (25,5,3,'b',11,6.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (26,6,5,'b+',12,6.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (27,7,3,'a',13,7.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
INSERT INTO semester_result 
 VALUES (28,8,7,'o',15,9.0,str_to_date('21-08-2020' , '%d-%m-%Y')); 
SELECT stud_id
  ,syllabus_id
  ,semester
  ,grade
  ,credits
  ,gpa
  ,result_date 
 FROM semester_result; 
