/*Update PAID_STATUS and PAID_YEAR in SEMESTER_FEE table who has done the payment. Entries should be updated based on student ROLL_NUMBER*/
/*Single update*/
UPDATE semester_fee 
SET paid_year = (2020) 
   ,paid_status = "paid" 
WHERE "2" = (
    SELECT roll_number
    FROM student 
    WHERE semester_fee.stud_id = student.ID
);
/*bulk update*/
UPDATE semester_fee 
SET paid_year = (2019) 
   ,paid_status = "Paid" 
WHERE (
    SELECT roll_number
    FROM student 
    WHERE semester_fee.stud_id = student.ID
) IN (7,12);
SELECT cdept_id
  ,stud_id
  ,semester
  ,amount
  ,paid_year
  ,paid_status 
 FROM semester_fee;