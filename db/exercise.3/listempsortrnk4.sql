/*List Employees details from a particular university along with their college and department details.
 Details should be sorted by rank and college name*/
SELECT u.univ_code
      ,c.name
      ,u.univ_name
      ,c.city,c.state
      ,c.year_opend
      ,d.dep_name
      ,e.name AS employee_name
      ,de.name AS designation_name
      ,de.rnk AS employee_rank
 FROM college c
    INNER JOIN university u 
    ON c.univ_code = u.univ_code
    INNER JOIN employee e
    ON c.id = e.college_id
    INNER JOIN designation de
    ON de.id = e.desig_id
    INNER JOIN college_department cd
    ON cd.cdept_id = e.cdept_id
    INNER JOIN department d 
    ON cd.udept_code = d.dept_code
 WHERE  u.univ_name = 'anna university'
 ORDER BY de.rnk;