/*collected and uncollected semester fees amount per semester for each college under an university.*/
SELECT u.univ_name
  ,c.name
  ,sf.semester 
  ,sum(sf.collected) AS collected_fee
  ,sum(sf.uncollected) AS uncollected_fee
  ,sf.paid_year
  FROM student s
	 INNER JOIN college c
     ON s.college_id = c.id
     INNER JOIN university u
     ON c.univ_code = u.univ_code
     INNER JOIN semester_fee sf
     ON sf.stud_id = s.id
 WHERE sf.paid_year = 2020
 GROUP BY sf.semester
    ,c.name;
/*Collected semester fees amount for each university for the given year*/ 
SELECT u.univ_name
      ,sf.semester 
      ,sum(sf.collected) AS collected_fee
      ,sf.paid_year
FROM student s
	 INNER JOIN college c
     ON s.college_id = c.id
     INNER JOIN university u
     ON c.univ_code = u.univ_code
     INNER JOIN semester_fee sf
     ON sf.stud_id = s.id
 WHERE sf.paid_year = 2019
 AND univ_name = 'anna university'
 GROUP BY sf.semester
    ,c.name;