/*Read Records by Sub-queries by using two tables FOREIGN KEY relationships*/
CREATE TABLE subject(sub_id INT PRIMARY KEY,subject_name VARCHAR(25));
CREATE TABLE staff(sf_id INT,staff_name VARCHAR(25),sub_id INT,FOREIGN KEY(sub_id) REFERENCES subject(sub_id));
INSERT INTO subject 
 VALUES(1,'maths');
INSERT INTO subject 
 VALUES(2,'science');
INSERT INTO subject 
 VALUES(3,'tamil');
SELECT sub_id
  ,subject_name 
 FROM subject;
INSERT INTO staff 
 VALUES(1,'kumar',2);
INSERT INTO staff 
 VALUES(2,'raj',3);
INSERT INTO staff 
 VALUES(3,'gokul',1);
SELECT sf_id
  ,staff_name
  ,sub_id 
 FROM staff;
SELECT sub.subject_name 
  ,sf.staff_name 
 FROM subject sub
  ,staff sf 
 WHERE (sf.sub_id=sub.sub_id);