/*Insert Records into Table*/
CREATE TABLE student(st_id INT NOT NULL,first_name VARCHAR(25),surname VARCHAR(25),dob VARCHAR(25),city VARCHAR(25) DEFAULT 'salem');
DESC student;
INSERT INTO student (st_id,first_name,surname,dob) 
 VALUES(1,'sunil','gokul','21-05-2001');
INSERT INTO student (st_id,first_name,surname,dob,city)
 VALUES(2,'kumar','raj','2-06-2001','coimbatore');
INSERT INTO student (st_id,first_name,surname,dob)
 VALUES(3,'kavin','krish','1-02-2001');
SELECT st_id 
  ,first_name 
  ,surname 
  ,dob 
  ,city 
 FROM student;