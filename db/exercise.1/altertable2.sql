/*Alter Table with Add new Column and Modify\Rename\Drop column*/
CREATE TABLE student(std_id INT,name VARCHAR(25));
DESC student;
/*Add a New Column*/
ALTER TABLE student 
 ADD address VARCHAR(25);
DESC student;
/*rename column name*/
ALTER TABLE student 
 RENAME COLUMN address 
 TO mob_no;
DESC student;
/*rename column datatype*/
ALTER TABLE	student 
 MODIFY mob_no INT;
DESC student;
/*drop the column*/
ALTER TABLE student 
 DROP mob_no;
DESC student;