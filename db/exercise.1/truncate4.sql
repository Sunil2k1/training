/*Truncate Records From table*/
CREATE TABLE student(st_id INT,name VARCHAR(25),dob VARCHAR(25));
DESC student;
INSERT INTO student 
 VALUES(1,'sunil','21-05-2001');
INSERT INTO student 
 VALUES(2,'kavin','24-12-2001'); 
SELECT st_id 
  ,name
  ,dob  
 FROM student;
TRUNCATE TABLE student;
SELECT st_id 
  ,name
  ,dob 
 FROM student; 