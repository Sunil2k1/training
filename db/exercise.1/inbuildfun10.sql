/*USE inbuilt Functions - now,MAX, MIN, AVG, COUNT, FIRST, LAST, SUM*/
create table customer(id INT,name VARCHAR(25),age INT,amt INT);
INSERT INTO customer 
 VALUES(1,'sunil',23,100);
INSERT INTO customer 
 VALUES(2,'kavin',40,150);
INSERT INTO customer 
 VALUES(3,'gokul',56,500);
SELECT id 
  ,name 
  ,age 
  ,amt 
 FROM customer;
 /*Using max Function*/
SELECT MAX(amt) 
 AS largestprice 
 FROM customer;
 /*Using min Function*/
SELECT MIN(amt) 
 AS lowestprice 
 FROM customer;
/*Using AVG Function*/
SELECT AVG(amt) 
 AS averageprice 
 FROM customer;
/*Using sum Function*/ 
SELECT SUM(amt) 
 AS totalprice 
 FROM customer;
 /*Using COUNT Function*/ 
SELECT COUNT(id) 
 AS totalcustomers 
 FROM customer;
 /*first*/
SELECT name 
 AS first_customer
 FROM customer 
 ORDER BY name 
 DESC 
 LIMIT 1;
/*last*/ 
SELECT name 
 AS last_customer
 FROM customer
 ORDER BY name 
 ASC 
 LIMIT 1; 