/*Fetch ALL records and Fetch particular column Records*/
CREATE TABLE student(st_id INT,name VARCHAR(25),class VARCHAR(25),age INT);
DESC student;
INSERT INTO student 
 VALUES(1,'sunil','b',18);
INSERT INTO student 
 VALUES(2,'kavin','a',14);
INSERT INTO student 
 VALUES(3,'gokul','c',12);
INSERT INTO student 
 VALUES(4,'krish','a',13);
SELECT name FROM student;
SELECT st_id
  ,name
  ,class 
  ,age 
 FROM student ;