/*USE Where to filter records using AND,OR,NOT,LIKE,IN,ANY, wildcards ( % _ )*/
CREATE TABLE student(st_id INT,name VARCHAR(25),class VARCHAR(25),age INT);
DESC student;
INSERT INTO student 
 VALUES(1,'sunil','b',18);
INSERT INTO student 
 VALUES(2,'kavin','a',14);
INSERT INTO student 
 VALUES(3,'gokul','c',12);
INSERT INTO student 
 VALUES(4,'krish','a',13);
INSERT INTO student 
 VALUES(5,'raj','b',17);
INSERT INTO student 
 VALUES(6,'kavin','a',12);
INSERT INTO student 
 VALUES(7,'ram','c',12);
INSERT INTO student 
 VALUES(8,'vel','a',15);
SELECT st_id
  ,name
  ,class
  ,age 
 FROM student ;
SELECT st_id
  ,name
  ,class
  ,age 
 FROM student  
 WHERE (age > 13);
 /*Using or*/
SELECT st_id
  ,name
  ,class
  ,age 
 FROM student  
 WHERE name = 'sunil' 
 OR name = 'krish';
/*Using not and in*/ 
SELECT st_id
  ,name
  ,class
  ,age 
 FROM student 
 WHERE st_id 
 NOT IN (2,5,7);
/*Using like and wildcards'_'*/
SELECT st_id
  ,name
  ,class
  ,age 
 FROM student 
 WHERE name 
 LIKE '_avin';
/*Using like and wildcards'%'*/ 
SELECT st_id
  ,name
  ,class
  ,age 
 FROM student 
 WHERE name 
 LIKE '%i%';
/*Using and*/  
SELECT st_id
  ,name
  ,class
  ,age 
 FROM student 
 WHERE age = 14 
 AND name = 'kavin';
/*Using in*/   
SELECT st_id
  ,name
  ,class
  ,age 
 FROM student 
 WHERE st_id 
 IN (1,3,5,8);
 /*Using any*/
SELECT name  FROM student 
 WHERE name= ANY
 (       SELECT name 
         FROM student
 );