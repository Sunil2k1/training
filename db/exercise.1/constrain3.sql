/*Use Constraints NOT NULL, DEFAULT, CHECK, PRIMARY KEY, FOREIGN KEY,INDEX*/
CREATE TABLE student (st_id INT NOT NULL PRIMARY KEY ,name VARCHAR(25),age INT CHECK(age>11),class VARCHAR(25) DEFAULT 'a');
CREATE TABLE staff(sf_id INT NOT NULL PRIMARY KEY,sf_name VARCHAR(24),st_id INT,FOREIGN KEY(st_id) REFERENCES student(st_id));
DESC student;
DESC staff;
/*Creating index*/
CREATE INDEX st 
 ON student(st_id,name,age,class);
DROP INDEX st 
 ON student;