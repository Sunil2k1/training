/*Create VIEW for your Join Query and select values from VIEW*/
CREATE TABLE subject(sub_id INT PRIMARY KEY,subject_name VARCHAR(25));
CREATE TABLE staff(sf_id INT,staff_name VARCHAR(25),sub_id INT,FOREIGN KEY(sub_id) REFERENCES subject(sub_id));
INSERT INTO subject 
 VALUES(1,'maths');
INSERT INTO subject 
 VALUES(2,'science');
INSERT INTO subject 
 VALUES(3,'tamil');
SELECT sub_id
  ,subject_name 
 FROM subject;
INSERT INTO staff 
 VALUES(1,'kumar',2);
INSERT INTO staff 
 VALUES(2,'raj',3);
INSERT INTO staff 
 VALUES(3,'gokul',1);
SELECT sf_id
  ,staff_name
  ,sub_id 
 FROM staff;
CREATE VIEW s 
 AS SELECT sub_id
  ,subject_name 
 FROM subject;
CREATE VIEW f 
 AS SELECT sf_id
  ,staff_name
  ,sub_id 
 FROM staff;
SELECT s.subject_name
  ,f.staff_name 
 FROM s 
 INNER JOIN f 
 WHERE(s.sub_id=f.sub_id);