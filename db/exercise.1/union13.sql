/*At-least use 3 tables for Join Union 2 Different table with same Column name (use Distinct,ALL)/*/
CREATE TABLE subject(sub_id INT,sub_name VARCHAR(25),staff_id INT);
CREATE TABLE staff(sf_id INT,sf_name VARCHAR(25),sub_id INT);
CREATE TABLE student(std_id INT,std_name VARCHAR(25),sf_id INT);
INSERT INTO subject 
 VALUES(1,'maths',3);
INSERT INTO subject 
 VALUES(2,'science',1);
INSERT INTO subject 
 VALUES(3,'tamil',2);
INSERT INTO staff 
 VALUES(1,'kumar',2);
INSERT INTO staff 
 VALUES(2,'raj',3);
INSERT INTO staff 
 VALUES(3,'gokul',1);
INSERT INTO student 
 VALUES(1,'krish',3);
INSERT INTO student 
 VALUES(2,'vel',2);
INSERT INTO student 
 VALUES(3,'saravanan',1);
 /*Join Union 2 Different table with same Column name use ALL*/
SELECT sub_id  
 FROM staff 
 UNION ALL SELECT sub_id 
 FROM staff;
 /*Join Union 2 Different table with same Column name use Distinct*/
SELECT sub_id 
 FROM staff 
 UNION SELECT sub_id 
 FROM staff;
SELECT sub_id 
 FROM staff 
 UNION SELECT sf_name 
 FROM staff 
 WHERE sf_name='gokul';