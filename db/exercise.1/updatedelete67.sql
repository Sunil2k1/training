/*Update Field value on the Fields with/without WHERE*/
CREATE TABLE student(st_id INT,name VARCHAR(25),dob VARCHAR(25));
DESC student;
INSERT INTO student 
 VALUES(1,'sunil','21-05-2001');
INSERT INTO student 
 VALUES(2,'kavin','02-01-2001');	
SELECT st_id
  ,name
  ,dob 
 FROM student;
UPDATE student 
 SET name='kumar' 
 WHERE st_id=1;
SELECT st_id
  ,name
  ,dob 
 FROM student;
/*Delete Records from the table with/without WHERE*/
DELETE FROM student  
 WHERE name='kumar';
SELECT st_id
  ,name
  ,dob  FROM student;