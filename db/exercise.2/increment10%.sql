/*increment 10% of every employee salary*/ 
UPDATE employee 
 SET annual_salary=annual_salary+(annual_salary*10/100);
SELECT emp_id
  ,first_name
  ,surname
  ,dob
  ,date_of_joining
  ,annual_salary
  ,exp_inyears
  ,area
  ,dept_no  
 FROM employee;
