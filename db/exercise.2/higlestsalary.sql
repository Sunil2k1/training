/*the highest and least paid in all the department*/ 
SELECT dept_no
  ,MAX(annual_salary) 
 AS MaxSalary
  ,MIN(annual_salary) 
 AS MinSalary 
 FROM employee employee 
 GROUP BY dept_no;
