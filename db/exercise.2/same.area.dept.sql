/*list out employees from the same area and from the same department*/ 
SELECT e.first_name
  ,d.dept_name 
  ,e.area
 FROM employee e
  ,department d 
 WHERE e.dept_no=d.dept_no 
 AND e.area 
 IN ('salem') 
 AND d.dept_name 
 IN ('ITDesk') ;