/*create table department and employee*/
/*department number must be the primary key in department table*/
CREATE TABLE department(dept_no INT PRIMARY KEY,dept_name VARCHAR(25));
ALTER TABLE department ADD INDEX(dept_name);
DESC department;
/*  employee number must be the primary key in employee table
    department number is the foreign key in the employee table*/
CREATE TABLE employee(emp_id INT PRIMARY KEY,first_name VARCHAR(25),surname VARCHAR(25),dob VARCHAR(25),date_of_joining VARCHAR(25),annual_salary INT,exp_inyears VARCHAR(25),area VARCHAR(25),dept_no INT,FOREIGN KEY(dept_no) REFERENCES department(dept_no));
ALTER TABLE employee ADD INDEX(first_name);
DESC employee;
INSERT INTO department 
 VALUES(1,'ITDesk');
INSERT INTO department 
 VALUES(2,'Finance');
INSERT INTO department 
 VALUES(3,'Engineering');
INSERT INTO department 
 VALUES(4,'HR');
INSERT INTO department 
 VALUES(5,'Recruiment');
INSERT INTO department 
 VALUES(6,'Facility');
SELECT dept_no
  ,dept_name 
 FROM department;
 /*insert at least 5 employees for each department*/
INSERT INTO employee 
 VALUES(1,'sunil','gokul','02/02/2001','4/05/2021',600000,'fresher','salem',1);
INSERT INTO employee 
 VALUES(2,'kavin','gokul','04/05/2001','7/05/2021',700000,4,'erode',2);
INSERT INTO employee 
 VALUES(3,'kumar','raj','19/03/2001','9/05/2021',1200000,1,'coimbatore',5);
INSERT INTO employee 
 VALUES(4,'hulk','bruce','22/06/2001','1/06/2021',400000,'fresher','chennai',4);
INSERT INTO employee 
 VALUES(5,'tom','kent','04/09/2001','2/06/1999',300000,4,'salem',3);
INSERT INTO employee 
 VALUES(6,'jerry','jack','18/10/2001','4/07/2021',500000,3,'salem',1);
INSERT INTO employee 
 VALUES(7,'dev','stark','11/1/2001','27/08/2021',600000,'fresher','chennai',6);
INSERT INTO employee 
 VALUES(8,'krish','tonny','11/12/2001','17/09/2021',500000,'fresher','namakal',3);
INSERT INTO employee 
 VALUES(9,'kanan','vel','15/11/2001','4/10/2021',600000,2,'salem',2);
INSERT INTO employee 
 VALUES(10,'ravi','vijay','01/01/2001','1/11/2021',600000,4,'erode',4);
INSERT INTO employee 
 VALUES(11,'ashok','buddy','02/02/2001','17/03/2021',2000000,'fresher','thirupur',1);
INSERT INTO employee 
 VALUES(12,'karthi','natasha','04/05/2001','fresher',750000,'fresher','coimbatore',2);
INSERT INTO employee 
 VALUES(13,'suriya','narayanan','19/03/1990','9/05/2021',650000,6,'salem',5);
 INSERT INTO employee 
 VALUES(14,'arun','aravindh','22/06/2001','1/06/2021',640000,2,'sangagiri',4);
INSERT INTO employee 
 VALUES(15,'cholan','aadhi','04/09/2001','fresher',650000,'fresher','athur',3);
INSERT INTO employee 
 VALUES(16,'arjun','karnan','18/10/2001','4/07/2021',900000,'fresher','bavani',1);
INSERT INTO employee 
 VALUES(17,'sanjay','rathesh','11/1/2001','27/08/2021',600000,'fresher','pollachi',6);
INSERT INTO employee 
 VALUES(18,'vishnu','tharun','11/12/2001','17/09/2021',420000,2,'salem',3);
INSERT INTO employee 
 VALUES(19,'vijayan','kumaran','15/11/2001','4/10/2021',600000,2,'erode',2);
INSERT INTO employee 
 VALUES(20,'saran','jayanth','1/01/2001','1/11/2021',600000,1,'coimbatore',4);
INSERT INTO employee 
 VALUES(21,'murugapan','mani','02/02/2001','3/12/2021',600000,'fresher','namakal',1);
INSERT INTO employee  
 VALUES(22,'vadivelu','madhavn','04/05/2001','7/05/2021',750000,'fresher','salem',2);
INSERT INTO employee 
 VALUES(23,'vivek','sendhil','19/03/2001','9/05/2021',1000000,3,'chennai',5);
INSERT INTO employee 
 VALUES(24,'barathi','veelan','22/06/2001','1/06/2021',930000,3,'chennai',4);
INSERT INTO employee 
 VALUES(25,'pandi','sathyan','04/09/2001','6/07/2021',650000,4,'salem',3);
INSERT INTO employee 
 VALUES(26,'akash','karan','18/10/2001','4/07/2021',600000,'fresher','erode',2);
INSERT INTO employee 
 VALUES(27,'thanesh','suresh','11/1/2001','27/08/2021',600000,'fresher','coimbatore',6);
INSERT INTO employee 
 VALUES(28,'surandar','sukumar','11/12/2001','17/09/2021',420000,1,'erode',5);
INSERT INTO employee 
 VALUES(29,'rasul','rahman','15/11/2001','4/10/2021',600000,1,'chennai',6);
INSERT INTO employee 
 VALUES(30,'ram','ranjith','01/01/2001','1/11/2021',600000,3,'namakal',5);
SELECT emp_id
  ,first_name
  ,surname
  ,dob
  ,date_of_joining
  ,annual_salary
  ,exp_inyears
  ,area
  ,dept_no 
 FROM employee;
