/*employee names and their respective department name*/
SELECT  e.first_name
  ,d.dept_name 
 FROM employee e 
 INNER JOIN  department d
 ON (d.dept_no = e.dept_no);